# Errors

The server may respond with the following errors:

## Client Errors

### 400 Bad Request

The server cannot or will not process the request due to an apparent client error:

-   malformed request syntax
-   size too large
-   invalid request message framing
-   deceptive request routing

### 401 Not Authenticated

Specifically for use when authentication is required and has failed or has not yet been provided. The response must include a "WWW-Authenticate" header field containing a challenge applicable to the requested resource.

    HTTP/1.1 401 Unauthorized
    WWW-Authenticate: Bearer realm="alinex.de"

### 402 Payment Required

Reserved for future use in HTTP. The original intention was that this code might be used as part of some form of digital cash or micropayment scheme.

### 403 Not Allowed

The request contained valid data and was understood by the server, but the server is refusing action. This may be due to:

-   server did not accept the authentication
-   user not having the necessary permissions/ability for a resource
-   needing a specific account of some sort
-   attempting a prohibited action like creating a duplicate record where only one is allowed

The request should not be repeated.

### 404 Resource not Found

The requested resource could not be found but may be available in the future. Subsequent requests by the client are permissible. This code is also used to deny an action without giving a specific reason.

### 405 Method not Allowed

A request method is not supported for the requested resource. Like a GET request on a form that requires data to be presented via POST, or a PUT request on a read-only resource.

### 406 Not Acceptable

The requested resource is capable of generating only content not acceptable according to the "Accept" headers sent in the request. Possible "Content-Type" entries may be given in the response.

### 408 Request Timeout

The client did not produce a complete request within the time that the server was prepared to wait. The client MAY repeat the request without modifications at any later time.

### 409 Conflict

Indicates that the request could not be processed because of conflict in the current state of the resource:

-   edit conflict between multiple simultaneous updates

### 411 Length Required

The request did not specify the length of its content, which is required by the requested resource.

### 422 Unprocessable Entity

This is used if code 400 is not possible like in a well-formed request which was unable to be followed due to:

-   semantic errors
-   like validation problem

### 429 Too Many Requests

The user has sent too many requests in a given amount of time. Intended for use with rate-limiting schemes.
The current setting of the rate limit is shown in any response header:

    X-RateLimit-Limit: 6
    X-RateLimit-Remaining: 5
    X-RateLimit-Reset: 1604087930

## Server Error

### 500 Internal Server Error

A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.

### 501 Not Implemented

The server either does not recognize the request method, or it lacks the ability to fulfill the request. Usually this implies future availability (e.g., a new feature of a web-service API).

### 502 Bad Gateway

The server was acting as a gateway or proxy and received an invalid response from the upstream server.

### 503 Service Unavailable

The server cannot handle the request (because it is overloaded or down for maintenance). Generally, this is a temporary state. A "Retry-After" header field may give a time when it will be available again.

{!docs/assets/abbreviations.txt!}
