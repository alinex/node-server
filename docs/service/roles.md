title: Roles

# Role Service

The user service will be used internally by the authentication service to check for authenticated roles.
Also it is used for user management through the client application.

!!! info

    This service isbased on the [REST API](rest.md). See the documentation there for all the possbilities on how to filter, sort, search and select the output fotmat.

## Model

The user has the following fields:

-   `_id` - object id
-   `name` - short name for the role
-   `description` - description explaining the use
-   `disabled`? - flag to disable role
-   `abilities` - list of what the roles are allowed to do
    -   `inverted`? - if `true` this defines what is forbidden
    -   `action` - list of allowed actions
    -   `subject` - list of allowed subjects
    -   `conditions`? - filter cindition for allowed records
    -   `fields`? - list of fields allowed (if none all are allowed)
-   `createdAt` - date/time role was created at
-   `updatedAt` - date/time the record was last changed
-   `__v` - integer // version id

## find - list roles

A list of roles will be retrieved.

_Only for authenticated users!_

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/roles
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('roles').find();
        ```

    === "CLI helper"

        ```bash
        bin/server-call /roles
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/roles \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
        "total": 5,
        "limit": 10,
        "skip": 0,
        "data": [
            {
                "_id": "5e6a5deeeda15707987a9954",
                "name": "Local",
                "description": "Local Administration",
                "abilities": [
                    {
                        "action": [
                            "read"
                        ],
                        "subject": [
                            "users",
                            "roles",
                            "info",
                            "checkup"
                        ]
                    }
                ],
                "createdAt": "2020-03-12T17:01:34.027Z",
                "updatedAt": "2020-03-12T17:01:34.027Z"
            },
            ...
        ]
    }
    ```

## get - role record

Get information for a specific role.

_Only for authenticated roles!_

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/roles/5e6a5deeeda15707987a9954
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('roles').get('5e6a5deeeda15707987a9954');
        ```

    === "CLI helper"

        ```bash
        bin/server-call /roles/5e6a5deeeda15707987a9954
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/roles/5e6a5deeeda15707987a9954 \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
        "_id": "5e6a5deeeda15707987a9954",
        "name": "Local",
        "description": "Local Administration",
        "abilities": [
            {
                "action": [
                    "read"
                ],
                "subject": [
                    "users",
                    "roles",
                    "info",
                    "checkup"
                ]
            }
        ],
        "createdAt": "2020-03-12T17:01:34.027Z",
        "updatedAt": "2020-03-12T17:01:34.027Z"
    }
    ```

## create - new role

Create new role.

!!! example

    === "HTTP"

        ```bash
        POST {{SERVER}}/roles
        Authorization: Bearer {{JWT}}
        Content-Type: application/json

        {
            "name": "support",
            "description": "1st level support",
            "abilities": [{ "action": ["read"], "subject": ["info"] }]
        }
        ```

    === "JavaScript"

        ```js
        app.service('roles').create({
            name: "support",
            description: "1st level support",
            abilities: [{ action: ["read"], subject: ["info"] }]
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call -m POST /roles \
            -d '{
                "name": "support",
                "description": "1st level support",
                "abilities": [{ "action": ["read"], "subject": ["info"] }]
            }'
        ```

    === "cURL"

        ```bash
        curl --request POST $SERVER/roles \
             --header  "Authorization: Bearer $JWT" \
             --header  'Content-Type: application/json' \
             --data    '{
                "name": "support",
                "description": "1st level support",
                "abilities": [{ "action": ["read"], "subject": ["info"] }]
            }'
        ```

    Will produce a result like:

    ```json
    {
        _id: "5e4ec281b3fbcb6b1024e345",
        name: "support",
        description: "1st level support",
        abilities: [{ action: ["read"], subject: ["info"] }]
        createdAt: new Date("2020-02-20T17:31:45.775Z"),
        updatedAt: new Date("2020-02-20T17:31:45.775Z"),
        __v: 0
    }
    ```

## update - replace record

Update a complete record with new settings.

!!! example

    === "HTTP"

        ```bash
        PUT {{SERVER}}/roles/429ee71276122f55a3a94796
        Authorization: Bearer {{JWT}}
        Content-Type: application/json

        {
            name: "support",
            description: "1st level support",
            abilities: [{ action: ["read"], subject: ["info"] }]
        }
        ```

    === "JavaScript"

        ```js
        app.service('roles').update('429ee71276122f55a3a94796',
        {
            name: "support",
            description: "1st level support",
            abilities: [{ action: ["read"], subject: ["info"] }]
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call -m PUT /roles/429ee71276122f55a3a94796 \
            -d '{
                name: "support",
                description: "1st level support",
                abilities: [{ action: ["read"], subject: ["info"] }]
            }'
        ```

    === "cURL"

        ```bash
        curl --request PUT $SERVER/roles/429ee71276122f55a3a94796 \
             --header  "Authorization: Bearer $JWT" \
             --header  'Content-Type: application/json' \
             --data    '{
                "email" : "demo@alinex.de",
                "password" : "demo123",
                "nickname" : "demo",
                "disabled": false,
                "name" : "Demo User",
                "position" : "Test"
            }'
        ```

    Will produce a response like:

    ```json
    { _id: "429ee71276122f55a3a94796" }
    ```

## patch - change some values

Update only some fields.

!!! example

    === "HTTP"

        ```bash
        PATCH {{SERVER}}/roles/5e4c59fad2fd58577a9eca15
        Authorization: Bearer {{JWT}}
        Content-Type: application/json

        { "name" : "demo" }
        ```

    === "JavaScript"

        ```js
        app.service('roles').patch('5e4c59fad2fd58577a9eca15', { name: "demo" });
        ```

    === "CLI helper"

        ```bash
        bin/server-call -m PATCH /roles/5e4c59fad2fd58577a9eca15 \
            -d '{ "name" : "demo" }'
        ```

    === "cURL"

        ```bash
        curl --request PATCH $SERVER/roles/5e4c59fad2fd58577a9eca15 \
             --header  "Authorization: Bearer $JWT" \
             --header  'Content-Type: application/json' \
             --data    '{ "name" : "demo" }'
        ```

    Will produce a response like:

    ```json
    { _id: "5e4c59fad2fd58577a9eca15" }
    ```

## remove - role

!!! example

    === "HTTP"

        ```bash
        DELETE {{SERVER}}/roles/5e4c59fad2fd58577a9eca15
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('roles').delete('5e4c59fad2fd58577a9eca15');
        ```

    === "CLI helper"

        ```bash
        bin/server-call -m DELETE /roles/5e4c59fad2fd58577a9eca15
        ```

    === "cURL"

        ```bash
        curl --request DELETE $SERVER/roles/5e4c59fad2fd58577a9eca15 \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    { _id: "429ee71276122f55a3a94796" }
    ```

{!docs/assets/abbreviations.txt!}
