title: Authentication

# Authentication Service

A JSON web token is used for authentication. It is an access token that is valid for a limited time (one day by default) that is issued by the server and needs to be sent with every API request that requires authentication.

!!! Notice

    An alternative login method is to come from localhost or an IP address registered as `autologin` in the user's record.
    
!!! info

    This service is based on the [REST API](rest.md). See the documentation there for all the possibilities on how to filter, sort, search and select the output format.

## JSON Web Token (JWT)

It is defined as an open standard in [RFC 7519](https://tools.ietf.org/html/rfc7519) and defines a compact and self-contained way for securely transmitting information between parties as a JSON object. This information can be verified and trusted because it is digitally signed.

The JWT defines a compact and self-contained way for securely transmitting information between parties as a signed JSON object consisting of three parts:

-   Header
-   Payload
-   Signature

Therefore, a JWT typically looks like the following with all three parts being Base64 encoded:

    xxxxx.yyyyy.zzzzz

To easily analyze your JWT you may let it's payload and header be displayed using the [online decoder](https://jwt.io/#debugger-io).

### Header

The header typically consists of two parts: the type of the token, which is JWT, and the hashing algorithm being used, such as HMAC SHA256 or RSA.

```json
{
    "alg": "HS256",
    "typ": "JWT"
}
```

### Payload

The second part of the token is the payload, which contains the claims. Claims are statements about the user.

The JWT specification defines seven claims that can be included in a token. These are registered claim names, and they are:

    iss - issuer: identifies the principal that issued the JWT
    sub - subject: identifies the principal that is the subject of the JWT
    aud - audience: identifies the recipients that the JWT is intended for as
          string or array
    exp - expirarion time: identifies the expiration time on or after which the
          JWT MUST NOT be accepted for processing
    nbf - not brfore: identifies the time before which the JWT MUST NOT be accepted
          for processing
    iat - issued at: identifies the time at which the JWT was issued
    jti - unique identifier: provides a unique identifier for the JWT

Additional payload is possible, but should be kept at a minimum if needed.

An example of payload could be:

```json
{
    "iss": "alinex.de", // issuer
    "sub": "429ee71276122f55a3a94796", // subject
    "aud": "https://demo.alinex.de", // audience
    "exp": 1583946857, // expiration date
    "iat": 1583860457, // creation date
    "jti": "edb90316-b334-45a1-babe-59b6064251a9" // identifier (user id)
}
```

### Signature

To create the signature part you have to take the encoded header, the encoded payload, a secret, the algorithm specified in the header, and sign that like:

```js
HMACSHA256(base64UrlEncode(header) + '.' + base64UrlEncode(payload), secret);
```

The signature is used to verify that the sender of the JWT is who it says it is and to ensure that the message wasn't changed along the way. Only the server knows the secret so no one can make a valid JWT as the server itself and only accepts his own JWT.

As an example, if the JWT is changed on the way to the client or back on the next action the server will reject.

## Model

-   `accessToken` - JSON Web Token
-   `authentication`
    -   `strategy` - only `local` possible at the moment
-   `user`
    -   `_id` - object id
    -   `__v` - integer // version id
    -   `email` - email address used for login
    -   `password` - `bcrypt` encrypted password
    -   `nickname` - nickname used to be displayed in the system
    -   `name`? - fullname (for administration only)
    -   `position`? - position within this area
    -   `avatar`? - gravatar link as user icon
    -   `disabled`? - flag to disable user
    -   `roles_id` - list of role ids
    -   `roles` - list of roles with abilities
    -   `abilities` - combined abilities
    -   `createdAt` - date/time account created at
    -   `updatedAt` - date/time the record was last changed

## create - get a new JWT token

Create a new JWT token for the given user.

!!! example

    === "HTTP"

        ```bash
        POST {{SERVER}}/authentication
        Content-Type: application/json

        {
          "strategy": "local",
          "email": "{{email}}",
          "password": "{{password}}"
        }
        ```

    === "JavaScript"

        ```js
        app.service('authentication').create({
            "strategy": "local",
            "email": "demo@alinex.de",
            "password": "demo123"
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-auth -e demo@alinex.de -p demo123
        ```

    === "cURL"

        ```bash
        curl --request POST $SERVER/authentication \
             --header  'Content-Type: application/json' \
             --data    '{ "strategy": "local", "email": "demo@alinex.de", "password": "demo123" }'
        ```

    Will produce a response like:

    ```json
    {
        "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1ODIxNDQ4OTMsImV4cCI6MTU4MjIzMTI5MywiYXVkIjoiaHR0cHM6Ly9kZW1vLmFsaW5leC5kZSIsImlzcyI6ImFsaW5leC5kZSIsInN1YiI6IjQyOWVlNzEyNzYxMjJmNTVhM2E5NDc5NiIsImp0aSI6IjA2N2Q0Njk1LTE1NzYtNGY2OC04ZDQzLTVlMDM2MWM0MTQzYSJ9.Tu36BRMmcInyJAMFkqudljPngkHgCvFxwP0liffyfQA",
        "authentication": {
            "strategy": "local"
        },
        "user": {
            "_id": "429ee71276122f55a3a94796",
            "email": "demo@alinex.de",
            "nickname": "demo",
            "disabled": false,
            "createdAt": "Tue Feb 18 2020 23:24:06 GMT+0100 (CET)",
            "updatedAt": "Tue Feb 18 2020 23:24:06 GMT+0100 (CET)",
            "__v": 0,
            "name": "Demo User",
            "position": "Test",
            "avatar": "https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm",
            "roles_id": [
                "5b2025aedcb10b38733b9825"
            ],
            "abilities": [{
                "action": ["read", "update", "create", "remove"],
                "subject": ["users", "roles", "info", "checkup"],
                "fields": []
            }]
        }
    }
    ```

The process to do so is:

1. incoming call to authentication service
2. normalize email address (make all representation of the same mailbox the same)
3. find user record for local strategy
4. get full user record
5. also load roles and abilities
6. return result

## delete - revoke a JWT token

Revoke an earlier created JWT token before it gets invalid. But it is no need to do so, you may also forget it and it gets invalid after one day (maybe changed in configuration).

!!! example

    === "HTTP"

        ```bash
        DELETE {{SERVER}}/authentication
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('authentication').delete();
        ```

    === "CLI helper"

        ```bash
        bin/server-call -m delete /authentication
        ```

    === "cURL"

        ```bash
        curl --request DELETE $SERVER/authentication \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "revoked": true,
      "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MDQwNDk3MTAsImV4cCI6MTYwNDEzNjExMCwiYXVkIjoiaHR0cHM6Ly9kZW1vLmFsaW5leC5kZSIsImlzcyI6ImFsaW5leC5kZSIsInN1YiI6IjQyOWVlNzEyNzYxMjJmNTVhM2E5NDc5NiIsImp0aSI6IjlmMTZjOTllLTdkMDQtNDk2NS05ZDMwLWEyODAzMDMyMjQ5OCJ9.EZ_dpQjkfNohLv_irIeI3ILy-MDOAOJWt3CMGEZRcUg"
    }
    ```

If the token is already invalid or revoked an error will be thrown:

```json
HTTP/1.1 409 Conflict

{
  "code": 409,
  "message": "Conflict",
  "detail": "Conflict: This token was already revoked"
}
```

After revoking an access Token it is no longer possible to login on the server with it.

{!docs/assets/abbreviations.txt!}
