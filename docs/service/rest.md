# REST API

The server may be called through plain HTTP requests or using the JavaScript API (Feathers Client) through Socket.io. The following description shows both types.

| JavaScript | HTTP method | Path        | Typical Use                                               |
| ---------- | ----------- | ----------- | --------------------------------------------------------- |
| .find()    | GET         | /messages   | List all elements or search for them                      |
| .get()     | GET         | /messages/1 | Get one specific element by ID                            |
| .create()  | POST        | /messages   | Create a new element                                      |
| .update()  | PUT         | /messages/1 | Replace the element with a complete new set of attributes |
| .patch()   | PATCH       | /messages/1 | Change parts of an element                                |
| .remove()  | DELETE      | /messages/1 | Remove the element from store                             |

If a service is called the server will return the possible methods as response error:

    Allow: POST,DELETE

## Find

Retrieves a list of all matching resources from the service. It has a lot of possibilities to define the resulting dataset.

!!! example

    List all information entries.

    === "HTTP"

        ```bash
        GET {{SERVER}}/info
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('info').find();
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/info \
             --header "Authorization: Bearer $JWT"
        ```

    Will produce a response with a body like:

    ```json
    [
      {
        "group": "host",
        "name": "Architecture",
        "value": "x64"
      },
      ...
    ]
    ```

### Filtering

You can also send additional filtering constraints on specific field values.
To get only records with 'group' equal 'node':

!!! example

    List only info entries having `group=node`:

    === "HTTP"

        ```bash
        GET {{SERVER}}/info
            ?group=node
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('info').find({
            query: {
                group: 'node'
            }
        });
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/info -G \
             --data-urlencode 'group=node' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response with a body like:

    ```json
    [
        {
            "group": "node",
            "name": "process id",
            "value": 7615
        },
        {
            "group": "node",
            "name": "parent process id",
            "value": 7614
        },
        ...
    ]
    ```

You may also use any of the built-in find operands ($le, $lt, $ne, $eq, \$in, etc.) the general format supported in all service backends is as follows.

| Operand | Usage                         | Comment                                 |
| ------- | ----------------------------- | --------------------------------------- |
| -       | `field: value`                | Simple equality                         |
| `$in`   | `field: {$in: [value, ...]}`  | In list of values                       |
| `$nin`  | `field: {$nin: [value, ...]}` | Not in list of values                   |
| `$lt`   | `field: {$lt: value}`         | Field lower than given value            |
| `$lte`  | `field: {$lte: value}`        | Field lower or equal than given value   |
| `$gt`   | `field: {$gt: value}`         | Field greater than given value          |
| `$gte`  | `field: {$gte: value}`        | Field greater or equal than given value |
| `$ne`   | `field: {$ne: value}`         | Field is not equal value                |
| `$or`   | `$or: [ exp, ...]`            | Joins query clauses with a logical NOR  |

(Mongoose operands)[https://mongoosejs.com/docs/api/query.html]:

| Operand   | Usage                    | Comment                                               |
| --------- | ------------------------ | ----------------------------------------------------- |
| `$eq`     | `field: {$eq: value}`    | Field is equal value                                  |
| `$and`    | `$and: [ exp, ...]`      | Joins query clauses with a logical AND                |
| `$not`    | `$not: exp`              | Inverts the effect of a query expression              |
| `$nor`    | `$nor: [ exp, ...]`      | Joins query clauses with a logical OR                 |
| `$exists` | `field: {$exists: bool}` | Matches documents that have the specified field       |
| `$type`   | `field: {$type: value}`  | Selects documents if a field is of the specified type |

Some other Mongoose types which has to be whitelisted in service setup to be able to use:

| Operand     | Usage                                | Comment                                                |
| ----------- | ------------------------------------ | ------------------------------------------------------ |
| `$regexp`   | `field: {$regex: /pattern/options}`  | Match a specified regular expression                   |
| `$all`      | `field: {$all: [value,...]}`         | Matches arrays that contain all elements               |
| `$where`    | `field: {$where: javascript}`        | Matches documents that satisfy a JavaScript expression |
| `$populate` | `field: {$populate: sub_collection}` | Populate other collections                             |

Knex operands:

| Operand    | Usage                        | Comment                                                                                 |
| ---------- | ---------------------------- | --------------------------------------------------------------------------------------- |
| `$and`     | `$and: [ exp, ...]`          | Find all records that matches all criteria                                              |
| `$like`    | `field: {$like: pattern}`    | Find all records where the value matches the given string pattern (% as wildcard)       |
| `$notlike` | `field: {$notlike: pattern}` | Find all records where the value doesn't match the given string pattern (% as wildcard) |
| `$ilike`   | `field: {$ilike: pattern}`   | Match case insensitive (only on postgres)                                               |

For example, to find all records which are not in group 'node':

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/info
            ?name[$ne]=node
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('info').find({
            query: {
                name: {
                    $ne: 'node'
                }
            }
        });
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/info -G \
             --data-urlencode 'name[$ne]=node' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response with body like:

    ```json
    [
        {
            "group": "host",
            "name": "architecture",
            "value": "x64"
        },
        {
            "group": "host",
            "name": "cpu type",
            "value": "Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz"
        },
        ...
    ]
    ```

You can also specify multiple fields which have to be all matching the defined parameters.
That's like an _and_ search. To make alternative groups of restrictions use `$or`:

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/info
            ?$or[0][group][$ne]=node
            &$or[1][name]=cpu
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('info').find({
            query: {
                $or: [
                    { group: { $ne: 'node' } },
                    { name: 'cpu' }
                ]
            }
        });
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/info -G \
             --data-urlencode '$or[0][group][$ne]=node' \
             --data-urlencode '$or[1][name]=cpu' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will producre a response like:

    ```json
    [
        {
            "group": "host",
            "name": "architecture",
            "value": "x64"
        },
        {
            "group": "host",
            "name": "cpu type",
            "value": "Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz"
        },
        ...
    ]
    ```

### Limit

Most services support pagination here with the additional parameters. If nothing is
given the preconfigured default will be used.

`$limit` will return only the number of results you specify.
Retrieves the first ten values:

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/info
            ?$limit=2
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('info').find({
            query: {
                $limit: 2
            }
        });
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/info -G \
             --data-urlencode '$limit=2' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    [
        {
            "group": "host",
            "name": "architecture",
            "value": "x64"
        },
        {
            "group": "host",
            "name": "cpu type",
            "value": "Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz"
        }
    ]
    ```

If you want to get only the number of records you may also set `$limit` to `0`.
This ensures that no record is retrieved but you get the meta info like totalmemin the returned page object.

### Offset (skip)

`$skip` will skip the specified number of results.

!!! example

    If you skip 2 records the result will start with record number 3, so to show record 3 and 4:

    === "HTTP"

        ```bash
        GET {{SERVER}}/info
            ?$limit=2
            &$skip=2
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('info').find({
            query: {
                $limit: 2,
                $skip: 2
            }
        });
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/info -G \
             --data-urlencode '$limit=2' \
             --data-urlencode '$skip=2' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    [
        {
            "group": "host",
            "name": "cpu cores",
            "value": 4
        },
        {
            "group": "host",
            "name": "cpu speed (MHz)",
            "value": 2730
        }
    ]
    ```

### Sorting

`$sort` will sort based on the object you provide. It can contain a list of properties
by which to sort mapped to the order (1 ascending, -1 descending).

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/info
            ?$sort[name]=1
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('info').find({
            query: {
                $sort: {
                    name: 1
                }
            }
        });
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/info -G \
             --data-urlencode '$sort[name]=1' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    [
        {
            "group": "env",
            "name": "COLORTERM",
            "value": "truecolor"
        },
        {
            "group": "env",
            "name": "DBUS_SESSION_BUS_ADDRESS",
            "value": "unix:abstract=/tmp/dbus-NdF5Pg7sjt,guid=e0a8fb125f75f2636fe7bdf45c912466"
        },
        ...
    ]
    ```

### Select columns

`$select` allows to pick which fields to include in the result.

!!! example

    To only retrieve the fields 'name' and 'value' but not the group call:

    === "HTTP"

        ```text
        GET {{SERVER}}/info
            ?$select[]=name
            &$select[]=value
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('info').find({
            query: {
                $select: ['name', 'value']
            }
        });
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/info -G \
             --data-urlencode '$select[]=name' \
             --data-urlencode '$select[]=value' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    [
    {
        "name": "architecture",
        "value": "x64"
    },
    {
        "name": "cpu type",
        "value": "Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz"
    },
    {
        "name": "cpu cores",
        "value": 4
    },
    ...
    ]
    ```

## Get

Retrieve a single resource from the service by its ID:

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup/host.load
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').get('host.load');
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup/host.load \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "host.load": {
        "name": "host.load",
        "title": "System Load",
        "description": "This test will check the current localhost server CPU load values.",
        ...
      },
      "_status": "OK",
      "_summary": {
        "total": 1,
        "OK": 1
      }
    }
    ```

### Select properties

`$select` allows to pick which fields to include in the record:

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup/host.load
            ?$select[]=_status
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').get('host.load', {
            query: {
                $select: ['message']
            }
        });
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup/host.load -G \
             --data-urlencode '$select[]=_status' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "_status": "OK"
    }
    ```

## Create

Create a new resource with data.

!!! example

    === "HTTP"

        ```bash
        POST {{SERVER}}/messages
        Authorization: Bearer {{JWT}}
        Content-Type: application/json

        [
            { "text": "I really have to iron" }
        ]
        ```

    === "JavaScript"

        ```js
        app.service('messages').create({
            text: 'I really have to iron'
        });
        ```

    === "cURL"

        ```bash
        curl --request POST $SERVER/messages \
             --header  "Authorization: Bearer $JWT" \
             --header  "Content-type: application/json" \
             --data    '[{ "text": "I really have to iron" }]'
        ```

### Create multiple

You may also create multiple records in one call:

!!! example

    === "HTTP"

        ```bash
        POST {{SERVER}}/messages
        Authorization: Bearer {{JWT}}
        Content-Type: application/json

        [
            { "text": "I really have to iron" },
            { "text": "Do laundry" }
        ]
        ```

    === "JavaScript"

        ```js
        app.service('messages').create([
            { text: 'I really have to iron' },
            { text: 'Do laundry' }
        ]);
        ```

    === "cURL"

        ```bash
        curl --request POST $SERVER/messages \
             --header  "Authorization: Bearer $JWT" \
             --header  "Content-type: application/json" \
             --data    '[{ "text": "I really have to iron" }, { "text": "Do laundry" }]'
        ```

## Update

Completely replace a single or multiple resources.

Given an ID the specified record will be replaced:

!!! example

    === "HTTP"

        ```bash
        PUT {{SERVER}}/messages/2
        Authorization: Bearer {{JWT}}
        Content-Type: application/json

        [
            { "text": "I really have to do laundry" }
        ]
        ```

    === "JavaScript"

        ```js
        app.service('messages').update(2, {
            text: 'I really have to do laundry'
        });
        ```

    === "cURL"

        ```bash
        curl --request PUT $SERVER/messages/2 \
             --header  "Authorization: Bearer $JWT" \
             --header  "Content-type: application/json" \
             --data    '{ "text": "I really have to do laundry" }'
        ```

## Patch

Merge the existing data of a single or multiple resources with the new data.

!!! example

    === "HTTP"

        ```bash
        PATCH {{SERVER}}/messages/2
        Authorization: Bearer {{JWT}}
        Content-Type: application/json

        { "read": true }
        ```

    === "JavaScript"

        ```js
        app.service('messages').patch(2, { read: true });
        ```

    === "cURL"

        ```bash
        curl --request PATCH $SERVER/messages/2 \
             --header  "Content-type: application/json" \
             --header  "Authorization: Bearer $JWT" \
             --data    '{ read: true }'
        ```

### Patch multiple

When no id is given patch all records or select some using query parameters:

!!! example

    === "HTTP"

        ```bash
        PATCH {{SERVER}}/messages
              ?complete=false
        Authorization: Bearer {{JWT}}
        Content-Type: application/json

        { "complete": true }
        ```

    === "JavaScript"

        ```js
        app.service('messages').patch(null,
            { read: true },
            { query: { complete: true } }
        );
        ```

    === "cURL"

        ```bash
        curl --request PATCH $SERVER/messages?complete=false \
             --header  "Authorization: Bearer $JWT" \
             --header  "Content-Type: application/json" \
             --data    '{ "complete": true }'
        ```

See the description for filtering in the `find` method above.

## Remove

Remove a single or multiple resources.

If an id is given only this will be removed.

!!! example

    === "HTTP"

        ```bash
        DELETE {{SERVER}}/messages/2
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('messages').remove(2);
        ```

    === "cURL"

        ```bash
        curl --request DELETE $SERVER/messages/2 \
             --header  "Authorization: Bearer $JWT"
        ```

### Remove multiple

But you may also call it without an id and a filter definition:

!!! example

    === "HTTP"

        ```bash
        DELETE {{SERVER}}/messages
               ?read=true
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('messages').remove(null,
            { query: { read: true } }
        );
        ```

    === "cURL"

        ```bash
        curl --request PATCH $SERVER/messages -G \
             --data-urlencode '{ "read": true }' \
             --header  "Authorization: Bearer $JWT"
        ```

See the description for filtering in the `find` method above.

## Custom Actions

To keep everything clear custom actions should not be added and also no nested routes like `user/:id/sendMessage`. Let the structure be clean and only at the standard this also helps in working with web sockets and the routing.

Actions to be taken should be added using one of the default methods and should be defined with it's data section:

-   `PATCH` may be used to run actions which may change the object(s).
-   `GET` may be used for specific reporting.

To detect special actions which are not direct data changes it should be identified using the `__action` selector. This can be used in a conditional case within the handler.

## Response Format

The REST interface allows to send the resulting data in different formats depending on the accept header. The following formats are supported:

-   `application/json`
-   `application/bson`
-   `application/x-msgpack`
-   `application/javascript`
-   `text/x-coffeescript`
-   `text/x-yaml`
-   `text/x-ini`
-   `text/x-java-properties`
-   `text/x-toml`
-   `application/xml`
-   `text/csv`
-   `application/vnd.ms-excel`
-   `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`
-   `text/plain`

The default response type will be `application/json` if nothing is specified.

!!! example

    Retrieve info as XML:

    ```bash
    curl -sX GET http://localhost:3030/info # default application/json
    curl -sX GET http://localhost:3030/info -H 'Accept: text/csv'
    ```
    === "HTTP"

        ```bash
        GET {{SERVER}}/info
        Authorization: Bearer {{JWT}}
        Accept: application/xml
        ```

    === "cURL"

        ```bash
        curl --request PATCH $SERVER/info \
             --header  "Authorization: Bearer $JWT" \
             --header  "Accept: application/xml"
        ```

    Will produce a response with a body like:

    ```xml
    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <data>
      <entry>
        <group>host</group>
        <name>Architecture</name>
        <value>x64</value>
      </entry>
      <entry>
        <group>host</group>
        <name>CPU Type</name>
        <value>Intel(R) Core(TM) i7-7500U CPU @ 2.70GHz</value>
      </entry>
    </data>
    ```

## Errors

As an example we call a service which didn't implement the GET method, only the FIND method:

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}//info/1
        Accept: application/json
        ```

    === "JavaScript"

        ```js
        app.service('info').get(1);
        ```

    === "cURL"

        ```text
        curl --request PATCH $SERVER/info \
             --header  "Accept: application/json"
        ```

    Will produce a response like:

    ```json
    {
      "code": 404,
      "message": "Page not Found",
      "detail": "Please verify your address/link."
    }
    ```

    This will be displayed using json like above or as a html page in the browser (depending on accept header).

{!docs/assets/abbreviations.txt!}
