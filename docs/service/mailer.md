title: Mailer

# Mailer Service (internal)

The internal mailer service is used to send emails.

## Model

The user has the following fields:

-   `from` - from email address (optional)
-   `to` - address to send mail to
-   `subject` - for the outgoing message
-   `html` - this is the email body

## create - new email

!!! warning

    This is not ready to use, yet!

!!! security

    Only to be used internally.

As this can only be used internally you may send a email as follows:

```ts
app.service('mailer').create(email).then(function (result) {
  console.log('Sent email', result);
}).catch(err => {
  console.log(err);
});
```

{!docs/assets/abbreviations.txt!}
