title: Overview

# Services

This is a detailed documentation with described examples but you may also use the integrated Swagger interface for a short overview of the methods and to test them out.

See the [client](../usage/client.md) description on how to access the Server in different ways.

In the examples here the typical use is shown using different tools like pure HTTP using VS Code Rest Client, internal Helper, Curl or JavaScript Call.

{!docs/assets/abbreviations.txt!}
