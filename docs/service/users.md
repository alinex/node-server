title: Users

# User Service

The user service will be used internally by the authentication service to check for authenticated users.
Also it is used for user management through the client application.

!!! info

    This service isbased on the [REST API](rest.md). See the documentation there for all the possbilities on how to filter, sort, search and select the output fotmat.

## Model

The user has the following fields:

-   `_id` - object id
-   `email` - email address used for login
-   `password` - `bcrypt` encrypted password
-   `autologin` - array with IP addresses or CIDRs
-   `nickname` - nickname used to be displayed in the system
-   `name`? - fullname (for administration only)
-   `position`? - position within this area
-   `avatar`? - gravatar link as user icon
-   `disabled`? - flag to disable user
-   `roles_id` - list of roles
-   `createdAt` - date/time account created at
-   `updatedAt` - date/time the record was last changed
-   `__v` - integer // version id

## find - list users

A list of users will be retrieved, containing the base data of all of them.

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/users
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('users').find();
        ```

    === "CLI helper"

        ```bash
        bin/server-call /users
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/users \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "total": 4,
      "limit": 10,
      "skip": 0,
      "data": [
        {
          "_id": "429ee71276122f55a3a94796",
          "email": "demo@alinex.de",
          "nickname": "demo",
          "disabled": false,
          "createdAt": "Fri Aug 07 2020 16:37:36 GMT+0200 (CEST)",
          "updatedAt": "Fri Aug 07 2020 16:37:36 GMT+0200 (CEST)",
          "name": "Demo User",
          "position": "Test",
          "avatar": "https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm",
          "roles_id": [
            "5b2025aedcb10b38733b9825"
          ]
        },
        ...
      ]
    }
    ```

## get - user record

Get user information for a specific user. This will include the resolved abilities through the user's roles.

Therefore it contains more information as a find.

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/users/429ee71276122f55a3a94796
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('users').get('429ee71276122f55a3a94796');
        ```

    === "CLI helper"

        ```bash
        bin/server-call /users/429ee71276122f55a3a94796
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/users/429ee71276122f55a3a94796 \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "_id": "429ee71276122f55a3a94796",
      "email": "demo@alinex.de",
      "nickname": "demo",
      "disabled": false,
      "createdAt": "Fri Aug 07 2020 16:37:36 GMT+0200 (CEST)",
      "updatedAt": "Fri Aug 07 2020 16:37:36 GMT+0200 (CEST)",
      "name": "Demo User",
      "position": "Test",
      "avatar": "https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm",
      "roles_id": [
        "5b2025aedcb10b38733b9825"
      ],
      "roles": [
        {
          "_id": "5b2025aedcb10b38733b9825",
          "name": "Admin",
          "description": "Super Administrator with maximum rights",
          "abilities": [
            {
              "action": [
                "read",
                "update",
                "create",
                "remove"
              ],
              "subject": [
                "users",
                "roles",
                "info",
                "checkup"
              ],
              "fields": [],
              "_id": "5f986adef918c7137dbd756f",
              "inverted": false
            }
          ],
          "createdAt": "2018-06-12T19:57:34.027Z",
          "updatedAt": "2020-10-27T18:45:50.833Z",
          "disabled": false
        }
      ]
    }
    ```

## create - new user

Create new user.

!!! example

    === "HTTP"

        ```bash
        POST {{SERVER}}/users
        Authorization: Bearer {{JWT}}
        Content-Type: application/json

        {
            "email" : "demo@alinex.de",
            "password" : "demo123",
            "nickname" : "demo",
            "disabled": false,
            "name" : "Demo User",
            "position" : "Test"
        }
        ```

    === "JavaScript"

        ```js
        app.service('users').create({
            email: "demo@alinex.de",
            password: "demo123",
            nickname: "demo",
            disabled: false,
            name: "Demo User",
            position: "Test"
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call -m POST /users \
            -d '{
                "email" : "demo1@alinex.de",
                "password" : "demo123",
                "nickname" : "demo1",
                "disabled": false,
                "name" : "Demo User 1",
                "position" : "Test"
            }'
        ```

    === "cURL"

        ```bash
        curl --request POST $SERVER/users \
             --header  "Authorization: Bearer $JWT" \
             --header  'Content-Type: application/json' \
             --data    '{
                "email" : "demo@alinex.de",
                "password" : "demo123",
                "nickname" : "demo",
                "disabled": false,
                "name" : "Demo User",
                "position" : "Test"
            }'
        ```

    Will produce a result like:

    ```json
    {
        roles_id: [],
        _id: "5e4ec281b3fbcb6b1024e345",
        email: "demo1@alinex.de",
        nickname: "demo1",
        disabled: false,
        name: "Demo User 1",
        position: "Test",
        avatar: "https://www.gravatar.com/avatar/920b31bfc299ab42211370364e2fe603?s=60&d=mm",
        createdAt: new Date("2020-02-20T17:31:45.775Z"),
        updatedAt: new Date("2020-02-20T17:31:45.775Z"),
        __v: 0
    }
    ```

## update - replace record

Update a complete record with new settings.

!!! example

    === "HTTP"

        ```bash
        PUT {{SERVER}}/users/429ee71276122f55a3a94796
        Authorization: Bearer {{JWT}}
        Content-Type: application/json

        {
            "email" : "demo@alinex.de",
            "password" : "demo123",
            "nickname" : "demo",
            "disabled": false,
            "name" : "Demo User",
            "position" : "Test"
        }
        ```

    === "JavaScript"

        ```js
        app.service('users').update('429ee71276122f55a3a94796',
        {
            email: "demo@alinex.de",
            password: "demo123",
            nickname: "demo",
            disabled: false,
            name: "Demo User",
            position: "Test"
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call -m PUT /users/429ee71276122f55a3a94796 \
            -d '{
                "email" : "demo@alinex.de",
                "password" : "demo123",
                "nickname" : "demo",
                "disabled": false,
                "name" : "Demo User",
                "position" : "Test"
            }'
        ```

    === "cURL"

        ```bash
        curl --request PUT $SERVER/users/429ee71276122f55a3a94796 \
             --header  "Authorization: Bearer $JWT" \
             --header  'Content-Type: application/json' \
             --data    '{
                "email" : "demo@alinex.de",
                "password" : "demo123",
                "nickname" : "demo",
                "disabled": false,
                "name" : "Demo User",
                "position" : "Test"
            }'
        ```

    Will produce a response like:

    ```json
    { _id: "429ee71276122f55a3a94796" }
    ```

## patch - change some values

Update only some fields.

!!! example

    === "HTTP"

        ```bash
        PATCH {{SERVER}}/users/5e4c59fad2fd58577a9eca15
        Authorization: Bearer {{JWT}}
        Content-Type: application/json

        { "nickname" : "demo" }
        ```

    === "JavaScript"

        ```js
        app.service('users').patch('5e4c59fad2fd58577a9eca15', { nickname: "demo" });
        ```

    === "CLI helper"

        ```bash
        bin/server-call -m PATCH /users/5e4c59fad2fd58577a9eca15 \
            -d '{ "nickname" : "demo" }'
        ```

    === "cURL"

        ```bash
        curl --request PATCH $SERVER/users/5e4c59fad2fd58577a9eca15 \
             --header  "Authorization: Bearer $JWT" \
             --header  'Content-Type: application/json' \
             --data    '{ "nickname" : "demo" }'
        ```

    Will produce a response like:

    ```json
    { _id: "5e4c59fad2fd58577a9eca15" }
    ```

## remove - user

!!! example

    === "HTTP"

        ```bash
        DELETE {{SERVER}}/users/5e4c59fad2fd58577a9eca15
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('users').delete('5e4c59fad2fd58577a9eca15');
        ```

    === "CLI helper"

        ```bash
        bin/server-call -m DELETE /users/5e4c59fad2fd58577a9eca15
        ```

    === "cURL"

        ```bash
        curl --request DELETE $SERVER/users/5e4c59fad2fd58577a9eca15 \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    { _id: "429ee71276122f55a3a94796" }
    ```

{!docs/assets/abbreviations.txt!}
