title: Info

# Info Service

The info service is used to get some information about the server. This may be used for analysis.

The following info groups are defined:

| Check    | Description                   |
| -------- | ----------------------------- |
| host     | Hardware or virtual machine   |
| os       | Operating System              |
| user     | User process is running under |
| env      | Environment settings          |
| node     | NodeJS                        |
| server   | Web Server                    |
| service  | Services                      |
| database | Database                      |

!!! info

    This service isbased on the [REST API](rest.md). See the documentation there for all the possbilities on how to filter, sort, search and select the output fotmat.

## Model

The values returned is a List of elements containing:

-   **group** - a short name for the group
-   **name** - short name for the element within the group
-   **value** - value of this element

## find - run all checks

List all or some specific values.

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/info
            ?group=server
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('info').find({
            query: {
                group: 'server'
            }
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call /info?group=server
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/info -G \
             --data    'group=server' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    [
        {
            "group": "server",
            "name": "version",
            "value": "0.3.2"
        },
        {
            "group": "server",
            "name": "name",
            "value": "@alinex/server"
        },
        {
            "group": "server",
            "name": "feathers",
            "value": "4.5.1"
        },
        {
            "group": "server",
            "name": "author",
            "value": "Alexander Schilling <info@alinex.de>"
        },
        {
            "group": "server",
            "name": "copyright"
        }
    ]
    ```

Call it without a filter to see all values or filter on any other property.

## get - retrieve one group or one value

URI: `/info/<group>.<name>`

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/info/server.version
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('info').get('server.version');
        ```

    === "CLI helper"

        ```bash
        bin/server-call /info/server.version
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/info/server.version \
             --header  "Authorization: Bearer $JWT"
        ```

    ```json
    "0.3.2"
    ```

{!docs/assets/abbreviations.txt!}
