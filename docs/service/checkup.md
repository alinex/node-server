title: Checkup

# Checkup Service

The checkup service is used to test that everything works fine and the server can be used.

The server has a list of checkup defined which will run with timing and it's results be presented back to be easily analyzed for manual check or monitoring tools like Nagios.

!!! warning "Security"

    The checkup service is fully accessible from local IP without authentication. Or with authentication from anywhere.

The configuration has to be done in `config/checkup/*.yml`:
-   `default.yml` contains cases which are run if no config parameter is given
-   `selcheck.yml` is also used on startup
-   more setups may be created

The following checkups are defined in the selfcheck:

| Check            | Description                                                                                                        |
| ---------------- | ------------------------------------------------------------------------------------------------------------------ |
| host.uptime      | Server should be up for more than 5 Minutes. If not this is a warning to get you informed about the previous boot. |
| host.load        | The load should not be too high to have a well performing and working system.                                      |
| host.memory      | Low available memory and free swap memory indicate to an overloaded and nonperforming system.                      |
| host.disk        | Check space on all disks.                                                                                          |
| host.logs        | Check the free space for log files.                                                                                |
| services.mongodb | The mongo database should be running and listening on port.                                                        |

!!! info

    This service is based on the [REST API](rest.md). See the documentation there for all the possbilities on how to filter, sort, search and select the output fotmat.

## Model

The values returned may be different based on the used report format and options. But they are based on the following default format:

A map of checkup with meta data:

-   **name**
-   **title**
-   **description**
-   **test** class used for test
-   **data** specific test settings
-   **parent** path with parent names
-   **tags**
-   **cache** seconds to keep value
-   **fix** specific fix settings
-   **autofix** is repair allowed

And the real results:

-   **start**
-   **status** OK, WARN, ERROR, NODATA
-   **error**
-   **result** object with detailed values
-   **fixes** list of fixes
-   **log** detailed log depending on verbode level

## find - run all checkup

Run a complete check to find any problems.

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').find();
        ```

    === "CLI helper"

        ```bash
        bin/server-call /checkup
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup -G \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "host.uptime": {
        ...
      },
      "host.load": {
        ...
      },
      ...
      "host.daemon": {
        ...
      }
    }
    ```

## get - run test or group

If the name of a group is given all tests within will be run.

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup/host.uptime
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').get('host.uptime');
        ```

    === "CLI helper"

        ```bash
        bin/server-call /checkup/host.uptime
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup/host.uptime \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "host.uptime": {
        "name": "host.uptime",
        "title": "Host Startup Time",
        "description": "This test will check if the localhost server is started within the last minutes, maybe there was a crash or some changes made.",
        "test": "./linux/uptime",
        "data": {
          "warn": {
            "age": "5 min"
          }
        },
        "parent": [
          "",
          "host",
          "host.uptime"
        ],
        "tags": [
          "linux",
          "uptime"
        ],
        "cache": 50,
        "fix": {},
        "autofix": false,
        "status": "OK",
        "result": {
          "boot": "2020-10-30T17:24:45.000Z",
          "age": 17456
        },
        "start": "2020-10-30T22:15:41.031Z",
        "log": "",
        "fixes": []
      },
      "_status": "OK",
      "_summary": {
        "total": 1,
        "OK": 1
      }
    }
    ```

!!! info

    If you use multiple case files you can set them directly by putting the basename before the path:

    ```bash
    # GET {{SERVER}}/checkup/{{case-config}}:{{path}}
    GET {{SERVER}}/checkup/test:host.uptime
    # is the same as
    GET {{SERVER}}/checkup/host.uptime?cases=test
    GET {{SERVER}}/checkup?cases=test&paths[]=host.uptime    
    ```

To make calling tests from other programms or systems really simple another shortcut exists: 

```bash
# GET {{SERVER}}/checkup/{{case-config}}:{{path}}/{{reporter}}
GET {{SERVER}}/checkup/test:host.uptime/status
# is the same as
GET {{SERVER}}/checkup/host.uptime?cases=test&reporter=status
GET {{SERVER}}/checkup?cases=test&paths[]=host.uptime&reporter=status
```

## patch - run with autofix

If the name of a group is given all tests within will be run.

!!! example

    === "HTTP"

        ```bash
        PATCH {{SERVER}}/checkup/host.uptime
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').patch('host.uptime');
        ```

    === "CLI helper"

        ```bash
        bin/server-call -m PATCH /checkup/host.uptime
        ```

    === "cURL"

        ```bash
        curl --request PATCH $SERVER/checkup/host.uptime \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "host.uptime": {
        "name": "host.uptime",
        "title": "Host Startup Time",
        "description": "This test will check if the localhost server is started within the last minutes, maybe there was a crash or some changes made.",
        "test": "./linux/uptime",
        "data": {
          "warn": {
            "age": "5 min"
          }
        },
        "parent": [
          "",
          "host",
          "host.uptime"
        ],
        "tags": [
          "linux",
          "uptime"
        ],
        "cache": 50,
        "fix": {},
        "autofix": false,
        "status": "OK",
        "result": {
          "boot": "2020-10-30T17:24:45.000Z",
          "age": 17456
        },
        "start": "2020-10-30T22:15:41.031Z",
        "log": "",
        "fixes": []
      },
      "_status": "OK",
      "_summary": {
        "total": 1,
        "OK": 1
      }
    }
    ```

## Parameters

These parameters are mostly possible on both, find and get.

### Case Config

If multiple case configurations exist it can be selected with the `cases` parameter. The default is to use `default` configuration which is defined in `config/default.yml`.

!!! note

    This is only possible in FIND requests, because a GET request contains one specific path which is used.

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup
            ?cases=selfcheck
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').find({
            query: {
                cases: 'selfcheck'
            }
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call /checkup?reporter=status
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup -G \
             --data    'cases=selfcheck' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "host.uptime": {
        ...
      },
      "host.load": {
        ...
      }
    }
    ```

### Paths

The `paths` parameter can contain a single path or array of paths to execute.

!!! note

    This is only possible in FIND requests, because a GET request contains one specific path which is used.

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup
            ?paths[]=host.uptime&paths[]=host.load
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').find({
            query: {
                paths: ['host.uptime', 'host.load']
            }
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call /checkup?paths[]=host.uptime&paths[]=host.load
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup -G \
             --data    'paths[]=host.uptime&paths[]=host.load' \
             --header  "Authorization: Bearer $JWT"
        ```

### Tags

The `tags` parameter can contain a single tag or array of tags wo select for execution.

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup
            ?tags[]=linux&tags[]=load
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').find({
            query: {
                tags: ['linux', 'load']
            }
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call /checkup?tags[]=linux&tags[]=load
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup -G \
             --data    'tags[]=linux&tags[]=load' \
             --header  "Authorization: Bearer $JWT"
        ```

### Verbosity

You can define the `verbose` level like defined in the [Checkup](https://alinex.gitlab.io/node-checkup/usage/api/#verbosity) as an integer between 0..9.

### Extend Verbosity

The `extend` flag will extend verbosity only on failure and only for the test which failed, if set to `true`.

### Analyzation Run

An `analyze` flag used to run additional analyzation, if set to `true`.

### Reporter

The reporter can be selected from one of the following by adding the `reporter` setting.

#### Status report

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup
            ?reporter=status
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').find({
            query: {
                reporter: 'status'
            }
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call /checkup?reporter=status
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup -G \
             --data    'reporter=status' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "status": "OK",
      "error": ""
    }
    ```

All checkup are done here, too. But only the overall results with the collected errors will be returned.

This is a very good report to be used from monitoring like Nagios which will check for `"status": "OK"`. Or with request header `Àccept: application/xml` check for `<status>OK</status>`.

#### List report

This is the default if no reporter is set.

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').find();
        ```

    === "CLI helper"

        ```bash
        bin/server-call /checkup
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "host.uptime": {
        "name": "host.uptime",
        "title": "Host Startup Time",
        "description": "This test will check if the localhost server is started within the last minutes, maybe there was a crash or some changes made.",
        "test": "./linux/uptime",
        "data": {
          "warn": {
            "age": "5 min"
          }
        },
        "parent": [
          "",
          "host",
          "host.uptime"
        ],
        "tags": [
          "linux",
          "uptime"
        ],
        "cache": 50,
        "fix": {},
        "autofix": false,
        "status": "OK",
        "result": {
          "boot": "2020-10-30T17:24:45.000Z",
          "age": 12705
        },
        "start": "2020-10-30T20:56:30.343Z",
        "log": "",
        "fixes": []
      },
      ...
    }
    ```

#### Group report

In this report the tests are structured within group elements. Each group contains:

-   **tests** object with tests directly contained
-   **sub** sub groups object
-   **status** overall status which is the most critical value
-   **summary** number of results within each status

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup
            ?reporter=group
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').find({
            query: {
                reporter: 'group'
            }
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call /checkup?reporter=status
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup -G \
             --data    'reporter=group' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```json
    {
      "tests": {},
      "sub": {
        "host": {
          "name": "host",
          "title": "Local machine",
          "parent": [
            "",
            "host"
          ],
          "tags": [],
          "data": {},
          "cache": 50,
          "fix": {},
          "autofix": false,
          "tests": {
            "host.uptime": {
              "name": "host.uptime",
              "title": "Host Startup Time",
              "description": "This test will check if the localhost server is started within the last minutes, maybe there was a crash or some changes made.",
              "test": "./linux/uptime",
              "data": {
                "warn": {
                  "age": "5 min"
                }
              },
              "parent": [
                "",
                "host",
                "host.uptime"
              ],
              "tags": [ "linux", "uptime" ],
              "cache": 50,
              "fix": {},
              "autofix": false,
              "status": "OK",
              "result": {
                "boot": "2020-10-30T17:24:45.000Z",
                "age": 15018
              },
              "start": "2020-10-30T21:35:03.711Z",
              "log": "",
              "fixes": []
            },
            ...
      },
      "status": "OK",
      "summary": {
        "total": 6,
        "OK": 6
      }
    }
    ```

#### Console report

This will return color coded text to be displayed on a linux console.

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup
            ?reporter=console
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').find({
            query: {
                reporter: 'console'
            }
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call /checkup?reporter=console
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup -G \
             --data    'reporter=console' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```text
    [32mOK [1mLocal machine[22m (host)[39m
        [32m✔   OK [1mHost Startup Time[22m (host.uptime)[39m
        [32m✔   OK [1mSystem Load[22m (host.load)[39m
        [32m✔   OK [1mSystem Memory[22m (host.memory)[39m
        [32m✔   OK [1mDisks[22m (host.disk)[39m
        [32m✔   OK [1mSpace for Logs[22m (host.logs)[39m
    [32mOK [1mExternal Services[22m (services)[39m
        [32m✔   OK [1mMongoDB Daemon[22m (services.mongodb)[39m

    6 test cases for all paths
    [32m6 test cases succeeding[39m
    ```

#### HTML report

This will return a simple formatted HTML page and is best used within the browser. This is also the default if called from a web browser.

!!! example

    === "HTTP"

        ```bash
        GET {{SERVER}}/checkup
            ?reporter=html
        Authorization: Bearer {{JWT}}
        ```

    === "JavaScript"

        ```js
        app.service('checkup').find({
            query: {
                reporter: 'html'
            }
        });
        ```

    === "CLI helper"

        ```bash
        bin/server-call /checkup?reporter=html
        ```

    === "cURL"

        ```bash
        curl --request GET $SERVER/checkup -G \
             --data    'reporter=html' \
             --header  "Authorization: Bearer $JWT"
        ```

    Will produce a response like:

    ```html
    <html><head>
      </head><title>Checkup: Alle </title>
    </head>
    <body>
    <h1>Checkup: Alle </h1>  
    <h3>Zusammenfassung:</h3>
      <table><tr><td>OK</td><td>1 Test</td><td>OK</td></tr><tr><td></td><td>1 Test</td><td>total</td></tr></table>
    <h3>Details:</h3>
      <table>
        <tr title="Prüft ob die Homepage mit 'Onleihe Version' oder 'Digitale Medien' angezeigt wird."><th>OK</th><td><b>Homepage</b> (dvb.user.service.onleihe-de-tst-01.home)</td></tr>
      </table>
    </body></html>
    ```

{!docs/assets/abbreviations.txt!}
