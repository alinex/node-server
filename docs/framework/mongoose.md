# Mongo DB

## Identification

The `_id` field is primary key for every document. These automatically generated ObjectIDs start with a timestamp and are automatically indexed in every collection so it's possible to sort your documents automatically.

> A BSON ObjectID is a 12-byte value consisting of a **4-byte timestamp** (seconds since epoch), a **3-byte machine id**, a **2-byte process id**, and a **3-byte counter**.
> Note that the timestamp and counter fields must be stored big endian unlike the rest of BSON.

## Styleguide

To make the database itself more readable and understandable we use the following styleguide. It is based on best practice and common use.

### Database

-   **Lowercase**: avoids case sensitivity issues.
-   **CamelCase**: use camelCase like `userNewDB`
-   **Singular**: names should be sungular in contrast to collections
-   **Postfix**: append `DB` on the end of name.

As an example use `alinexDB`.

### Collections

-   **Lowercase**: avoids case sensitivity issues.
-   **Plural**: more obvious to label a collection of something as the plural, e.g. "files" rather than "file"
-   **Dot notation** for higher detail collections: Gives some indication to how collections are related. For example you can be reasonably sure you could delete "users.pagevisits" if you deleted "users", provided the people that designed the schema did a good job.
-   **CamelCase**: alternative to categorization using dot notation

### References

For one-to-one relations the name always follows the structure `<document>_id`.

For lists use the plural form `<document>s_id`.

## Services

To connect to Mongo DB we use the [mongoose](http://mongoosejs.com/) ORM system.

For a mongo DB based service you need a model like `src/models/users.ts`:

```ts
export default function(app: Application) {
    const mongooseClient = app.get('core-mongoose');
    const users = new mongooseClient.Schema(
        {
            // _id
            // __v version id
            email: { type: String, unique: true, required: true },
            password: { type: String, required: true },
            nickname: { type: String, unique: true, required: true },
            name: { type: String },
            position: { type: String },
            avatar: { type: String },
            disabled: { type: Boolean },
            roles_id: [{ type: mongooseClient.Schema.Types.ObjectId, ref: 'roles', required: true }]
        },
        {
            // createdAt
            // updatedAt
            timestamps: true
        }
    );

    return mongooseClient.model('users', users);
}
```

See the possible Schema definitions in the [mongoose docs](http://mongoosejs.com/docs/schematypes.html) and the possible
[options](http://mongoosejs.com/docs/api.html#schema_Schema).

The service itself may look like `src/services/users/index.ts`:

```ts
const service = new Users({ paginate }, app);
app.use('/users', service);
```

And the corresponding class loading the model `src/services/users/class.ts`:

```ts
export class Users extends Service {
    public docs: any; // swagger definition

    constructor(options: Partial<MongoDBServiceOptions>, app: Application) {
        super(options);

        const client: Promise<Db> = app.get('core-mongo');

        client.then(db => {
            this.Model = db.collection('users');
        });
    }
}
```

## References

In case of references to other documents, only the `_id` will be stored. But through the hooks it may be populated into the real object. If the object will be stored, it will be compressed again to only store the `_id`.

Within the services you often want to depopulate/join the subdocuments which are stored in different collections together as if they are stored as one big data structure. This reduces the number of calls to the server and database and helps improve the overall performance.

You can do so within the base service hooks as displayed here from the `users` service by loading the following hooks:

    const { populate, discard } = require('feathers-hooks-common')
    const depopulate = require('../../hooks/depopulate')

To specify how to populate define a population Schema:

    const populateRoles = {
      schema: {
        include: {
          parentField: 'roles_id',  // field in this service
          service: 'roles',         // sub service
          childField: '_id',        // key field in sub service
          nameAs: 'roles_id',       // where to store sub data (can overwrite original field)
          asArray: true             // specify if this is an array or single value
        }
      }
    }

Now you can use this hooks as follows:

    module.exports = {
      before: {
        all: [
          depopulate(populateRoles)
        ],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
      },

      after: {
        all: [
          populate(populateRoles),
          discard('_include')
        ],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
      },

      error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
      }
    }
