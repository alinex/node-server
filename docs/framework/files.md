# File Structure

The following files are on a production server:

    bin             # binaries to start the server
    config          # configuration
    lib             # program code
    log             # log files
    node_modules    # third party node modules
    public          # static files
    views           # templates (EJS)
    run             # collection of all neccessary files

The development system will also contain:

    .vscode         # editor config
    docs            # documentation sources
    src             # code sources
    test            # unit tests
    dist            # distribution for production server

## bin

This directory contains the binary to start the server, a systemd service script, mongo setup and a shell script to easily run the server in production mode.

## config

Contains the configuration files for the app

-   `default.yml` contains the basic application configuration
-   `production.yml` file override `default.yml` (when in production mode by setting `NODE_ENV=production`)

In this files you may change something like the `hostname`, `port` or the authentication.
More complex things should be changed in the code.

-   `disallowed-passwords.txt` a list of passwords which are always disallowed

## public

Contains static files to be served like `favicon`, images and stylesheets.
HTML content mostly comes through the templates.

## views

HTML Templates using [EJS](https://ejs.co/) which are simple HTML files with embedded variables and JavaScript code.

## node_modules

Installed dependencies which are added in the `package.json`

## log

Log files are stored as:

-   daily rotating file
-   automatic compression
-   separate error log
-   cleaned up after some time

The `combined.log` will contain everything from the last 5 days while the `error.log` will only contain error level messages but up to 90 days long.

## src

The base files are:

    cli.ts              # Start script to be called from command line
    index.ts            # Server include as control center
    config.ts           # Configuration setup
    configSchema.ts     # Definition of configuration
    server.ts           # Loading and controlling server
    app.ts              # Setup of the express server and the middleware
    app.hooks.ts        # General hooks
    logger.ts           # Setup for winston logger
    authentication.ts   # Setup of authentication
    channel.ts          # setup of communication chanels to the client
    formatter.ts        # REST formatter by accept header

    mongodb.ts          # Setup of mongo database connection
    mongoose.ts         # Setup of mongoose database connection
    pg.ts               # Setup of pg database connection
    mysql.ts            # Setup of mysql database connection
    mysql2.ts           # Setup of mysql2 database connection
    mssql.ts            # Setup of mssql database connection
    oracledb.ts         # Setup of oracledb database connection
    sqlite3.ts          # Setup of sqlite3 database connection

And then the concrete logic is split in the following directories:

    middleware          # for additional middleware
    services            # all the REST services
    models              # database models (mongoose)
    hooks               # scripts to be used as hook setups

### middleware

This are additional components which are loaded globally. They add general plugins into the application or also integrate completely selfdefined routes.

Add specific addon applications:

-   swagger

### services

Each service is organized as a directory and contains at least three files:

    index.ts            # the service setup bringing all together
    class.ts            # defining the model and methods
    hooks.ts            # hook scripts to be added

Find more under [services](services.md).

### hooks

Hooks are handler which are globally stored to be used in multiple services.
They are loaded into the service and referenced in it's hooks.

Common hooks are:

-   logger - used for debugging and logs the service call before or after
-   populate - integrate referenced documents
-   depopulate - remove integrated documents

Access control:

-   authenticate - login
-   authorize - check access rights

## test

Unit Tests are partly available under `/test` but you may also consider using the integrated tests of the server itself under the `check` service.

-   `services/` has our service tests
    -   `users.test.ts` contains some basic tests for the users service
-   `app.test.ts` tests that the index page appears, as well as 404 errors for HTML pages and JSON
-   `authentication.test.ts` includes some tests that basic authentication works

## docs

This folder contains the whole documentation as shown on this site as markdown. Use `mkdocs` to create an HTML representation of it.

## dist

The distribution folder will contain all the files which are needed for the production server. The structure within is the same as the folders described above. But in only with the modules needed for production and in case of a special project mixed with the addons and changes from core and the module.

{!docs/assets/abbreviations.txt!}
