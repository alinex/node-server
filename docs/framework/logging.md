# Logging

Logging is done in multiple ways:

-   Console Logging

    Is used to welcome in console and output fatal errors which break the server on STDERR.

-   Debug Module

    This allows to set different named tags which can be enabled on server start. It is used mainly to debug specific parts. A lot of sub modules used are also supporting it.

-   Logger

    Allows to log to file in specific formats also containing the time. This is used to log service
    calls and requests.
    It has predefined severity levels and often an identifier is added to corelate different entries together.

What never should be logged are passwords and sensitive data. Personal information should also be avoided in log.

## Logger

The logger is defined to log timestamps, level, processing unit, errors. By default this is done human readable.

The following log levels are possible:

-   error: errors
-   warn: service errors
-   info: step information
-   http: request and response
-   verbose: service calls
-   debug: hooks and error stack
-   silly: call data

The `silly` level will also log passwords and sensitive data. Therefore this should only be used in development and debugging.

The log level will be defined in the [configuration](../usage/running.md#configuration) or as environment setting `LOG_LEVEL`.

By default logging will be made:

-   on console with configured log level
-   into `log/combined.log` with defined log level
-   into `log/error.log` only for error level
-   actions will go into `log/action.log` for verifiability

The files will be rotated daily and removed after 30 days for combined-log and 90 days for errors.
The action log will rotate monthly with 5 years to keep them.

On the file system you will find the log files with a date pattern appended and a softlink pointing to the current and active log for easy access. Additionally some dot files in the form `.<md5>-audit.json` are used internally to purge and therefore is considered a necessary piece.

    -rw-rw-r--  1 alex alex  321 Okt 23 09:34 .21f339fc71826f96a363cb9e194a22aaa12c1531-audit.json
    -rw-rw-r--  1 alex alex    0 Okt 23 09:20 action.log
    -rw-rw-r--  1 alex alex    0 Okt 23 09:34 action.log.2020-10
    -rw-rw-r--  1 alex alex  323 Okt 23 09:20 .c9e29f01937abb1677b5bdd4e2db78af001f3f62-audit.json
    lrwxrwxrwx  1 alex alex   23 Okt 23 09:34 combined.log -> combined.log.2020-10-23
    -rw-rw-r--  1 alex alex 1534 Okt 23 09:34 combined.log.2020-10-23
    -rw-rw-r--  1 alex alex  326 Okt 23 09:20 .df94afc317bbb875160682c9c0fd99f524f320b7-audit.json
    lrwxrwxrwx  1 alex alex   20 Okt 23 09:34 error.log -> error.log.2020-10-23
    -rw-rw-r--  1 alex alex    0 Okt 23 09:20 error.log.2020-10-23


### Console output

```text
                              __   ____     __
              ######  #####  |  | |    \   |  |   ########### #####       #####
             ######## #####  |  | |     \  |  |  ############  #####     #####
            ######### #####  |  | |  |\  \ |  |  #####          #####   #####
           ########## #####  |  | |  | \  \|  |  #####           ##### #####
          ##### ##### #####  |  | |  |_ \     |  ############     #########
         #####  ##### #####  |  | |    \ \    |  ############     #########
        #####   ##### #####  |__| |_____\ \___|  #####           ##### #####
       #####    ##### #####                      #####          #####   #####
      ##### ######### ########################## ############  #####     #####
     ##### ##########  ########################   ########### #####       #####
    ____________________________________________________________________________

                       W E B   A N D   R E S T   S E R V E R
    ____________________________________________________________________________

Loading server modules...
Using test configuration.
2020-03-06 22:24:53 VERBOSE  Selfcheck...
2020-03-06 22:24:53 VERBOSE  Current load: 0.231201171875, 0.2581787109375, 0.246337890625 (limit 2); lag 1.824ms
2020-03-06 22:24:53 VERBOSE  At least 100MB free disk space for log folder needed, currently 367541MB are free
2020-03-06 22:24:54 VERBOSE  Connected to mongoDB V3.6.3 running under PID 1373 on alex-Aspire-E5-574
2020-03-06 22:24:54 VERBOSE  Found 7 user(s)
2020-03-06 22:24:54 VERBOSE  Binding server...
Server successfully started!
2020-03-06 22:24:54 INFO     Server listening on http://localhost:3030
2020-03-06 22:24:55 HTTP     ::ffff:127.0.0.1 HTTP    > GET /users - "Alinex CLI helper"
2020-03-06 22:24:55 DEBUG    ::ffff:127.0.0.1 HOOK    > authenticate
2020-03-06 22:24:55 DEBUG    ::ffff:127.0.0.1 HOOK    < authenticate 429ee71276122f55a3a94796
2020-03-06 22:24:55 VERBOSE  ::ffff:127.0.0.1 SERVICE > users::find
2020-03-06 22:24:55 VERBOSE  ::ffff:127.0.0.1 SERVICE < users::find
2020-03-06 22:24:55 HTTP     ::ffff:127.0.0.1 HTTP    < GET /users 200 OK 1936 B 42 ms
```

In the example above you see the server startup and the call to the users list.

Logging lines contains the following information in columns:

-   date
-   logging level in square brackets
    -   `error` - hard error there the system won't work correctly
    -   `warn` - problems which are not critical and you may work on
    -   `http` - HTTP request or response
    -   `info` - mandatory information like server start and external requests
    -   `verbose` - internal rewuests and processing information
    -   `debug` - show request headers, parameters and response values
    -   `silly` - seldom used for specific deep debugging of specific parts
-   message

The message itself can be of different types:

-   requests
    -   client IP or `-`
    -   used protocol (HTTP, HTTPS, SOCKETIO, REST, INTERNAL)
    -   direction `<` or `>`
    -   method (GET, FIND, CREATE, UPDATE, PATCH, REMOVE)
    -   path
    -   username or `-`
    -   user agent
-   response
    -   client IP or `-`
    -   used protocol (HTTP, HTTPS, SOCKETIO, REST, INTERNAL)
    -   direction `<` or `>`
    -   method (GET, FIND, CREATE, UPDATE, PATCH, REMOVE)
    -   path
    -   status code and message
    -   content length
    -   request time for HTTP
-   hook
    -   client IP or `-`
    -   type: HOOK
    -   direction `<` or `>`
    -   hook name
    -   special data (part from the parameter or result)
-   service
    -   client IP or `-`
    -   type: SERVICE
    -   direction `<` or `>`
    -   service name
    -   special data (part from the parameter or result)
-   processing information

### Extended Logging

```text
                              __   ____     __
              ######  #####  |  | |    \   |  |   ########### #####       #####
             ######## #####  |  | |     \  |  |  ############  #####     #####
            ######### #####  |  | |  |\  \ |  |  #####          #####   #####
           ########## #####  |  | |  | \  \|  |  #####           ##### #####
          ##### ##### #####  |  | |  |_ \     |  ############     #########
         #####  ##### #####  |  | |    \ \    |  ############     #########
        #####   ##### #####  |__| |_____\ \___|  #####           ##### #####
       #####    ##### #####                      #####          #####   #####
      ##### ######### ########################## ############  #####     #####
     ##### ##########  ########################   ########### #####       #####
    ____________________________________________________________________________

                       W E B   A N D   R E S T   S E R V E R
    ____________________________________________________________________________

Loading server modules...
Using test configuration.
2020-03-06 22:26:24 SILLY    Loading authentication middleware...
2020-03-06 22:26:24 SILLY    Loading swagger middleware...
2020-03-06 22:26:24 SILLY    Loading checkup service...
2020-03-06 22:26:24 SILLY    Loading info service...
2020-03-06 22:26:24 SILLY    Loading mailer service...
2020-03-06 22:26:24 SILLY    Loading roles service...
2020-03-06 22:26:24 SILLY    Loading users service...
2020-03-06 22:26:25 VERBOSE  Selfcheck...
2020-03-06 22:26:25 VERBOSE  Current load: 0.500244140625, 0.351806640625, 0.2808837890625 (limit 2); lag 1.591ms
2020-03-06 22:26:25 VERBOSE  At least 100MB free disk space for log folder needed, currently 367540MB are free
2020-03-06 22:26:25 VERBOSE  Connected to mongoDB V3.6.3 running under PID 1373 on alex-Aspire-E5-574
2020-03-06 22:26:25 VERBOSE  Found 7 user(s)
2020-03-06 22:26:25 VERBOSE  Binding server...
Server successfully started!
2020-03-06 22:26:25 INFO     Server listening on http://localhost:3030
2020-03-06 22:26:29 HTTP     ::ffff:127.0.0.1 HTTP    > GET /users - "Alinex CLI helper"
2020-03-06 22:26:29 SILLY    ::ffff:127.0.0.1 HTTP    > Headers: {
  accept: 'application/json;q=0.9, */*;q=0.8',
  authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1ODM1Mjk3MjAsImV4cCI6MTU4MzYxNjEyMCwiYXVkIjoiaHR0cHM6Ly9kZW1vLmFsaW5leC5kZSIsImlzcyI6ImFsaW5leC5kZSIsInN1YiI6IjQyOWVlNzEyNzYxMjJmNTVhM2E5NDc5NiIsImp0aSI6IjBiYmNhN2QyLWRiNmEtNDBmNy1iZTBmLWM3NTgyZTVkYTIzZSJ9.8l_RQ47ZQoYdBSIfJv7MVGI6QN798j7uxyWKkzyNrCw',
  'user-agent': 'Alinex CLI helper',
  host: 'localhost:3030',
  connection: 'close'
}
2020-03-06 22:26:29 DEBUG    ::ffff:127.0.0.1 HOOK    > authenticate
2020-03-06 22:26:29 SILLY    ::ffff:127.0.0.1 HOOK    > Params: {
  query: {},
  route: {},
  provider: 'rest',
  authentication: {
    strategy: 'jwt',
    accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1ODM1Mjk3MjAsImV4cCI6MTU4MzYxNjEyMCwiYXVkIjoiaHR0cHM6Ly9kZW1vLmFsaW5leC5kZSIsImlzcyI6ImFsaW5leC5kZSIsInN1YiI6IjQyOWVlNzEyNzYxMjJmNTVhM2E5NDc5NiIsImp0aSI6IjBiYmNhN2QyLWRiNmEtNDBmNy1iZTBmLWM3NTgyZTVkYTIzZSJ9.8l_RQ47ZQoYdBSIfJv7MVGI6QN798j7uxyWKkzyNrCw'
  }
}
2020-03-06 22:26:29 DEBUG    ::ffff:127.0.0.1 HOOK    < authenticate 429ee71276122f55a3a94796
2020-03-06 22:26:29 SILLY    ::ffff:127.0.0.1 HOOK    < Result: {
  query: {},
  route: {},
  provider: 'rest',
  authentication: {
    strategy: 'jwt',
    accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1ODM1Mjk3MjAsImV4cCI6MTU4MzYxNjEyMCwiYXVkIjoiaHR0cHM6Ly9kZW1vLmFsaW5leC5kZSIsImlzcyI6ImFsaW5leC5kZSIsInN1YiI6IjQyOWVlNzEyNzYxMjJmNTVhM2E5NDc5NiIsImp0aSI6IjBiYmNhN2QyLWRiNmEtNDBmNy1iZTBmLWM3NTgyZTVkYTIzZSJ9.8l_RQ47ZQoYdBSIfJv7MVGI6QN798j7uxyWKkzyNrCw',
    payload: {
      iat: 1583529720,
      exp: 1583616120,
      aud: 'https://demo.alinex.de',
      iss: 'alinex.de',
      sub: '429ee71276122f55a3a94796',
      jti: '0bbca7d2-db6a-40f7-be0f-c7582e5da23e'
    }
  },
  user: {
    _id: 429ee71276122f55a3a94796,
    email: 'demo@alinex.de',
    password: '$2a$12$UDsnL7PeLa7qMBIN9ufi1e/3q.GiYJDct1SHw2lDo2NEwxjR8S2Iq',
    nickname: 'demo',
    disabled: false,
    createdAt: 'Tue Feb 18 2020 23:24:06 GMT+0100 (CET)',
    updatedAt: 'Tue Feb 18 2020 23:24:06 GMT+0100 (CET)',
    __v: 0,
    name: 'Demo User',
    position: 'Test',
    avatar: 'https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm',
    roles_id: [ 5b2025aedcb10b38733b9825, [length]: 1 ],
    roles: {
      _id: 5b2025aedcb10b38733b9825,
      abilities: [
        {
          action: [Array],
          subject: [Array],
          fields: [Array],
          _id: 5b2025aedcb10b38733b9826
        },
        [length]: 1
      ],
      name: 'Admin',
      description: 'Super Administrator with maximum rights',
      createdAt: 2018-06-12T19:57:34.027Z,
      updatedAt: 2018-06-12T19:57:34.027Z,
      __v: 0
    }
  },
  authenticated: true
}
2020-03-06 22:26:29 VERBOSE  ::ffff:127.0.0.1 SERVICE > users::find
2020-03-06 22:26:29 SILLY    ::ffff:127.0.0.1 SERVICE > Params: {
  query: {},
  route: {},
  provider: 'rest',
  authentication: {
    strategy: 'jwt',
    accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1ODM1Mjk3MjAsImV4cCI6MTU4MzYxNjEyMCwiYXVkIjoiaHR0cHM6Ly9kZW1vLmFsaW5leC5kZSIsImlzcyI6ImFsaW5leC5kZSIsInN1YiI6IjQyOWVlNzEyNzYxMjJmNTVhM2E5NDc5NiIsImp0aSI6IjBiYmNhN2QyLWRiNmEtNDBmNy1iZTBmLWM3NTgyZTVkYTIzZSJ9.8l_RQ47ZQoYdBSIfJv7MVGI6QN798j7uxyWKkzyNrCw',
    payload: {
      iat: 1583529720,
      exp: 1583616120,
      aud: 'https://demo.alinex.de',
      iss: 'alinex.de',
      sub: '429ee71276122f55a3a94796',
      jti: '0bbca7d2-db6a-40f7-be0f-c7582e5da23e'
    }
  },
  user: {
    _id: 429ee71276122f55a3a94796,
    email: 'demo@alinex.de',
    password: '$2a$12$UDsnL7PeLa7qMBIN9ufi1e/3q.GiYJDct1SHw2lDo2NEwxjR8S2Iq',
    nickname: 'demo',
    disabled: false,
    createdAt: 'Tue Feb 18 2020 23:24:06 GMT+0100 (CET)',
    updatedAt: 'Tue Feb 18 2020 23:24:06 GMT+0100 (CET)',
    __v: 0,
    name: 'Demo User',
    position: 'Test',
    avatar: 'https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm',
    roles_id: [ 5b2025aedcb10b38733b9825, [length]: 1 ],
    roles: {
      _id: 5b2025aedcb10b38733b9825,
      abilities: [
        {
          action: [Array],
          subject: [Array],
          fields: [Array],
          _id: 5b2025aedcb10b38733b9826
        },
        [length]: 1
      ],
      name: 'Admin',
      description: 'Super Administrator with maximum rights',
      createdAt: 2018-06-12T19:57:34.027Z,
      updatedAt: 2018-06-12T19:57:34.027Z,
      __v: 0
    }
  },
  authenticated: true
}
2020-03-06 22:26:29 VERBOSE  ::ffff:127.0.0.1 SERVICE < users::find
2020-03-06 22:26:29 SILLY    ::ffff:127.0.0.1 SERVICE < Result: [
  {
    _id: 429ee71276122f55a3a94796,
    email: 'demo@alinex.de',
    password: '$2a$12$UDsnL7PeLa7qMBIN9ufi1e/3q.GiYJDct1SHw2lDo2NEwxjR8S2Iq',
    nickname: 'demo',
    disabled: false,
    createdAt: 'Tue Feb 18 2020 23:24:06 GMT+0100 (CET)',
    updatedAt: 'Tue Feb 18 2020 23:24:06 GMT+0100 (CET)',
    __v: 0,
    name: 'Demo User',
    position: 'Test',
    avatar: 'https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm',
    roles_id: [ 5b2025aedcb10b38733b9825, [length]: 1 ]
  },
  { _id: 5e4d9b5e6dd2f140a8dd5f6a },
  { _id: 5e4ea17a5ea908574c0985a5 },
  {
    _id: 5e4ec010b3fbcb6b1024e341,
    roles_id: [ [length]: 0 ],
    email: 'demo@alinex.de',
    password: '$2a$10$C5d8mQwfV0X9Uj28h9y2/OQX6IxgzuacbAIPYxkBC6GhZPRDo8Jxm',
    nickname: 'demo',
    disabled: false,
    name: 'Demo User',
    position: 'Test',
    avatar: 'https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm',
    createdAt: 2020-02-20T17:21:20.133Z,
    updatedAt: 2020-02-20T17:21:20.133Z,
    __v: 0
  },
  {
    _id: 5e4ec281b3fbcb6b1024e345,
    roles_id: [ [length]: 0 ],
    email: 'demo1@alinex.de',
    password: '$2a$10$oK1Zgktnzm9Elw9gkL5AeelMc62IDlgvMENwYWk49bhXMYRfc31l6',
    nickname: 'demo1',
    disabled: false,
    name: 'Demo User 1',
    position: 'Test',
    avatar: 'https://www.gravatar.com/avatar/920b31bfc299ab42211370364e2fe603?s=60&d=mm',
    createdAt: 2020-02-20T17:31:45.775Z,
    updatedAt: 2020-02-20T17:31:45.775Z,
    __v: 0
  },
  {
    _id: 5e4ec38965bd407065855e0d,
    email: 'demo1@alinex.de',
    password: '$2a$10$DmGYCCaCw7DOlohNIUyIjeGzg5pOIzJVz450JnLUSogTF0eD4kUJG',
    nickname: 'demo1',
    disabled: false,
    name: 'Demo User 1',
    position: 'Test',
    avatar: 'https://www.gravatar.com/avatar/920b31bfc299ab42211370364e2fe603?s=60&d=mm'
  },
  {
    _id: 5e4ee39b65bd407065855e0e,
    email: 'demo1@alinex.de',
    password: '$2a$10$YCIq71r3QAKYQZ07uhuWVuMqAc3AjRzzely8Br6tkbIC8yM1a/Lni',
    nickname: 'demo1',
    disabled: false,
    name: 'Demo User 1',
    position: 'Test',
    avatar: 'https://www.gravatar.com/avatar/920b31bfc299ab42211370364e2fe603?s=60&d=mm'
  },
  [length]: 7
]
2020-03-06 22:26:29 HTTP     ::ffff:127.0.0.1 HTTP    < GET /users 200 OK 1936 B 42 ms
2020-03-06 22:26:29 SILLY    ::ffff:127.0.0.1 HTTP    < Response: {
  total: 7,
  limit: 10,
  skip: 0,
  data: [
    {
      _id: 429ee71276122f55a3a94796,
      email: 'demo@alinex.de',
      nickname: 'demo',
      disabled: false,
      createdAt: 'Tue Feb 18 2020 23:24:06 GMT+0100 (CET)',
      updatedAt: 'Tue Feb 18 2020 23:24:06 GMT+0100 (CET)',
      __v: 0,
      name: 'Demo User',
      position: 'Test',
      avatar: 'https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm',
      roles_id: [ 5b2025aedcb10b38733b9825, [length]: 1 ],
      roles: {
        _id: 5b2025aedcb10b38733b9825,
        abilities: [ [Object], [length]: 1 ],
        name: 'Admin',
        description: 'Super Administrator with maximum rights',
        createdAt: 2018-06-12T19:57:34.027Z,
        updatedAt: 2018-06-12T19:57:34.027Z,
        __v: 0
      }
    },
    { _id: 5e4d9b5e6dd2f140a8dd5f6a },
    { _id: 5e4ea17a5ea908574c0985a5 },
    {
      _id: 5e4ec010b3fbcb6b1024e341,
      roles_id: [ [length]: 0 ],
      email: 'demo@alinex.de',
      nickname: 'demo',
      disabled: false,
      name: 'Demo User',
      position: 'Test',
      avatar: 'https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm',
      createdAt: 2020-02-20T17:21:20.133Z,
      updatedAt: 2020-02-20T17:21:20.133Z,
      __v: 0,
      roles: null
    },
    {
      _id: 5e4ec281b3fbcb6b1024e345,
      roles_id: [ [length]: 0 ],
      email: 'demo1@alinex.de',
      nickname: 'demo1',
      disabled: false,
      name: 'Demo User 1',
      position: 'Test',
      avatar: 'https://www.gravatar.com/avatar/920b31bfc299ab42211370364e2fe603?s=60&d=mm',
      createdAt: 2020-02-20T17:31:45.775Z,
      updatedAt: 2020-02-20T17:31:45.775Z,
      __v: 0,
      roles: null
    },
    {
      _id: 5e4ec38965bd407065855e0d,
      email: 'demo1@alinex.de',
      nickname: 'demo1',
      disabled: false,
      name: 'Demo User 1',
      position: 'Test',
      avatar: 'https://www.gravatar.com/avatar/920b31bfc299ab42211370364e2fe603?s=60&d=mm'
    },
    {
      _id: 5e4ee39b65bd407065855e0e,
      email: 'demo1@alinex.de',
      nickname: 'demo1',
      disabled: false,
      name: 'Demo User 1',
      position: 'Test',
      avatar: 'https://www.gravatar.com/avatar/920b31bfc299ab42211370364e2fe603?s=60&d=mm'
    },
    [length]: 7
  ]
}
```

## Debugging

Like in all my modules the debug module is included. The [debug](https://github.com/visionmedia/debug#readme) documentation shows how to use and control it.

The server module uses the following names:

-   `server:*` - to get all
-   `server:config` - configuration use
-   `socket.io:*` - websocket transfer

{!docs/assets/abbreviations.txt!}
