# Configuration

## Loader

Within the `config.ts` file the source of the configuration can be changed:

```ts
// define configuration source
const val = new Validator(schema, { source: `config/default.yml` }, { source: `config/${TYPE}.yml` });
```

See [DataStore](https://alinex.gitlab.io/node-datastore/api/loader) for more details.

The ability is to use

-   multiple files
-   other formats
-   including database settings

## Adding values

If more settings should be added, the `configSchema.ts` has to be extended. This can also be done in for service specific configurations within `service/<name>/configSchema.ts`.

See [Schema Specification](https://alinex.gitlab.io/node-validator/schema) for more details.

## Using Settings

The loading is already done in the base framework and you may import and use the values directly.

```ts
import config from './config';

const host = config('server.host');
```

{!docs/assets/abbreviations.txt!}
