# Hooks

A hook function can be a normal or async function that takes the hook context as the parameter and returns the context object, `undefined` or throws an error.

-   `context.app` - (read only) Feathers application object. This can be used to retrieve other services (via context.app.service('name')) or configuration values.
-   `context.service` - (read only) service this hook currently runs on.
-   `context.path` - (read only) service name (or path) without leading or trailing slashes.
-   `context.method` - (read only) service method (`find`, `get`, `create`, `update`, `patch`, `remove`).
-   `context.type` - (read only) hook type (one of `before`, `after` or `error`).
-   `context.params` - (writable) service method parameters (including `params.query`).
-   `context.id` - (writable) id for a get, remove, update and patch service method call.
-   `context.data` - (writable) data of a create, update and patch service method call.
-   `context.error` - (writable) error object that was thrown in a failed method call. It is only available in error hooks.
-   `context.result` - (writable) result of the successful service method call. It is only available in after hooks but can also be set in a before hook to skip the actual service method (database) call or an error hook to swallow the error and return a result instead.
-   `context.dispatch` - (writable) a "safe" version of the data that should be sent to any client (not internal calls). If `context.dispatch` has not been set `context.result` will be sent to the client instead.
-   `context.statusCode` - (writable) override the standard HTTP status code that should be returned.

## General

This are hooks which are used as general hooks adding the same functionality to all services.

### rateLimit

The system has a rate limiter defined on the connection level which could be configured. But some endpoints could be much more resource-intensive than others. Therefore this hook will help.

```ts
// max 5 calls per minute (60000ms) }
before: {
    create: [rateLimit({ max: 5, interval: 60000 })];
}
```

An error will be returned if limit reached for an IP address.

### cache

The cache will store results of the service to be used in another call:

```ts
import { useCache, createCache } from '../../hooks/cache';

const cache = createCache(10); // number of elements to cache

before: {
    all: [useCache(cache)],
},
after: {
    all: [useCache(cache)]
}
```

This will store the element in the store after the first call and load it again on a second call. This will also be shown in the logging:

```text
2020-03-13 22:23:34 DEBUG    ::ffff:127.0.0.1 HOOK    > cache get users 429ee71276122f55a3a94796
2020-03-13 22:23:43 DEBUG    ::ffff:127.0.0.1 HOOK    < cache get users 429ee71276122f55a3a94796
```

If the cache should work over additional hooks, which may change the service result afterwards the `_isCached` flag can be used to define which `after` hooks to run:

```ts
import populate from '../../hooks/populate';
import { useCache, createCache } from '../../hooks/cache';
import { iff } from 'feathers-hooks-common';

const cache = createCache(10);
const schema = {...};

before: {
    get: [useCache(cache)],
},
after: {
    all: [],
    find: [],
    get: [iff(context => !context.result._isCached, populate({ schema }), useCache(cache))],
    create: [useCache(cache)],
    update: [useCache(cache)],
    patch: [useCache(cache)],
    remove: [useCache(cache)]
}
```

In the above example populate is only called if the result comes not from the cache (which already includes this). Here the `useCache` have to be the last hook.

## Basics

### validate

This hook is used to validate and sanitize incoming data using a schema definition. It is based on the [Alinex Validator](https://alinex.gitlab.io/node-validator) package.

To use this hook you need to define the schema first:

=== "schema.ts"

    ```ts
    import * as Builder from '@alinex/validator/lib/schema';

    export const patch = new Builder.ObjectSchema({
      item: {
        email: new Builder.EmailSchema({ normalize: true, registered: true })
      }
    })
    ```

=== "hooks.ts"

    ```ts
    import validate from '../../hooks/validate';
    import * as schema from './schema';

    export default {
        before: {
            all: [],
            find: [],
            get: [],
            create: [],
            update: [],
            patch: [validate(schema.patch)],
            remove: []
        },

        after: {
            all: [],
            find: [],
            get: [],
            create: [],
            update: [],
            patch: [],
            remove: []
        },

        error: {
            all: [],
            find: [],
            get: [],
            create: [],
            update: [],
            patch: [],
            remove: []
        }
    };
    ```

If the validation fails, it will return an HTTP Error 422: Unprocessable Data. It will also be logged on the server.

### populate

Populating data with related records is almost unavoidable in an app, and its time consuming to code. With this hook it can easily be achieved by calling other services to gather related data.

This will join associated records. This makes the service return not only the object data but also all the information of sub objects. This reduces the needed number of calls to the REST server.

```ts
import populate from '../../hooks/populate';

export default {
    before: {
        update: [discard('roles')],
        patch: [discard('roles')]
    },

    after: {
        all: [
            populate({
                schema: {
                    include: {
                        service: 'roles',
                        nameAs: 'roles',
                        parentField: 'roles_id',
                        childField: '_id'
                    }
                }
            })
        ]
    }
};
```

As the above describes a 1:n relation it will add a `roles` list of object to the result based on the `roles_id` list. The `discard` hook will remove the added `_include` list which the original populate method will add. To remove the added information before updating the record, if it is send back an additional `discard` is used.

See [populate](https://hooks-common.feathersjs.com/hooks.html#populate) for more options and other relations.

## Access Control

To make short circuit calls best is to always put the access control hooks first.

### authenticate

This hook will set authentication for the service. The user has to login to use it. Disabled accounts are not allowed.

    before: { all: [ authenticate() ] }

By default the `jwt` authentication is used but others may be given.

If not authenticated two special users may be assigned:

- `local` for access from localhost address (mostly administrators)
- `guest` for all other not authenticated users

After this hook the flag `context.params.authenticated = true` is set. This is not set if the `local` or `guest` user is assigned.

### abilities

This will load the abilities of the user in result, params or for the local/guest access.

    after: { create: [ abilities() ] }

This will be called automatically before the  `authorize` hook because these are the rules to check within the authorization check. The abilities will be collected from the associated roles which are active.

### authorize

If not already done, this will call the authorization and abilities hooks first. Afterwards the abilities will be checked if the current active request is allowed.

    before: { all: [ authorize() ] }

This has to be called after the `authentication` hook because the guest abilities will be used if no user is logged in.

## User Management

### passwordCheck

This hook will check that the password reaches the strength configured in the authentication section.

    before: {
        create: [ checkPassword() ],
        update: [ checkPassword() ]
    }

### gravatar

This will add a avatar link (using gravatar) to the user record based on the email address.

    before: {
        create: [gravatar()],
        update: [gravatar()],
        patch: [gravatar()]
    }

{!docs/assets/abbreviations.txt!}
