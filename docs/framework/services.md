# Mongoose Services

## Basics

Each service is organized as a directory under `services` and contains at least three files:

    index.ts            # the service setup bringing all together
    hooks.ts            # hook scripts to be added
    schema.ts           # service specific configuration definition
    api.ts              # swagger definition

All services are loaded with their `index.ts` file automatically.

### index

The index has to load all parts:

```ts
import createModel from '../../models/users';
import hooks from './hooks';
import api from './api';
```

The body will look something like:

```ts
import { ServiceAddons, Paginated, Params } from '@feathersjs/feathers';
import { Service, MongooseServiceOptions } from 'feathers-mongoose';
import { Application } from '../../declarations';
import config from '../../config';
import { serviceLogger } from '../../logger';

import createModel from '../../models/users';
import api from './api';
import hooks from './hooks';

class Users extends Service {
    public docs = api;
    private context: any;

    constructor(options: Partial<MongooseServiceOptions>, app: Application, path: string) {
        super({ ...options, Model: createModel(app) });
        this.context = { app, path };
        app.use(path, this);
        app.service('users').hooks(hooks);
    }

    public async find(params?: Params): Promise<any[] | Paginated<any[]>> {
        const context = { ...this.context, params, method: 'find' };
        serviceLogger(context);
        const result = await super.find(params);
        serviceLogger({ ...context, result }, true);
        return result;
    }
}

// Add this service to the service type index
declare module '../../declarations' {
    interface ServiceTypes {
        users: Users & ServiceAddons<any>;
    }
}

export default function(app: Application) {
    const paginate = config('paginate');
    // Initialize our service with any options it requires
    new Users({ paginate }, app, '/users');
}
```

The class will contain the methods of this service. All methods are optional and if a method is not implemented Feathers will automatically emit a `NotImplemented` error.

Service methods must use async/await or return a Promise and have the following parameters:

-   `id` - The identifier for the resource. A resource is the data identified by a unique id.
-   `data` - The resource data.
-   `params` - Additional parameters for the method call (see params)
    -   `query` - the query parameters from the client, either passed as URL query parameters (see the REST chapter) or through websockets (see Socket.io or Primus).
    -   `provider` - The transport (rest, socketio or primus) used for this service call. Will be undefined for internal calls from the server (unless passed explicitly).
    -   `authentication` - The authentication information to use for the authentication service
    -   `user` - The authenticated user, either set by Feathers authentication or passed explicitly.
    -   `connection` - If the service call has been made by a real-time transport (e.g. through websockets), params.connection is the connection object that can be used with channels.

### models

Mongoose services needs a data model definition which is stored in the common `src/models` folder.

### hooks

the hooks are build with the following structure:

```ts
export default {
    before: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },

    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },

    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
```

Each element may get one or multiple hook scripts added.

### API docs

This is mainly used for the swagger documentation. This Swagger config is defined as [OpenApi 3](https://swagger.io/docs/specification/basic-structure/).

The general setup for swagger is done directly in the middleware while service specific definitions are added as API which is available as `service.docs`:

```ts
export default {
    description: 'Basic service to get the users to be used for accessing this application',
    externalDocs: {
        description: 'find more info here',
        url: 'https://swagger.io/about'
    },
    definitions: {
        users: {
            type: 'object',
            required: ['email', 'password'],
            properties: {
                email: {
                    type: 'string',
                    description: 'Email address as unique identifier',
                    example: 'info@alinex.de'
                },
                password: {
                    type: 'string',
                    description: 'Secret password (make it unguessable)'
                },
                _id: {
                    type: 'string',
                    description: 'The id of the user'
                }
            }
        },
        users_list: {
            type: 'array',
            items: { $ref: '#/components/schemas/users' }
        }
    },
    securities: 'all',
    // securities: ['create', 'update', 'patch', 'remove'],
    // multi: 'all',
    operations: {
        find: {
            summary: 'run some checks',
            description: 'Run some checks and get all results back together with the time used.'
            'parameters[]': {
                description: 'Property to query results',
                in: 'query',
                name: '$search',
                type: 'string'
            }
        },
        get: {
            summary: 'run one specific check',
            description: 'Run a single check and get the result back together with the time used.'
        },
        create: false,
        update: false,
        patch: false,
        remove: false
    }
};
```

The most common settings are shown above, but find all at [Swagger Options](https://github.com/feathersjs-ecosystem/feathers-swagger#swaggeroptions) or the [OAS Specification](https://github.com/OAI/OpenAPI-Specification).

### Config Schema

This file should export an [Alinex Validator](https://alinex.gitlab.io/node-validator) schema as default to check the configuration entries for this service. The configuration itself has to be put under an entry with the service name.

See [Schema Specification](https://alinex.gitlab.io/node-validator/schema) for more details.

## RDBMS Service

{!docs/assets/abbreviations.txt!}
