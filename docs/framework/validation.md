# Validation

Server side validation is a needed step for security. Each incoming data should be checked before the processing takes place.

This goal will be reached by adding a validate hook before processing the information.
See [validate hook](http://127.0.0.1:8000/framework/hooks/#validate) for more details how this will be established.

## Schema

Therefore a schema definition is needed. See the [Validator](https://alinex.gitlab.io/node-validator/schema) for all the options possible.

Basically you generate a `schema.ts` file within the service directory containing the definition:

=== "schema.ts"

    ```ts
    import * as Builder from '@alinex/validator/lib/schema';

    const fields = {
      _id: new Builder.StringSchema(),
      name: new Builder.StringSchema({ trim: true, min: 4 }),
      description: new Builder.StringSchema({ trim: true, min: 4 }),
      disabled: new Builder.BooleanSchema(),
      abilities: new Builder.ArraySchema({
        item: { '*': new Builder.ObjectSchema({
          item: {
            action: new Builder.ArraySchema({ item: { '*': new Builder.StringSchema({ min: 1 }) } }),
            subject: new Builder.ArraySchema({ item: { '*': new Builder.StringSchema({ min: 1 }) } }),
            conditions: new Builder.ObjectSchema(),
            fields: new Builder.ArraySchema({ item: { '*': new Builder.StringSchema({ min: 1 }) } })
          }
        }) }
      })
    }

    export const create = new Builder.ObjectSchema({
      item: fields,
      denyUndefined: true
    })

    export const update = new Builder.ObjectSchema({
      item: fields,
      denyUndefined: true,
      mandatory: true
    })

    export const patch = new Builder.ObjectSchema({
      item: fields,
      denyUndefined: true
    })
    ```

This example shows how to define multiple definitions, one per each method. They will work on the data send with the request.

## Integration

Within the service hooks this will be added:

=== "hooks.ts"

    ```ts
    // ...
    import { discard } from 'feathers-hooks-common';
    import authorize from '../../hooks/authorize';
    import validate from '../../hooks/validate';
    import * as schema from './schema';
    // ...

    export default {
      before: {
        // ...
        create: [
            validate(schema.create),
            hashPassword('password'), gravatar()],
        update: [
            authorize(), discard('roles', 'createdAt', 'updatedAt', 'avatar', '__caslSubjectType__'),
            validate(schema.update),
            hashPassword('password'), gravatar()],
        patch: [
            authorize(), discard('roles', 'createdAt', 'updatedAt', 'avatar', '__caslSubjectType__'),
            validate(schema.patch),
            hashPassword('password'), gravatar()],
      },
      // ...
    };
    ```

As shown above often some pre hooks like `authorize` or `discard` are used before and some preprocessing like `hashPassword` or `gravatar` will be needed.

{!docs/assets/abbreviations.txt!}
