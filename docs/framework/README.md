title: Overview

# Basic Concepts

The Alinex Server is a base framework containing functionality always needed, but to add your specific business logic additional services are needed.

It is based on [feathers-js](https://docs.feathersjs.com/) a service oriented framework and provides:

-   TypeScript Support

    Using the latest language features, Feathers is a small library that provides the structure to create complex applications but is flexible enough to not be in the way.

-   Flexible Plugins

    Feathers is a “batteries included but easily swappable framework” with a large ecosystem of plugins. Include exactly what you need. No more, no less.

-   Database Agnostic

    Feathers has adapters for 15+ databases out of the box. You can have multiple databases in a single app and swap them out painlessly due to a consistent query interface.

-   Universal

    Feathers can be used the same way on the server with Node.js and in the browser with frameworks like React, Angular or VueJS or on mobile with React Native.

-   Secure

    Using server side validation against schema for at least the core requests.

-   Service Oriented

    Feathers gives you the structure to build service oriented apps from day one. When you eventually need to split your app into microservices it’s an easy transition.

-   Instant Real-time REST APIs

    Feathers provides instant CRUD functionality via Services, exposing both a RESTful API and real-time backend through websockets automatically.

-   Easy extensible

    Make your own server with this base and some free or selfmade extension modules.

Based is this on a setup of the [ExpressJS](https://expressjs.com/de/) server with a full blown set of modules to make it robust and feature rich.

## Projects

To extend the system a **parent project** has to be build. Also additional **extension projects** may be used.
Using these technique it is possible to include the core server, add business logic and customize it as far as needed.

## Middleware

This are modules with it's own logic not working with the common REST calls. Often third party express.js modules which can be included seamlessly.

## Services

All the application’s business logic for REST is contained in services. A service is a JavaScript object that implements CRUD methods. A service should be a entity centric small element. Each service may call others making a service oriented architecture. It can also group organizational elements together.

The database adapter takes care of mapping CRUD requests to your database in a standard way.

## Hooks

Hooks are like little middleware functions that allow you to run logic before and after each service call. You would use hooks to:

-   validate request payload against a schema
-   check if the user is logged in
-   check if the user has sufficient privileges
-   scope the database queries to what the user is allowed to see
-   modify payloads before they get stored in the database (e.g. updating updatedAt field)
-   modify the response to the user removing any unwanted fields
-   trigger other actions such as sending emails

Hooks compose well and help reuse code between your services.

{!docs/assets/abbreviations.txt!}
