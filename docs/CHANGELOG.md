# Last Changes


## Version 1.3 (03.04.2021)

-   checkup: also use html reporter as default for get in browser
-   checkup: display direct test or group report on GET
-   update checkup module to v1.4.0 with postgres support

-   Fix Version 1.3.1 (07.04.2021) with checkup fixes
-   Fix Version 1.3.2 (11.04.2021) with checkup fixes
-   Fix Version 1.3.3 (13.04.2021) with checkup fixes
-   Fix Version 1.3.5 (22.04.2021) scheduler add stack trace + new shortcut calls and support of case file in GET
-   Fix Version 1.3.6 (23.04.2021) allow checkup path in GET mixed mode
-   Fix Version 1.3.7 (29.04.2021) calling checkup with config + limiter update
-   Fix Version 1.3.8 (30.04.2021) roll back limiter update because buggy

## Version 1.2 (04.03.2021)

-   report service schema errors and fix checkup schema
-   checkup add file/database store
-   checkup with integrated scheduler
-   package updates

-   Fix Version 1.2.1 (04.03.2021) with new checkup fixes

## Version 1.1 (31.01.2021)

-   implement checkup scheduler as background task
-   update checkup and more packages
-   update doc theme
-   update info module usage/databases

## Version 1.0 (08.11.2020)

-   completely replace checks with checkup
    -   FIND
        -   paths query parameter -> list of results
        -   tags query parameter
    -   GET
        -   path -> complete report
    -   FIND/GET
        -   verbose query parameter
        -   extend query parameter
        -   analyze always on
        -   specify reporter
    -   PATCH
        -   autofix
    -   different formatters and content types
-   startup check separate suite
-   select checkup suite on call
-   add `$select` operator for GET in info and checkup
-   support more HTTP error codes
-   only revoke token if not already done
-   user login based on IP
-   fix style of 500 error message
-   allow sending text elements with predefined mimetype

## Version 0.8.0 (15.07.2020)

-   restructure of database config section
-   support multiple database connectors
-   restructure to support faster dev mode in parent project
-   checkup integration replace checks
-   i18next integration for CLI and requests
-   validator update with trans
-   supporting action log
-   fixed dev mode for parent project
-   adding server side validation

-   Hotfix 0.8.1 (13.10.2020) - make server run able again
-   Hotfix 0.8.2 (13.10.2020) - extract checkup config and fix database schema
-   Hotfix 0.8.3 (13.10.2020) - fix build of subprojects with server package
-   Hotfix 0.8.4 (13.10.2020) - fix authentication problem
-   Hotfix 0.8.5 (19.10.2020) - smaller fixes to help extending it
-   Hotfix 0.8.6 (21.10.2020) - add bash-lib to dist
-   Hotfix 0.8.7 (22.10.2020) - fix swagger security settings
-   Fix 0.8.8 (23.10.2020) - add symlink for logs
-   Update 0.8.9 (23.10.2020) - update packages and doc structure

## Version 0.7.0 (06.07.2020)

-   multiple response formats
-   support services in projects
-   swagger + project swagger

## Version 0.6.0 (06.06.2020)

-   system.d logging to file
-   `guest` and `local` user entries now needed
-   checks now with additional WARN state
-   info service result data restructured
-   updated modules
-   development mode for projects

Version 0.6.1 (17.06.2020) - module updates adding datastore xlsx support

## Version 0.5.1 (15.05.2020)

-   revoking access tokens
-   logging socket io calls
-   allow server to be used as module
-   further customize start page
-   allow extension projects
-   gui client support
-   client included in server
-   client download

## Version 0.4.2 (15.03.2020)

Access control checking completed:

-   added rate limiting for http, websocket
-   added hook for service specific rate limiters
-   added hook for password strength test
-   add gravatar and createdAt, updatedAt to users, disabled
-   switch to OAS 3.0 for swagger
-   style server homepage
-   service restructure with better logging
-   authentication as service
-   check authorize + local authorize
-   caching user get
-   config definition in service

## Version 0.3.0 (09.10.2019)

Rework core using TypeScript and newest software versions:

-   update server to feathers 4
-   use mongodb instead of mongoose ORM wrapper
-   logging with IP for all events in all protocols
-   integrate [Validator](https://alinex.gitlab.io/node-validator) for configuration
-   re-added and fixed swagger
-   selfcheck and check services etablished

Not all functionality from 0.2.0 is available, but more will follow soon.

## Never Released

-   update server and client modules
-   add swagger authentication using APIkey

## Version 0.2.0 (10.08.2018)

-   user and role management
-   information service and client display
-   http and service logging
-   JWT authentication
-   selfcheck system
-   client based on Quasar Framework
-   REST server based on feathers.js
-   initial version

{!docs/assets/abbreviations.txt!}
