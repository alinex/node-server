title: Client

# Client Applications

So far the server didn't provide much user interface beside the REST API.

## Access using CLI

A lot of services needs login before they can be used. This is done with the authentication service.
To show the use and test it in a very easy and simple way we use the `curl` and the in the server included `datastore` tools here.

With these the access token can be created using:

=== "HTTP"

    ```bash
    POST {{SERVER}}/authentication
    Content-Type: application/json

    {
        "strategy": "local",
        "email": "demo@alinex.de",
        "password": "demo123"
    }
    ```

=== "DataStore"

    ```bash
    node_modules/.bin/datastore -q \
        --input http://localhost:3030/authentication \
        --http-method post \
        --http-header 'Content-Type: application/json' \
        --http-data '{ "strategy": "local", "email": "demo@alinex.de", "password": "demo123" }' \
        --input-format json
    ```

=== "cURL"

    ```bash
    curl --request POST $SERVER/authentication \
         --header  'Content-Type: application/json' \
         --data    '{ "strategy": "local", "email": "demo@alinex.de", "password": "demo123" }'
    ```

Will produce a result like:

```json
{
    "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NzA4Mjk1OTYsImV4cCI6MTU3MDkxNTk5NiwiYXVkIjoiaHR0cHM6Ly9kZW1vLmFsaW5leC5kZSIsImlzcyI6ImFsaW5leC5kZSIsInN1YiI6IjQyOWVlNzEyNzYxMjJmNTVhM2E5NDc5NiIsImp0aSI6IjJhMzliOTIwLWNiNzktNGE5Yy1iY2Q2LWVkNjcxOTNjMDUxNCJ9.7ipKqms9VdJrKUJc4_5WGLvJ_KgC1nSmW_SOdufR-wE",
    "authentication": {
        "strategy": "local"
    },
    "user": {
        "_id": "429ee71276122f55a3a94796",
        "email": "demo@alinex.de",
        "nickname": "demo",
        "disabled": false,
        "createdAt": "Fri Sep 20 2019 22:42:42 GMT+0200 (CEST)",
        "updatedAt": "Fri Sep 20 2019 22:42:42 GMT+0200 (CEST)",
        "__v": 0,
        "name": "Demo User",
        "position": "Test",
        "avatar": "https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm",
        "roles_id": ["5b2025aedcb10b38733b9825"]
    }
}
```

To only get the access token add the options `--filter accessToken --output-format text` to the above [DataStore](https://alinex.gitlab.io/node-datastore) call.

Further on the examples will use the `$JWT` variable with the above accessToken to access restricted methods. It will be given within the following requests as authorization header.

=== "HTTP"

    ```json
    GET http://localhost:3030/authentication
    Authorization: Bearer $JWT
    ```

=== "DataStore"

    ```bash
    node_modules/.bin/datastore -q \
        --input $server/users \
        --http-header "Authorization: Bearer $JWT"
    ```

=== "cURL"

    ```bash
    curl --request GET $SERVER/users \
         --header  "Authorization: Bearer $JWT"
    ```

## Authentication Helper

To work in an easy way a small helper is integrated. It may be called without parameters on demo server or by giving the following options on any server:

-   `-s <url>, --server <url>`
-   `-e <address>, --email <address>`
-   `-p <passphrase>, --password <passphrase>`

!!! example

    ```bash
    bin/server-auth # for demo server
    bin/server-auth -s http://my-server.de -e my-email.gmail.com -p 329cnjdcsdw2
    ```

!!! response

    ```text
    Stored JSON web token in ~/.server.jwt
    ```

This will set the `$JWT` environment variable in a configuration file with the token.
You can later load this environment setting with `source ~/.server.jwt`. Or use the request
helper, which will load this file automatically.

## Request Helper

To work in an easy way a small helper is integrated. It may be called without parameters on demo server or by giving the following options on any server:

-   `-s <url>, --server <url>`
-   `-m <type>, --method <type>`
-   `-d <data>, --data <data>`
-   `<context>`

!!! example

    ```bash
    bin/server-call /users # for demo server
    bin/server-call -s http://my-server.de -m get /users
    ```

The output will also be colorized in bash. If not, install pygmentize:

```bash
brew install python
pip install pygments
```

## VS Code Rest Client

VS Code contains a great plugin called "Rest Client" with which you can write your REST call directly in the editor in a very simple format like it is send using plain HTTP. The Format has to be also set to HTTP then you could send it using ++ctrl+r++ or "Send Request" from the context menu. The result will show formatted with syntax highlighting in the right side tab.

=== "HTTP"

    ```bash
    @server = http://localhost:3030

    ### Authenticate

    # @name login
    POST {{server}}/authentication
    Content-Type: application/json

    {
      "strategy": "local",
      "email": "demo@alinex.de",
      "password": "demo123"
    }

    ### Collect JSON Web Token

    @JWT = {{login.response.body.accessToken}}

    ### Run checkup with authenticated user

    GET {{server}}/checkup/host.uptime
        ?reporter=status
    Authorization: Bearer {{JWT}}
    ```

Find a more detailed description under my [VS Code](https://alinex.gitlab.io/env/vscode.md#rest-client) description. There you will find a description including use of variables and environments.

As this is the best way to show what is called it will be the primary method in showing examples in this book.

## Swagger Server API

This is the API documentation and can also be used to test the REST server by calling single services.

Call it under: `/swagger` which is also linked from the server home page with the "API Docs" Button.

![swagger](swagger.png)

### Swagger Authentication

1.  Get a JWT Token

    -   Open POST /authentication
    -   Click "Try it out" on the right side
    -   Add the following text as body parameter:

            { "strategy": "local", "email": "demo@alinex.de", "password": "demo123" }

    -   Click "Execute"
    -   Find the "Response body" below
    -   Copy the "accessToken" value

2.  Set Authentication token

    -   Button "Authorization" on the top right
    -   Insert copied accessToken as value in popup
    -   Click button "Authorize"
    -   Click button "Close"

3.  Use secured Methods

    The lock on the right is now black (gray before) which indicates that authorization is active now,
    You can now execute all these methods.

## GUI Client

For real business logic a user friendly GUI may be used which is present as separate project: [alinex-gui](https://alinex.gitlab.io/node-gui). This contains a web, desktop and mobile application which can be integrated into the server.

After [installation](install.md) this GUI is also integrated into the server so that you may download the application or use the Web version directly on the server.

{!docs/assets/abbreviations.txt!}
