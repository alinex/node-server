title: Install

# Server Installation

## NodeJS Installation

To get the server up and running you have to install it on a linux system.

First if you don't have NodeJS v10 or newer running install it:

=== "Arch Linux"

    ```bash
    sudo pacman -S nodejs npm
    sudo pacman -S python-pygments # used in the helper to colorize cli output
    ```

=== "Debian"

    ```bash
    curl -sL https://deb.nodesource.com/setup_15.x | sudo -E bash -
    sudo apt-get install -y nodejs
    sudo apt-get install -y python-pygments # used in the helper to colorize cli output
    ```

## Package Install

Then you can simply install the NPM package:

```bash
npm install -g @alinex/server
```

Alternatively you may also download and install the code from gitlab:

```bash
clone https://gitlab.com/alinex/node-server
cd node-server
npm install
```

To let the server run in production mode by default set:

```bash
echo "export NODE_ENV=production" >> ~/.bash_profile
source ~/.bash_profile
```

### Use native Postgres driver

This is not necessary, but can bring you a better performance for a production system.

You need PostgreSQL client libraries & tools installed. An easy way to check is to type `pg_config`. If `pg_config` is in your path, you should be good to go. If it's not in your path you'll need to install:

=== "Arch Linux"

    ```bash
    sudo pacman -S postgresql-libs
    ```

=== "Debian"

    ```bash
    sudo apt-get install libpq-dev g++ make
    ```

=== "RHEL/CentOS"

    ```bash
    sudo yum install postgresql-devel
    ```

=== "OS X"

    ```bash
    sudo brew install postgres
    ```

=== "Windows"

    - Install Visual Studio C++ (successfully built with Express 2010). Express is free.
    - Install PostgreSQL (http://www.postgresql.org/download/windows/)
    - Add your Postgre Installation's bin folder to the system path (i.e. C:\Program Files\PostgreSQL\9.3\bin).
    - Make sure that both libpq.dll and pg_config.exe are in that folder.

Now `pg_config` should be in your path and you can install the NodeJS module:

```bash
cd node-checkup
npm install pg-native
```

### Update Package

If it was installed through NPM you only have to call the update command:

```bash
npm update -g @alinex/server
```

If installed from git, do the following from within the application directory:

```bash
# remove local changes
$ git stash save --keep-index
$ git stash drop
# get new code and modules
$ git pull
$ npm install
```

## Setup Server

This is necessary to initially combine all the included parts into one piece of software. The API address is needed because it will be build into the GUI.

```bash
# only the REST server
bin/server-setup
# with the included GUI
bin/server-setup --gui ../node-gui --api http://localhost:3030
```

This may take some time to build the GUI. It will produce a `dist` folder containing all the needed files to be used. Copy them to your server host.

### Certificate

If you want to use a self-signed certificate the easiest way is to use package ssl-cert. If this is installed you find the certificate ready to use at:

    /etc/ssl/certs/ssl-cert-snakeoil.pem
    /etc/ssl/private/ssl-cert-snakeoil.key

The certificate and key can be regenerated manually with the following command (needs root privileges ie sudo):

```bash
make-ssl-cert generate-default-snakeoil --force-overwrite
```

To use them copy both files to `config` folder. And add them in the [configuration section](running.md/#configuration).

### GUI Integration

If the GUI was not build together with the server you may include it manually any time. But keep in mind that the GUI needs the server address compiled into it at the moment.

```bash
# build the gui (in the gui folder):
cd ../node-gui
bin/gui-setup --api http://localhost:3030

# copy the dist folder from the gui
cd ../node-server
rm -r public/gui
cp -r ../node-gui/dist public/gui
```

## Database Setup

As some modules use MongoDB as data store you have to use a local or remote MongoDB server:

=== "Arch Linux"

    ```bash
    sudo pacman -S yay
    sudo yay -S mongodb-bin
    ```

=== "Debian"

    ```bash
    sudo apt-get install mongodb
    ```

This will install MongoDB. Afterwards you have to create a first user account by hand to be able to login. The following code shows how to do so with the included start records.

If you have already used Alinex Server and want to get a cleanup of the core tables to the original start values please call the drops first:

```bash
$ mongo alinex_server < bin/setup.mongo.clean.js
MongoDB shell version: 2.6.10
connecting to: alinex_server
```

If this is called on a new MongoDB database it will return some `false` values but will not make any problem.
Now insert the base data:

```bash
$ mongo alinex_server < bin/setup.mongo.insert.js
MongoDB shell version: 2.6.10
connecting to: alinex_server
WriteResult({ "nInserted" : 1 })
```

If you need host, port and user then call it like:

```bash
$ mongo alinex_server --host localhost --port 30222 \
  -u alinex_mongoadmin -p kapu873jud < bin/setup.mongo.insert.js
```

You may also use another name for the database instead of `alinex_server`. But you have to change it in the configuration, too.

Now you have the default user and rights and are able to do the rest online through the system itself. Use the login displayed as placeholder in the form and delete this user later.

If you use another address for your mongodb server or changed its name, set it within `config/production.yaml` as variablin the `database` section:

```yaml
database:
    core:
        client: mongodb
        connection: mongodb://alinex_mongoadmin:mi2zae8Cai@localhost:31544
```

{!docs/assets/abbreviations.txt!}
