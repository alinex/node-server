title: Overview

# How to use Guide

This part will guide you through all that is necessary to really use the server. It starts with the installation steps and will guide you through the administration and setup. But keep in mind, that this is only the base and you should need to [extend it](../development/project.md). The following will suite the core which is identically in all servers based on this.

Be patient this part will not be complete unless the complete system is stable and productive.
Also keep in mind that the system is not a fully blown application, but a starting point for an individual interface.

-   [Install Server](install.md)
-   [Running Server](running.md)
-   [Client](client.md)

See the documentation sections about [services](../service), the [framework](../framework) and the [development](../development) of own projects for more information.

This base system will give you all the things from the [demo.alinex.de](https://demo.alinex.de) but not more.

{!docs/assets/abbreviations.txt!}
