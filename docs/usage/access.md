# Access

For most elements within the system you have to be authenticated by logging in using your email address and password using the [authentication service](../service/authentication.md). Or automatic login if you come from a specified address.

![Access Entities](access-entity.svg)

Behind the scene an authorization system will hold all allowed abilities and will limit the user's work to only these. Everyone is only able to see and use the elements he is allowed to access. This is no pure relational scheme but a document oriented database holding this two collections.

## Authentication

There are multiple possibilities:

1.  If a **JSON web token** is already given it is checked and the request can further be processed.
2.  If a **local login** with username and password is given, that user will be logged in and further identified by a JSON web token.
3.  If the access comes from a **registered IP** range an automatic login as the corresponding user takes place. The processing will go further on.
4.  At last the user will be seen as **"Guest User"** with the abilities of this user.

The login through IP ranges is mainly used to allow system access with simple curl calls.

## Authorization

Access to data and services is restricted by authentication and authorization using user, roles and abilities. We combine here a Role Based Access Control with Attribute Based Access Control.

**Role Based Access Control** (RBAC) relies on the grouping of users into various roles which are then assigned rights. A right is typically made up of an action and a resource type, e.g. role manager can create (action) documents (resource type). Roles in RBAC systems are usually considered as static.

**Attribute Based Access Control** (ABAC), allows to enforce authorization decisions based on any attribute accessible to your application and not just the user’s role. This could be an attribute of the subject, the action he is performing, the resource he is trying to manipulate, or even the context. The combination of these four things allows for policies to be set up that are as fine-grained as you desire. Roles in ABAC systems are dynamic, a unique role might even be generated for each user.

The downside of ABAC is that administration gets complex and overview of who is allowed for what is not as easy. Therefore we define ABAC within central roles and give them to the users as abilities.

After the user is identified the authorization will check the user's abilities. Which is what the user is allowed to do.

### Roles

For easy administration the user rights are specified through roles which combine some abilities to a named group. Each user can now be set on multiple roles, which define what he is allowed to do.

-   name: string
-   description?: string
-   disabled?: flag // default is `false`
-   abilities: list

The roles are stored in the MongoDB collection `roles` containing also the defined abilities as a list.

Two special roles are existing which do not be used by references from the user but directly hard coded from the authorize hook:

-   `Local` - used if not authenticated but used locally (easier CLI use)
-   `Guest` - used if not authenticated as specific user

### Abilities

An ability is a right to do something. It contains:

-   action: 'read', 'update', 'create', 'remove'
-   subject: name of service
-   conditions?: { field: check, ... } // JSON definition like used in MongoDB
-   fields?: name, ...
-   inverted?: flag // default is `false`
-   reason?: string // mainly to specify why user can't do something. See forbidden reasons for details

All positive rules will be treated as (OR) alternatives. The same goes for inverted rules. But keep in mind that inverted rules goes over normal ones and are not over-writable in any group.

Within the conditions the following special values may be used:

-   `$MYSELF` which is replaced by the current user id

This allows to be able to update own records but not others.

The specified fields can also contain nested fields using dot notation. Wildcard is also possible using `*` to match anything except dots and `**` to match everything.

!!! Note

    Try to give permissions, not take them away. Inverted permissions should be used seldom.

## Administration

Just from the start the system comes with four basic roles:

-   Guest - show info, login or create account
-   Local - only useable from localhost (not logged in CLI user)
-   User - show info, edit own user information, login / logout
-   Admin - anything allowed

But this can be completely changed by you through the administration dialogs described below. To do this you need full rights on roles and users as set in the 'Admin' role.

Each user or role may be disabled which will work immediately.

Have a look at the [GUI](https://alinex.gitlab.io/node-gui/usage/access/) to see an example on how this may look in a client app.

{!docs/assets/abbreviations.txt!}
