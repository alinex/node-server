title: Running

## Run Production Server

Start the server from it's installation directory using:

```bash
npm start
```

Or from within the `dist` folder or there it is deployed. Use the command from the `bin` directory:

```bash
bin/server
```

By default the `production.yaml` setup will be used, but you can switch to any other using:

```bash
SERVER_CONFIG=test bin/server  # select config using enviorpnment
bin/server --config test       # select config using parameter
```

Also available are the following options:

```text
-V                  show version

-h                  display man page
--help

-c <type>           specify configuration to use
--config <type>

-r                  restart server on any file changes
--restart
```

The `--restart` option may be used on test or staging machines to automatically restart if code has been updated.

### Run as Daemon

On linux systems the `systemd` makes modules like forever, monit, PM2, etc unnecessary.
Copy the service file into the system:

```bash
sudo cp bin/alinex_server.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable alinex_server
sudo systemctl start alinex_server
```

Now you may use `systemctl` to start and stop the server.

### Reverse Proxy

You should always use a reverse proxy before this application server. Although the Alinex server can handle SSL termination, it is better done in the reverse proxy. The handling of certificates in a central base and the use of Let's Encrypt is easier to handle within the reverse proxy. This also has a higher security level because only the reverse proxy has access to the private certificate not possibly all modules which are included in the server.

All in all you get the following benefits:

-   **Load Balancing** - Perform load balancing to distribute clients' requests across proxied servers, which improve the performance, scalability, and reliability.
-   **Caching** - With a reverse proxy, you can cache the pre-rendered versions of pages to speed up page load times. It works by caching the content received from the proxied servers' responses and using it to respond to clients without having to contact the proxied server for the same content every time.
-   **SSL Termination** - As an SSL endpoint for connections with the clients it will handle and decrypt incoming SSL connections and encrypt the proxied server’s responses.
-   **Compression** - If the proxied server does not send compressed responses, you can compress the responses before sending them to the clients.
-   **Mitigating DDoS Attacks** - You can limit the incoming requests and number of connections per single IP address to a value typical for regular users. You can also block or restrict access based on the client location, and the value of the request headers such as “User-Agent” and “Referer”.

A configuration for the reverse proxy:

=== "Nginx"

    ```text
    server {
        listen 80;
        server_name www.example.com example.com;

        location / {
            proxy_pass http://127.0.0.1:3030;

            # support websocket
            proxy_http_version  1.1;
            proxy_cache_bypass  $http_upgrade;
            proxy_set_header Upgrade           $http_upgrade;
            proxy_set_header Connection        "upgrade";

            # add forwarded information
            proxy_set_header X-Real-IP         $remote_addr;
            proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-Host  $host;
            proxy_set_header X-Forwarded-Port  $server_port;
        }
    }
    ```

=== "Nginx Cluster"

    ```text
    # define all 2 host with 2 app servers each
    upstream alinex {
        least_conn;
        server alinex1.example.com:3031;
        server alinex1.example.com:3032;
        server alinex2.example.com:3031;
        server alinex2.example.com:3032;
    }

    server {
        listen 80;
        server_name www.example.com example.com;

        location / {
            proxy_pass http://alinex;

            # support websocket
            proxy_http_version  1.1;
            proxy_cache_bypass  $http_upgrade;
            proxy_set_header Upgrade           $http_upgrade;
            proxy_set_header Connection        "upgrade";

            # add forwarded information
            proxy_set_header X-Real-IP         $remote_addr;
            proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-Host  $host;
            proxy_set_header X-Forwarded-Port  $server_port;
        }
    }
    ```

### HTTP/2

HTTP/2 brings a few fundamental changes:

-   All requests are downloaded in parallel, not in a queue
-   HTTP headers are compressed
-   Pages transfer as a binary, not as a text file, which is more efficient
-   Servers can “push” data even without the user’s request, which improves speed for users with high latency

In general HTTP/2 don't need SSL but as the biggest browsers did't support it without it got a standard to support HTTP/2 mostly with SSL.

In general, the biggest immediate benefit of HTTP/2 is the speed increase offered by multiplexing for the browser connections which are often hampered by high latency (i.e. slow round trip speed). These also reduce the need (and expense) of multiple connections which is a work around to try to achieve similar performance benefits in HTTP/1.1.

For internal connections (e.g. between webserver acting as a reverse proxy and back end app servers) the latency is typically very, very, low so the speed benefits of HTTP/2 are negligible. Additionally each app server will typically already be a separate connection so again no gains here.

Therefor this server don't support HTTP/2. As this server is meant to be run behind a loadbalancer like NGINX it won't make sense. But to get the best performance better use HTTP/2 on the loadbalancer.

A basic configuration to serve a HTTP/1 service using HTTP/2 from external sources will be:

=== "Nginx"

    ```text
    # redirect all HTTP to HTTPS
    server {
        listen 80 default_server;
        server_name _;
        return 301 https://$host$request_uri;
    }

    server {
        # use HTTP/2
        listen [::]:443 ssl http2;
        listen 443 ssl http2;

        server_name www.example.com example.com;

        # SSL configuration
        ssl on;
        ssl_certificate /Users/xxx/ssl/myssl.crt;
        ssl_certificate_key /Users/xxx/ssl/myssl.key;
        # maybe remove old insecure cipher suites by defining the only allowed ones
        ssl_ciphers EECDH+CHACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:EECDH+3DES:RSA+3DES:!MD5;

        location / {
            proxy_pass http://127.0.0.1:3030;

            # support websocket
            proxy_http_version  1.1;
            proxy_cache_bypass  $http_upgrade;
            proxy_set_header Upgrade           $http_upgrade;
            proxy_set_header Connection        "upgrade";

            # add forwarded information
            proxy_set_header X-Real-IP         $remote_addr;
            proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-Host  $host;
            proxy_set_header X-Forwarded-Port  $server_port;
        }
    }
    ```

## Configuration

The configuration is based on [Validator](https://alinex.gitlab.io/node-validator) which will be called before server start.

The configuration will be read from `config/<type>.yaml`. We use `yaml` here because it is easy to write and allows for comments. The type can be `production.yaml`, `test.yaml` or any other value and can be selected to be used on startup. Also the `default.yaml` will be merged into each other to fill up missing values.

A detailed help for the configuration structure is integrated and can be called using:

```bash
bin/server -h # gives the global overview
bin/server -h server.ssl # describe server.ssl part of configuration
```

The following settings are possible:

### server

Really needed are host and port setting. Better use a high port and proxy/nat requests instead of starting server as root co bind to lower ports like 80/443:

-   `host: localhost`
-   `port: 3030`

The server may also support HTTPS but this may also be done using an offloading proxy before (Hardware or nginx).

-   `ssl`
    -   `cert: config/ssl-cert-snakeoil.pem`
    -   `key: consig/ssl-cert-snakeoil.key`

The checkup will run while starting the server, before it is available on the net and will exit immediately if something went wrong.
See the configuration of the 'selfcheck' checkup below. You may disable it to bring the server faster up:

-   `checkup: false`

### log

The log level should be high like `warn` for production, lower ones on test or for debugging: error, warn, info, http, verbose, debug, silly

-   `level: verbose`

To use another log level as configured you may also overwrite it with the environment setting `LOG_LEVEL`.

The logs are also written to file in the `log` directory. This is done with color codes, so for display best is to use `less -R` or set this as default in `/etc/profile`:

```bash
alias less='less -R'
```

### database

The database section needs at least a definition for the `core` database, but more may be defined, too.

-   `core`
    -   `client: mongodb`
    -   `connection`
        -   `host: localhost`
        -   `port: 27017`
        -   `database: alinex_server`

The core connection will be stored in the app under the name `core-mongodb` and `core-mongoose` all others only under it's direct name.

Possible clients are: `mongodb`, `mongoose`, `pg`, `mysql`, `mysql2`, `mssql`, `oracledb`, `sqlite3`

All the databases are defined using:

-   `<name>`
    -  `client: mongodb`
    -  `version: 5.9` // optional for pg, mysql, oracledb
    -   `connection`
        -   `user: admin`
        -   `password: admin`
        -   `host: localhost`
        -   `port: 27017`
        -   `database: alinex`
    - `searchPath: [alinex]` // only allowed in pg

!!! Note

    If you are using oracledb you need to install the Oracle Client libraries (64-bit). 
    Follow the [installation instructions](https://oracle.github.io/node-oracledb/INSTALL.html)

### limiter

This is a protection against DOS attacks. By default the values are very restrictive, you may enhance them for higher usage.

-   `api`
    -   `interval: 1min`
    -   `max: 60`
-   `auth`
    -   `interval: 1min`
    -   `max: 6`

The limiting is based on access of the same IP using the same protocol (HTTP/WebSocket). The `api` limiter will count everything while the `auth` limiter will count only authentication calls or unauthenticated (guest) access. Therefore the auth limiter should always be more restrictive or the same as th api limiter.

### paginate

Setup of results returned:

-   `default: 10`
-   `max: 100`

## Service Configuration

Some services can have additional configuration in the main file or also use additional configuration files.
### authentication

Within the `authentication` section in the Main configuration the following setup defines how to authenticate:

-   `secret: 7Jbcr4uF1KQvbyAyVBi5AWSOLmI=73je`
-   `authStrategies: [jwt, local]`

Using JSON Web Tokens to authenticate:

-   `jwtOptions`
    -   `header`
        `typ: JWT`
    -   `audience: https://demo.alinex.de`
    -   `subject": portal`
    -   `issuer: alinex.de`
    -   `algorithm: HS256`
    -   `expiresIn: 1d`

Using local authentication:

-   `local`
    -   `usernameField: email`
    -   `passwordField: password`

### checkup

Here the general setup can be defined in the main configuration file, but it also needs a cases configuration in the `config/checkup` folder.  

**Main configuration**

The following settings are the default ones. They will be used if nothing given on request or for the scheduler if nothing is defined in the specific scheduler.

- `verbose` - default if not set, see [checkup config](https://alinex.gitlab.io/node-checkup/usage/setup/#verbose-extend)
- `extend` - default if not set, see [checkup config](https://alinex.gitlab.io/node-checkup/usage/setup/#verbose-extend)
- `analyze` - default if not set, see [checkup config](https://alinex.gitlab.io/node-checkup/usage/setup/#analyze-autofix)
- `autofix` - default if not set, see [checkup config](https://alinex.gitlab.io/node-checkup/usage/setup/#analyze-autofix)
- `concurrency` - default if not set, see [checkup config](https://alinex.gitlab.io/node-checkup/usage/setup/#concurrency)
- `store` - by default not set, see [checkup config](https://alinex.gitlab.io/node-checkup/usage/setup/#storage-engines)

For the self check, which is done on startup the above defaults are used but you can overwrite them with settings under `selfcheck` including all the same elements as above. 

- `selfcheck`
    - `verbose` 
    - `extend` 
    - `analyze` 
    - `autofix` 
    - `concurrency` 
    - `store`

And at last the scheduler can be activated by adding the basename (without extension) to the scheduler setting with at least a `cron` element on a specific case.

- `scheduler`
    - `<config>` - basename like 'default' for which specific scheduler settings are defined
        - `cron` - the default cron setting if nothing defined incase, can be `none`, see [checkup config](https://alinex.gitlab.io/node-checkup/usage/setup/#cron)
        - `verbose` 
        - `extend` 
        - `analyze` 
        - `autofix` 
        - `concurrency` 
        - `paths` - filter only this list of paths to be run if scheduled
        - `tags` - filter only this list of tags to be run if scheduled
        - `store`

**Cases definition**

Multiple configurations may exist there. They are used within the [checkup service](../service/checkup.md). All files have to be `*.yml` files like the `config/checkup/selfcheck.yml`.

Only the selfcheck if really needed but you may use the integrated checkup module to also test other services and server.
Have a look at [Alinex Checkup](https://alinex.gitlab.io/node-checkup/usage/case/) for details on how to configure and what is possible there.

{!docs/assets/abbreviations.txt!}
