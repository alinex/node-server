title: Security

# Security

Security is a critical point to use such system and as long as it accessible you should protect the system and it's data from unauthorized access internally and externally.

## Secure Protocol

The first point is to use the HTTPS protocol so that the data and URLs can be transferred without anyone watching it. This can be done by enabling HTTPS in the server directly or mostly better by using an offloading proxy like Nginx or Apache before which may also give you the ability of scaling using load balance technology.

## Access Rights

The next part for security is to setup the roles correctly and allow only what is really necessary to somebody. Also don't forget to disable accounts which are no longer working on that system or for your organization.

## Validation

To harden the system incoming data structure will be validated to ensure the logic won't work on wrong structured data.

## Log Access

And at last consider logging each access to data or at least each changing action taken with user and time for security reasons. Your users have to be informed that you will do this. This can be done in an action log which is archived specially for critical changes.

## Sensitive Data

Account data needed for login of users will be completely removed if a user is deleted.
IP addresses are stored only within the log files which should only be enabled for debugging because of data exhaustion. Submitted and returned data may also be stored in logs while debugging.

## Explorative Testing

To find real problematic parts the software may be to big to check each and everything for any possible case so you have to make clustered tests in fields you fear. How much you can check the security depends mainly on the man power you have to investigate into this.

{!docs/assets/abbreviations.txt!}
