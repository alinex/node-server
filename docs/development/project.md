title: Project

# Parent Project

To easily extend the server you have to make your own project and include the `@alinex/server` within. This so called parent project will directly contain all additional services and changes to the core.

!!! info

    We also use the term extension project which describes a module which will not superset the server but add a specific module or part to the server. This is used to separate bigger code.

    At the moment none such project exists therefore informations for this will come later.

![Project](project.svg)

As shown in the diagram, the core Alinex modules are included in special projects for the server and the GUI. The `bin/*-setup` scripts are used to build the combination of both together. It also will include the GUI within the server (see [install](../usage/install.md)).

The following steps will show you how to extend and customize it.

## Basic Setup

Install the following node modules within a new project directory:

```bash
npm init
npm install --save @alinex/server
```

Use the starter scripts under bin:

```bash
mkdir bin
cd bin
ln -s ../node_modules/@alinex/server/bin/server* .
cd ..
```

Include the configuration:

=== "package.json"

    ```json
    {
        "name": "my-server",
        "title": "MyServer",
        "version": "0.3.0",
        "description": "MyServer for IT Systems",
        "private": true,
        "keywords": [
            "REST",
            "server",
            "Backoffice"
        ],
        "homepage": "https://myserver.alinex.de",
        "repository": {
            "type": "git",
            "url": "http://gitlab.local/alinex/my-server"
        },
        "bugs": "https://jira.local/projects/AXS/summary",
        "author": {
            "name": "Alexander Schilling",
            "email": "info@alinex.de"
        },
        "contributors": [],
        "license": "UNLICENSED",
        "bin": {
            "server": "./bin/server"
        },
        "man": "./bin/server.1",
        "main": "src/index.js",
        "scripts": {
            "build": "rm -rf lib && tsc",
            "postbuild": "cd src && find . -name '*.html' -type f -exec cp --parents {} ../lib \\; && cd ..",
            "prepare": "npm run build",
            "dev": "ts-node-dev --no-notify node_modules/@alinex/server/src/cli.ts --config test",
            "postversion": "git push origin --all && git push origin --tags",
            "dist": "bin/server-setup -g ../my-gui -a http://myserver.de",
            "deploy": "rsync -av --delete dist/* root@myserver:/opt/myserver && ssh root@myserver sudo systemctl restart myserver.service",
            "start": "npm run build && node node_modules/@alinex/server/lib/cli.js"
        },
        "directories": {
            "lib": "lib/",
            "test": "test/",
            "config": "config/"
        },
        "dependencies": {
            "@alinex/core": "^1.4.4",
            "@alinex/server": "^0.7.0",
            "axios": "^0.19.2",
            "debug": "^4.1.1",
            "supervisor": "^0.12.0"
        },
        "devDependencies": {
            "@types/chai": "^4.2.9",
            "@types/debug": "^4.1.5",
            "@types/mocha": "^7.0.1",
            "@types/node": "^14.0.14",
            "chai": "^4.2.0",
            "mocha": "^8.0.1",
            "ts-mocha": "^7.0.0",
            "ts-node": "^8.10.2",
            "ts-node-dev": "^1.0.0-pre.44",
            "tslint": "^6.0.0",
            "typescript": "^3.9.6"
        },
        "engines": {
            "node": "^12.0.0",
            "npm": ">= 3.0.0"
        }
    }
    ```

=== "tsconfig.json"

    ```json
    {
        "compilerOptions": {
            /* Basic Options */
            "target": "ES2018" /* Specify ECMAScript target version: 'ES3' (default), 'ES5', 'ES2015', 'ES2016', 'ES2017', 'ES2018', 'ES2019' or 'ESNEXT'. */,
            "module": "commonjs" /* Specify module code generation: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', or 'ESNext'. */,
            "lib": ["dom", "es2015", "es5", "es6"] /* Specify library files to be included in the compilation. */,
            // "allowJs": true,                       /* Allow javascript files to be compiled. */
            // "checkJs": true,                       /* Report errors in .js files. */
            // "jsx": "preserve",                     /* Specify JSX code generation: 'preserve', 'react-native', or 'react'. */
            "declaration": true /* Generates corresponding '.d.ts' file. */,
            "declarationMap": true /* Generates a sourcemap for each corresponding '.d.ts' file. */,
            "sourceMap": true /* Generates corresponding '.map' file. */,
            // "outFile": "./",                       /* Concatenate and emit output to single file. */
            "outDir": "./lib" /* Redirect output structure to the directory. */,
            // "rootDir": "./",                       /* Specify the root directory of input files. Use to control the output directory structure with --outDir. */
            // "composite": true,                     /* Enable project compilation */
            // "incremental": true,                   /* Enable incremental compilation */
            // "tsBuildInfoFile": "./",               /* Specify file to store incremental compilation information */
            "removeComments": false /* Do not emit comments to output. */,
            // "noEmit": true,                        /* Do not emit outputs. */
            // "importHelpers": true,                 /* Import emit helpers from 'tslib'. */
            // "downlevelIteration": true,            /* Provide full support for iterables in 'for-of', spread, and destructuring when targeting 'ES5' or 'ES3'. */
            // "isolatedModules": true,               /* Transpile each file as a separate module (similar to 'ts.transpileModule'). */

            /* Strict Type-Checking Options */
            "strict": true /* Enable all strict type-checking options. */,
            // "noImplicitAny": true,                 /* Raise error on expressions and declarations with an implied 'any' type. */
            // "strictNullChecks": true,              /* Enable strict null checks. */
            // "strictFunctionTypes": true,           /* Enable strict checking of function types. */
            // "strictBindCallApply": true,           /* Enable strict 'bind', 'call', and 'apply' methods on functions. */
            // "strictPropertyInitialization": true,  /* Enable strict checking of property initialization in classes. */
            // "noImplicitThis": true,                /* Raise error on 'this' expressions with an implied 'any' type. */
            // "alwaysStrict": true,                  /* Parse in strict mode and emit "use strict" for each source file. */

            /* Additional Checks */
            "noUnusedLocals": true /* Report errors on unused locals. */,
            // "noUnusedParameters": true,            /* Report errors on unused parameters. */
            // "noImplicitReturns": true,             /* Report error when not all code paths in function return a value. */
            // "noFallthroughCasesInSwitch": true,    /* Report errors for fallthrough cases in switch statement. */

            /* Module Resolution Options */
            // "moduleResolution": "node",            /* Specify module resolution strategy: 'node' (Node.js) or 'classic' (TypeScript pre-1.6). */
            // "baseUrl": "./",                       /* Base directory to resolve non-absolute module names. */
            // "paths": {},                           /* A series of entries which re-map imports to lookup locations relative to the 'baseUrl'. */
            // "rootDirs": [],                        /* List of root folders whose combined content represents the structure of the project at runtime. */
            // "typeRoots": [],                       /* List of folders to include type definitions from. */
            // "types": [],                           /* Type declaration files to be included in compilation. */
            // "allowSyntheticDefaultImports": true,  /* Allow default imports from modules with no default export. This does not affect code emit, just typechecking. */
            "esModuleInterop": true /* Enables emit interoperability between CommonJS and ES Modules via creation of namespace objects for all imports. Implies 'allowSyntheticDefaultImports'. */,
            "resolveJsonModule": true
            // "preserveSymlinks": true,              /* Do not resolve the real path of symlinks. */
            /* Source Map Options */
            // "sourceRoot": "",                      /* Specify the location where debugger should locate TypeScript files instead of source locations. */
            // "mapRoot": "",                         /* Specify the location where debugger should locate map files instead of generated locations. */
            // "inlineSourceMap": true,               /* Emit a single file with source maps instead of having a separate file. */
            // "inlineSources": true,                 /* Emit the source alongside the sourcemaps within a single file; requires '--inlineSourceMap' or '--sourceMap' to be set. */

            /* Experimental Options */
            // "experimentalDecorators": true,        /* Enables experimental support for ES7 decorators. */
            // "emitDecoratorMetadata": true,         /* Enables experimental support for emitting type metadata for decorators. */
        },
        "include": ["./src/**/*"]
    }
    ```

## Customization

To customize set the title, version and description within the `package.json` and overwrite some of the default settings like:

- `public/logo.png` the dashboard logo (300x50px)
- `view/includes/welcome.html` to add custom content to welcome page

You may also add any additional static files here, but keep in mind not to use urls of the core. So best practice is to use a sub directory for each application part.

## Services

To add services create a directory `src/services` with module directories. They will automatically be found and loaded by the core.

## Middleware

To add middleware a directory `src/middleware` has to be added with a default module `index.ts`:

```ts
import { readdirSync } from 'fs';
import { Application } from '@alinex/server/lib/declarations';

export default function (app: Application) {
    const logger = app.get('logger');

    readdirSync(__dirname) // __dirname
    .filter(f => f.substr(0, 6) !== 'index.' && f.substr(-5) !== '.d.ts' && f.substr(-4) !== '.map')
    .forEach(e => {
            logger.silly(`Loading ${e} middleware...`);
            app.configure(require(`./${e}`).default);
        });

    // app.configure(require('any-alinex-server-extension/lib/middleware').default);
}
```

The above code will include all middleware from this directory. If extension projects are added, don't forget to include their middleware from here, too.

Logging in the middleware may be done by getting a logger from the application:

```ts
const logger = app.get('logger');
logger.debug('Running...');
```

!!! attention

    Errors in synchroneous code can be thrown:

    ```ts
    app.get('/test1', (req: any, res: any) => {
        throw new Error("Test Error");
    });
    ```

    But in asynchroneous code it should be forwarded to the `next` function:

    ```ts
    app.get('/test2', async (req: any, res: any, next: any) => {
        next(new Error("Test Error"));
    });
    ```

    You may also catch and forward it:

    ```ts
    app.get('/test2', async (req: any, res: any, next: any) => {
        myDbFunc(...)
            .then(...)
            .catch(next);
    });
    ```

{!docs/assets/abbreviations.txt!}
