title: Overview

# Development

To develop your own [Project](project.md) you can always run a development edition of the server which will reload on each code change automatically:

```bash
npm run dev
```

## Tricks

### Specific Content-type

The return value of data structures can be choosen with the accept header. But sometimes the service will already produce a preformatted result which has to be sent with a specific content-type. Therefore a hidden meta data can be used.

```ts
  .then((res: any) => {
    // return string as object with text to allow properties
    const data = typeof res === 'string' ? { text: res } : res
    // add a hidden property with meta data for formatter
    Object.defineProperty(data, '__mimetype', { value: mimetypes[params.reporter] });
    return data
  })
```

As shown above if the return data is a string it cannot get properties so you need to make an object, first. Use `text` as element name here because the formatter will automatically know to return the content only if mimetype is `text/*`.
The last part will define a property which will not show if the element keys are read but is accessible by the formatter.

{!docs/assets/abbreviations.txt!}
