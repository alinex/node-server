# Roadmap

**What's coming next?**

That's a big question on a freelancer project. The roadmap may change any time or progress may be stuck. But hopefully I will cone further on in any way.

## Problematic Updates

- [X] ~~*sqlite3  5.0.0  ❯  5.0.1  not all (arch) distributions supported*~~ [2021-03-04]
- [ ] mongoose  5.10.1  ❯  5.11.14 (without @types/mongoose) but having build problems
- [ ] mongoose  5.10.1  ❯  5.11.18  https://mongoosejs.com
- [ ] mongodb  3.6.3  ❯  3.6.4  https://github.com/mongodb/node-mongodb-native
- [ ] @casl/mongoose  3.2.2  ❯  4.0.1  https://casl.js.org
- [ ] limiter 2.0.1 problem with esm modules

## later Versions

-   validator on update users/roles
-   check passwort strength + disallowed-passwords
-   check field in abilities for update
-   \$search in users:find
-   oauth google, github if enabled in config
-   2FA
-   mailer service
-   auth management (lost-password, email verify)
-   password generator

-   log service + Log view

-   Cluster support Node/Nginx
-   feathers-sync multiple instances

-   check pg database adapters
-   check mysql database adapters
-   check mssql database adapters
-   check oacle database adapters
-   check sqlit database adapters

{!docs/assets/abbreviations.txt!}
