# Alinex Server

![server icon](https://assets.gitlab-static.net/uploads/-/system/project/avatar/14261122/server-icon.png){: .right .icon}

A pre-configured and opinionated but ready to use REST Server which can easily be used as backend to other services or any frontend type. It may come bundled with the [alinex-gui](https://alinex.gitlab.io/node_gui).

Big Features:

-   stateless using simple JSON Web Tokens for authentication
-   websocket (Realtime API) or HTTP REST
-   service oriented architecture
-   multiple response formats
-   authorization
-   client and server validation
-   detailed request, error and module logging
-   extensible with own business logic

## Requirements

The Alinex Server needs only:

-   one CPU core
-   at least 200 MiB RAM
-   about 500 MiB HDD

To use it on the internet you have to also add a nginx proxy bevor doing HTTPS offloading and port forwarding.

## Chapters

Read all about the [alinex-server](https://alinex.gitlab.io/node-server) in the chapters (top navigation menu):

-   [Usage](usage) - How to use guide
-   [Services](service) - Service API
-   [Framework](app) - Adding applications
-   [Architecture](dev) - System architecture and technologies

## Support

I don't give any paid support but you may create [GitLab Issues](https://gitlab.com/alinex/node-server/-/issues):

-   Bug Reports or Feature: Please make sure to give as much information as possible. And explain how the system should behave in your opinion.
-   Code Fixing or Extending: If you help to develop this package I am thankful. Develop in a fork and make a merge request after done. I will have a look at it.
-   Should anybody be willing to join the core team feel free to ask, too.
-   Any other comment or discussion as far as it is on this package is also kindly accepted.

Please use for all of them the [GitLab Issues](https://gitlab.com/alinex/node-server/-/issues) which only requires you to register with a free account.

{!docs/assets/stats-pdf-license.txt!}

{!docs/assets/abbreviations.txt!}
