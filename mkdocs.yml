site_name: Alinex Server
site_description: Universal Web and REST Server.
site_author: Alexander Schilling
copyright: Copyright &copy; 2018 - 2021 <a href="https://alinex.de">Alexander Schilling</a>

nav:
  - General:
      - README.md
      - CHANGELOG.md
      - roadmap.md
      - policy.md
  - Usage:
      - usage/README.md
      - usage/install.md
      - usage/running.md
      - usage/client.md
      - usage/security.md
      - usage/access.md
  - Services:
      - service/README.md
      - service/rest.md
      - service/error.md
      - Access Control:
          - service/authentication.md
          - service/users.md
          - service/roles.md
      - Core:
          - service/info.md
          - service/checkup.md
          - service/mailer.md
  - Framework:
      - framework/README.md
      - framework/files.md
      - framework/services.md
      - framework/hooks.md
      - framework/config.md
      - framework/logging.md
      - framework/validation.md
      - framework/mongoose.md
  - Development:
      - development/README.md
      - development/project.md

theme:
  name: material
  icon:
    logo: material/code-tags
  favicon: https://assets.gitlab-static.net/uploads/-/system/project/avatar/14261122/server-icon.png?width=48
  language: en
  palette:
    scheme: slate
    primary: grey
    accent: dark orange
  font:
    text: Lato
    code: Roboto Mono
  features:
    - navigation.instant

repo_name: "alinex/node-server"
repo_url: "https://gitlab.com/alinex/node-server"
edit_uri: ""

extra:
  manifest: "manifest.webmanifest"
  social:
    - icon: material/gitlab
      link: https://gitlab.com/alinex
    - icon: material/github
      link: https://github.com/alinex
    - icon: material/book-open-variant
      link: https://alinex.gitlab.io
    - icon: material/home
      link: https://alinex.de

extra_css:
  - assets/extra.css

extra_javascript:
  - assets/mathjax.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

plugins:
  - search
  - with-pdf:
      cover_subtitle: Universal Web and REST Server
      cover_logo: https://alinex.gitlab.io/assets/icons/server.png
      toc_level: 3
      output_path: alinex-server.pdf

markdown_extensions:
  - extra
  - toc:
      permalink: true
  - pymdownx.caret
  - pymdownx.tilde
  - pymdownx.mark
  - admonition
  - pymdownx.details
  - codehilite:
      guess_lang: false
      linenums: false
  - pymdownx.tabbed
  - pymdownx.superfences
  - pymdownx.inlinehilite
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
  - pymdownx.keys
  - pymdownx.smartsymbols
  - pymdownx.tasklist:
      custom_checkbox: true
  - markdown_blockdiag:
      format: svg
  - markdown_include.include
  - pymdownx.arithmatex:
      generic: true
