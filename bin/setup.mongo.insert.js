// use it with:
// mongo alinex_server < bin/setup.mongo.insert.js
//
////////////////////////////////////////////////////////////////////////
// Roles
////////////////////////////////////////////////////////////////////////
//
db.roles.insert({
	_id: ObjectId('5e6a5deeeda15707987a9954'),
	name: 'Local',
	description: 'Local Administration',
	abilities: [
		{
			action: ['read'],
			subject: ['users', 'roles', 'info', 'checkup'],
		},
	],
	createdAt: new Date('2020-03-12T17:01:34.027Z'),
	updatedAt: new Date('2020-03-12T17:01:34.027Z'),
});
db.roles.insert({
	_id: ObjectId('5b2025aedcb10b38733b9825'),
	name: 'Admin',
	description: 'Super Administrator with maximum rights',
	abilities: [
		{
			action: ['read', 'update', 'create', 'remove'],
			subject: ['users', 'roles', 'info', 'checkup'],
		},
	],
	createdAt: new Date('2018-06-12T19:57:34.027Z'),
	updatedAt: new Date('2018-06-12T19:57:34.027Z'),
});
db.roles.insert({
	_id: ObjectId('5b2416ff0eff22577bb74b09'),
	name: 'Guest',
	description: 'Basic rights for unauthenticated user',
	abilities: [
		{
			action: ['read'],
			subject: ['info'],
		},
		{
			action: ['create'],
			subject: ['users'],
		},
		{
			action: ['create'],
			subject: ['authentication'],
		},
	],
	createdAt: new Date('2018-06-15T19:43:59.755Z'),
	updatedAt: new Date('2018-06-15T19:43:59.755Z'),
});
db.roles.insert({
	_id: ObjectId('5b25771e6a792d15344f2a9f'),
	name: 'User',
	description: 'Basic user rights',
	abilities: [
		{
			action: ['read'],
			subject: ['info', 'roles'],
		},
		{
			action: ['update', 'remove', 'read'],
			subject: ['users'],
			conditions: { _id: '$MYSELF' },
		},
		{
			action: ['read'],
			subject: ['info'],
		},
		{
			action: ['remove'],
			subject: ['authentication'],
		},
	],
	createdAt: new Date('2018-06-16T20:46:22.604Z'),
	updatedAt: new Date('2018-06-16T20:46:22.604Z'),
});
//
////////////////////////////////////////////////////////////////////////
// Users
////////////////////////////////////////////////////////////////////////
//
db.users.insert({
	_id: ObjectId('429ee71276122f55a3a94796'),
	email: 'demo@alinex.de',
	password: '$2a$12$UDsnL7PeLa7qMBIN9ufi1e/3q.GiYJDct1SHw2lDo2NEwxjR8S2Iq',
	nickname: 'demo',
	disabled: false,
	createdAt: Date(1520363282271),
	updatedAt: Date(1524236599870),
	name: 'Demo User',
	position: 'Test',
	avatar: 'https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm',
	roles_id: [ObjectId('5b2025aedcb10b38733b9825')],
});
db.users.insert({
	_id: ObjectId('5ed55da5b38b92889fb62dba'),
  email: 'local@alinex.de',
  autologin: ['127.0.0.1/8', '::1'],
	name: 'Local Access',
	nickname: 'local',
	createdAt: Date(1520363282271),
	updatedAt: Date(1524236599870),
	disabled: false,
	roles_id: [ObjectId('5e6a5deeeda15707987a9954')],
});
db.users.insert({
	_id: ObjectId('5ed55da5b38b92889fb62dbb'),
	email: 'guest@alinex.de',
	nickname: 'guest',
	disabled: false,
	createdAt: Date(1520363282271),
	updatedAt: Date(1524236599870),
	name: 'Guest User',
	position: 'Test and Administration',
	roles_id: [ObjectId('5b2416ff0eff22577bb74b09')],
});
db.users.insert({
	_id: ObjectId('5f22c05c3b1e27d2c0a52ed6'),
	email: 'user@alinex.de',
	password: '$2a$12$UDsnL7PeLa7qMBIN9ufi1e/3q.GiYJDct1SHw2lDo2NEwxjR8S2Iq', // demo123
	nickname: 'smuel',
	disabled: false,
	createdAt: Date(1520363282271),
	updatedAt: Date(1524236599870),
	name: 'Normal User',
	position: 'Test',
	roles_id: [ObjectId('5b25771e6a792d15344f2a9f')],
});
