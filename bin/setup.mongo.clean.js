// use it with:
// mongo alinex_server < bin/setup.mongo.clean.js

db.roles.drop()
db.createCollection("roles")
db.roles.createIndex( { name: 'text' } )

db.users.drop()
db.createCollection("users")
db.users.createIndex( { email: 'text' } )

db.revokedTokens.drop()
db.createCollection("revokedTokens", { "capped": true, "size": 10000 })
db.revokedTokens.createIndex( { token: 'text' } )
