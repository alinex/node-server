#!/bin/sh

systemctl is-active --quiet sshd || sudo systemctl start sshd
systemctl is-active --quiet mongodb || sudo systemctl start mongodb
systemctl is-active --quiet postgresql || sudo systemctl start postgresql
