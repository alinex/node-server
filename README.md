# Alinex Server

A pre-configured, ready to use REST Server which can easily be used as backend to other services or any frontend type.

## Documentation

Find a complete manual under [alinex.gitlab.io/node-server](https://alinex.gitlab.io/node-server).

If you want to have an offline access to the documentation, feel free to download the [PDF Documentation](https://alinex.gitlab.io/node-server/server-book.pdf).

## License

(C) Copyright 2018 - 2019 Alexander Schilling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

> <https://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
