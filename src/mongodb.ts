// @ts-ignore
import { parseConnectionString as parse } from 'mongodb-core';
import { MongoClient } from 'mongodb';

import { Application } from './declarations';

export default function (name: string, setup: any) {
  return function(app: Application) {
    const logger = app.get('logger');
    logger.silly(`Initializing ${name} as connection to ${setup.client} database...`);
    let connection = `${setup.client}://`
    if (setup.connection.user) {
      connection += `${setup.connection.user}`
      if (setup.connection.password) connection += `:${setup.connection.password}`
      connection += '@'
    }
    connection += setup.connection.host
    if (setup.connection.port) connection += `:${setup.connection.port}`
    connection += `/${setup.connection.database}`
    const promise = MongoClient.connect(connection, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
        .then(client => {
            const dbName = parse(connection, () => {});
            return client.db(dbName);
        })
        .catch(error => {
            logger.error(error);
        });

    app.set(name, promise);
      }
  }
