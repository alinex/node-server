import { existsSync, readdirSync, statSync } from 'fs';
import * as Builder from '@alinex/validator/lib/schema';

const server = new Builder.ObjectSchema({
  title: 'Web Server setup',
  item: {
    host: new Builder.DomainSchema(),
    port: new Builder.PortSchema(),
    ssl: new Builder.ObjectSchema({
      // https://nodejs.org/api/tls.html#tls_tls_createsecurecontext_options
      item: {
        key: new Builder.FileSchema({
          detail: 'Private keys in PEM format.',
          readable: true
        }),
        cert: new Builder.FileSchema({
          detail: 'Cert chains in PEM format.',
          readable: true
        })
      },
      mandatory: true,
      denyUndefined: true
    }),
    checkup: new Builder.BooleanSchema()
  },
  mandatory: ['host', 'port']
});

const log = new Builder.ObjectSchema({
  title: 'Log definition',
  item: {
    level: new Builder.StringSchema({
      lowerCase: true,
      allow: ['error', 'warn', 'info', 'http', 'verbose', 'debug', 'silly']
    })
  },
  mandatory: true,
  denyUndefined: true
});

const mongo = new Builder.ObjectSchema({
  title: 'Mongo Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['mongodb', 'mongoose'] }),
    connection: new Builder.ObjectSchema({
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 27017 }),
        database: new Builder.StringSchema()
      },
      mandatory: ['host', 'port', 'database']
    })
  },
  mandatory: true,
  denyUndefined: true
});
const pg = new Builder.ObjectSchema({
  title: 'Postgres Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['pg'] }),
    version: new Builder.StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new Builder.ObjectSchema({
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 5432 }),
        database: new Builder.StringSchema()
      }
    }),
    searchPath: new Builder.ArraySchema({
      item: {
        '*': new Builder.StringSchema()
      }
    }),
  },
  mandatory: ['client', 'connection'],
  denyUndefined: true
});
const mysql = new Builder.ObjectSchema({
  title: 'MySQL Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['mysql'] }),
    version: new Builder.StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new Builder.ObjectSchema({
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 3306 }),
        database: new Builder.StringSchema()
      }
    })
  },
  mandatory: ['client', 'connection'],
  denyUndefined: true
});
const mysql2 = new Builder.ObjectSchema({
  title: 'MySQL2 Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['mysql2'] }),
    version: new Builder.StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new Builder.ObjectSchema({
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 3306 }),
        database: new Builder.StringSchema()
      }
    })
  },
  mandatory: ['client', 'connection'],
  denyUndefined: true
});
const oracledb = new Builder.ObjectSchema({
  title: 'oracledb Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['oracledb'] }),
    version: new Builder.StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new Builder.ObjectSchema({
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 1521 }),
        database: new Builder.StringSchema()
      }
    })
  },
  mandatory: ['client', 'connection'],
  denyUndefined: true
});
const mssql = new Builder.ObjectSchema({
  title: 'Microsoft SQL Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['mssql'] }),
    version: new Builder.StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new Builder.ObjectSchema({
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 1433 }),
        database: new Builder.StringSchema()
      }
    })
  },
  mandatory: ['client', 'connection'],
  denyUndefined: true
});
const sqlite3 = new Builder.ObjectSchema({
  title: 'SQLite Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['sqlite3'] }),
    connection: new Builder.ObjectSchema({
      item: {
        filename: new Builder.FileSchema({ readable: true })
      },
      mandatory: true,
      denyUndefined: true
    }),
  },
  mandatory: true,
  denyUndefined: true
});
const database = new Builder.ObjectSchema({
  title: 'Database connections',
  item: {
    'core': mongo,
    '*': new Builder.LogicSchema({
      operator: 'or',
      check: [mongo, pg, mysql, mysql2, oracledb, mssql, sqlite3]
    })
  },
  mandatory: ['core']
});

const limiter = new Builder.ObjectSchema({
  title: 'Rate Limiter',
  item: {
    '/^api|auth$/': new Builder.ObjectSchema({
      item: {
        interval: new Builder.NumberSchema({
          detail: 'How long in milliseconds to keep records of requests in memory.',
          integer: true,
          unit: { from: 's', to: 'ms' },
          min: 1
        }),
        max: new Builder.NumberSchema({
          detail: 'Max number of connections during interval before sending a 429 response.',
          integer: true,
          min: 0
        })
      },
      mandatory: true,
      denyUndefined: true
    })
  },
  mandatory: true,
  denyUndefined: true
});

const paginate = new Builder.ObjectSchema({
  title: 'Pagination setting',
  item: {
    default: new Builder.NumberSchema({
      detail: 'Sets the default number of items',
      integer: true,
      min: 1
    }),
    max: new Builder.NumberSchema({
      detail: 'Sets the maximum allowed number of items per page',
      integer: true,
      min: 1
    })
  },
  mandatory: true,
  denyUndefined: true
});

const item: any = { server, log, database, limiter, paginate };
readdirSync(`${__dirname}/services`).forEach(d => {
  if (!statSync(`${__dirname}/services/${d}`).isDirectory()) return;
  const file = `${__dirname}/services/${d}/configSchema`;
  if (!existsSync(file + '.js') && !existsSync(file + '.ts')) return;
  try {
    const def = require(file);
    if (def.default) item[d] = def.default;
  } catch (e) {
    throw new Error(`Could not integrate service schema ${file}\n${e}`)
  }
});

export default new Builder.ObjectSchema({
  title: 'Setup for alinex server',
  detail: 'The configuration is broken into different blocks.',
  item
});
