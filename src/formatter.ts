import { NotAcceptable } from '@feathersjs/errors';
import DataStore from '@alinex/datastore';

function getDataStore (data: any): DataStore {
  if (data instanceof DataStore) return data
  const ds = new DataStore()
  ds.data = data
  return ds
}

function convert (res: any, format: string) {
  const ds = getDataStore(res.data)
  ds.format(`file:///data.${format}`)
    .then(res.send.bind(res))
}

function formatter (req: any, res: any) {
  // use result specific mime type
  if (typeof res.data === 'object' && res.data.__mimetype?.match(/^text\//)) {
    res.set({ 'Content-type': res.data.__mimetype })
    res.send(res.data.text || res.data)
    return
  }
  // convert object to mimetype based on accept header
  res.format({
    'application/json': () => convert(res, 'json'),
    'application/bson': () => convert(res, 'bson'),
    'application/x-msgpack': () => convert(res, 'msgpack'),
    'application/javascript': () => convert(res, 'js'),
    'text/x-coffeescript': () => convert(res, 'cson'),
    'text/x-yaml': () => convert(res, 'yaml'),
    'text/x-ini': () => convert(res, 'ini'),
    'text/x-java-properties': () => convert(res, 'properties'),
    'text/x-toml': () => convert(res, 'toml'),
    'application/xml': () => convert(res, 'xml'),
    'text/csv': () => convert(res, 'csv'),
    'application/vnd.ms-excel': () => convert(res, 'xls'),
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': () => convert(res, 'xlsx'),
    'text/plain': () => convert(res, 'txt'),
    default: function () {
      throw new NotAcceptable(`Accepted response type ${req.headers.accept} not possible!`)
    }
  });
}

export default formatter
