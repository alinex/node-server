import Checkup from '@alinex/checkup'
import Runner from '@alinex/checkup/lib/runner'

import { load } from './config';
import config from './config';
import logger from './logger';

export async function start(): Promise<void> {
    // load or wait to finish loading config
    await load();
    // loading and starting real server
    await import('./server').then(server => server.start());
    // start checkup scheduler
    const scheduler = config('checkup.scheduler')
    if (scheduler && Object.keys(scheduler).length) {
        Object.keys(scheduler).forEach(k => {
            const setup = scheduler[k]
            const checkup = new Checkup({
                // not allowed to change config for selfcheck
                // config: config('checkup.selfcheck.config') ?? `${process.cwd()}/config/checkup/selfcheck.yml`,
                config: `${process.cwd()}/config/checkup/${k}.yml`,
                verbose: setup.verbose ?? config('checkup.verbose'),
                extend: setup.extend ?? config('checkup.extend'),
                analyze: setup.analyze ?? config('checkup.analyze'),
                autofix: setup.autofix ?? config('checkup.autofix'),
                concurrency: setup.concurrency ?? config('checkup.concurrency'),
                store: setup.store ?? config('checkup.store'),
                cron: setup.cron
            })
            checkup.scheduler({
                paths: setup.paths,
                tags: setup.tags
            }, (runner: Runner) => {
                runner.on('test', (name: string) => logger.info(`Checkup scheduler: ${runner.report('console', name)}`))
                runner.on('analyze', (name: string) => logger.info(`Checkup scheduler: ${runner.report('console', name)}`))
                runner.on('repair', (name: string) => logger.info(`Checkup scheduler: ${runner.report('console', name)}`))
            }
            ).catch((e: Error) => logger.error(config('log.level') === 'debug' ? '%s\n%s' : '%s', `Checkup scheduler: ${e.message}`, e.stack))

        })
        logger.info(`Checkup scheduler started for ${Object.keys(scheduler).join(', ')} test suite.`)
    }
}
