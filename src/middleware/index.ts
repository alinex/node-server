import { readdirSync, existsSync } from 'fs';
import { resolve } from 'path';
import { Application } from '../declarations';

export default function(app: Application) {
    const logger = app.get('logger');
    const ordered = ['swagger'];

    ordered.forEach(e => {
        logger.silly(`Loading ${e} (core) middleware...`);
        app.configure(require(`./${e}`).default);
    });

    readdirSync(__dirname) // __dirname
        .filter(f => f.substr(0, 6) !== 'index.' && f.substr(-5) !== '.d.ts' && f.substr(-4) !== '.map')
        .forEach(e => {
            if (ordered.filter(c => e.startsWith(c)).length) return;
            logger.silly(`Loading ${e} (core) middleware...`);
            app.configure(require(`./${e}`).default);
        });

    // load parent middleware
    const parent = resolve('lib/middleware')
    if (!__dirname.endsWith('/src/middleware') && parent !== __dirname && existsSync(parent))
        app.configure(require(parent).default)

}
