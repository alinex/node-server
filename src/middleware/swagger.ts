import swagger from 'feathers-swagger';

import { Application } from '../declarations';
import config from '../config';

const pjson = require(`${process.cwd()}/package.json`);

export default function(app: Application) {
    app.configure(
        swagger({
            openApiVersion: 3,
            docsPath: '/swagger',
            //uiIndex: true,
            uiIndex: `${__dirname}/swagger.html`,
            specs: {
                info: {
                    title: `${pjson.title} API`,
                    description:
                      `This is the **API documentation** for the ${pjson.title} which can also be used to **test the REST services** by calling them through web forms.`,
                    // termsOfService: <url>
                    contact: pjson.author,
                    license: pjson.private ? undefined : { name: pjson.license }, // url: not knwon in pjson
                    version: pjson.version // get version from package.json
                },
                servers: config('server.ssl')
                    ? [
                          {
                              url: `http://${config('server.host')}:${config('server.port')}/`,
                              description: 'HTTP'
                          },
                          {
                              url: `https://${config('server.host')}:${config('server.port')}/`,
                              description: 'HTTPS'
                          }
                      ]
                    : undefined,
                components: {
                    securitySchemes: {
                        bearer: {
                            type: 'http',
                            scheme: 'bearer',
                            bearerFormat: 'JWT',
                            description:
                                'Get an JWT API key using **POST authentication**. The `accessToken` returned should be given here as `value` to authenticate for further calls.'
                        }
                    },
                    responses: {
                        Unauthorized: {
                            description: 'Authorization information is missing or invalid.'
                        },
                        NotFound: {
                            description: 'Object with the specified ID was not found.'
                        },
                        NotAcceptable: {
                          description: 'The result format specified within the accept header is not possible.'
                        },
                        ValidationError: {
                          description: 'Validation failed, the request is not processable because of problems in the request data.'
                        },
                        InternalError: {
                            description: 'Unexpected internal error.'
                        }
                    }
                },
                security: [ { bearer: [] } ],
                externalDocs: {
                    description: 'Documentation',
                    url: pjson.homepage
                }
            },
            ignore: { tags: ['mailer'] }
        })
    );
}
