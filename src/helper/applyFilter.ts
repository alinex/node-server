import { sorter, select, filterQuery } from '@feathersjs/adapter-commons';
import sift from 'sift';

// Use it like shown below with array or object
//
// const result = await getData(...)
//   .then(e => applyFilter(e, params));

export default async function(data: any, params: any, pageing = false) {
    // filter collected data
    const { query, filters } = filterQuery(params.query || {});
    // console.log(query, filters)
    if (filters.$select) {
      data = select(params)(data);
    }
    if (!Array.isArray(data)) return data
    // work with arrays
    data = data.filter(sift(query));
    const total = data.length;
    if (filters.$sort) {
        data.sort(sorter(filters.$sort));
    }
    if (filters.$skip) {
        data = data.slice(filters.$skip);
    }
    if (typeof filters.$limit !== 'undefined') {
        data = data.slice(0, filters.$limit);
    }
    if (pageing) {
        return Promise.resolve({
            total,
            limit: filters.$limit,
            skip: filters.$skip || 0,
            data: select(params)(data)
        });
    }
    // without pageing
    return Promise.resolve(select(params)(data));
}
