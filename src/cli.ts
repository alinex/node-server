import core from '@alinex/core';

// import * as server from './index'; load on demand
import { load, describe } from './config';
import { t } from './i18nCLI';

// Support quiet mode through switch
const quiet = ['-q', '--quiet'].filter(k => process.argv.includes(k)).length;
if (!quiet) console.log(core.logo('Web and REST Server'));

process.on('uncaughtException', err => core.exit(err, 1));
process.on('unhandledRejection', (err: any) => core.exit(err, 1));

// help
let configPath: string | undefined;
process.argv.forEach((e, i) => {
  if (['--help', '-h'].includes(e)) configPath = process.argv[i + 1] || '';
});
if (configPath && configPath.substr(0, 1) === '-') configPath = '';

if (configPath !== undefined) {
  console.log(`${t('help.usage')}

    server [options]...

${t('help.general')}:

    -q                  ${t('help.option.quiet')}
    --quiet

    -h [<path>]         ${t('help.option.help')}
    --help [<path>]     ${t('help.option.help2')}

${t('help.starting')}:

    -c <type>           ${t('help.option.config')}
    --config <type>     ${t('help.option.config2')}

${t('help.config')} ${configPath}:
`);
  console.log('    ' + describe(configPath));
  if (!configPath)
    console.log(`
    ${t('help.extended')}`);
  console.log();

  process.exit(0);
}

// configuration load
let configType;
process.argv.forEach((e, i) => {
  if (['--config', '-c'].includes(e)) configType = process.argv[i + 1];
});

// loading server
if (!quiet) console.info(t('start.load'));
load(process.env.SERVER_CONFIG || configType)
  .then(() => {
    import('./index')
      .then(server => server.start())
      .then(_ => {
        if (!quiet) console.info(t('start.done'));
      });
  })
