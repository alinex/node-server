import mongoose from 'mongoose';

import { Application } from './declarations';

export default function (name: string, setup: any) {
  return function (app: Application) {
    const logger = app.get('logger');
    logger.silly(`Initializing ${name} as connection to ${setup.client} database...`);
    let connection = `${setup.client}://`
    if (setup.connection.user) {
      connection += `${setup.connection.user}`
      if (setup.connection.password) connection += `:${setup.connection.password}`
      connection += '@'
    }
    connection += setup.connection.host
    if (setup.connection.port) connection += `:${setup.connection.port}`
    connection += `/${setup.connection.database}`
    // generate mongoose setup on template and add them both
    mongoose.Promise = global.Promise;
    mongoose
      .connect(connection, {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true
      })
      .catch((err: Error) => {
        logger.error(err);
        process.exit(1);
      });

    app.set(name, mongoose);
  }
}
