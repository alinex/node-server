import favicon from 'serve-favicon';
import compress from 'compression';
import helmet from 'helmet';
import cors from 'cors';
import { RateLimiter as SocketLimiter } from 'limiter';
import HttpLimiter from 'express-rate-limit';
import { TooManyRequests, NotFound } from '@feathersjs/errors';
import { existsSync } from 'fs';
import { i18n } from './i18n'
// @ts-ignore
import i18middleware from 'i18next-http-middleware'

//import Debug from 'debug';

import feathers from '@feathersjs/feathers';
import express from '@feathersjs/express';
import socketio from '@feathersjs/socketio';

import config from './config';
import { Application } from './declarations';
import { addLogger, errorLogger, socketLogger } from './logger';
import middleware from './middleware';
import services from './services';
import appHooks from './app.hooks';
import channels from './channels';
import formatter from './formatter';
// Don't remove this comment. It's needed to format import lines nicely.

const packageInfo = require(`${process.cwd()}/package.json`);
const serverInfo = require('../package.json');
let clientInfo = {};
try {
  clientInfo = require(`${process.cwd()}/public/gui/package.json`);
} catch (e) { }

const app: Application = express(feathers());

const apiLimiter = config('limiter.api');
if (apiLimiter) app.use('/', new (HttpLimiter as any)({ windowMs: apiLimiter.interval, max: apiLimiter.max }));
const authLimiter = config('limiter.auth');
if (authLimiter)
  app.use('/authentication', new (HttpLimiter as any)({ windowMs: authLimiter.interval, max: authLimiter.max }));

app.use(i18middleware.handle(i18n))
app.set('view engine', 'ejs');
app.set('views', [`${process.cwd()}/build/views-all`, `${process.cwd()}/views-all`, `${__dirname}/../views`]);

// Enable if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
// see https://expressjs.com/en/guide/behind-proxies.html
app.set('trust proxy', true);
// Enable security, CORS, compression, favicon and body parsing
app.use(helmet(
  { contentSecurityPolicy: false }
  //  {
  //  contentSecurityPolicy: {
  //    directives: {
  //      'default-src': ["'self'"],
  //      'base-uri': ["'self'"],
  //      'block-all-mixed-content': [],
  //      'font-src': ["'self'", 'https:', 'data:'],
  //      'frame-ancestors': ["'self'"],
  //      'img-src': ["'self'", 'data:', 'http:'],
  //      'object-src': ["'none'"],
  //      'script-src': ["'self'"],
  //      'script-src-attr': ["'none'"],
  //      'style-src': ["'self'", 'https:', 'unsafe-inline'],
  //      'upgrade-insecure-requests': [],
  //    }
  //  }
  //}
));
app.use(cors());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.configure(addLogger);

// Host the public folder
if (existsSync('public/favicon.ico'))
  app.use(favicon('public/favicon.ico'));
else
  app.use(favicon(`${__dirname}/../public/favicon.ico`));
app.use('/', express.static('public'));
app.use('/', express.static(`${__dirname}/../public`));
app.get('/', (req: any, res: any) => {
  res.render('index', { packageInfo, serverInfo, clientInfo });
});

// Set up Plugins and providers
app.configure(express.rest(formatter));
// Bridge request to feathers
app.use(function (req, res, next) {
  if (req.feathers) {
    // @ts-ignore
    req.feathers.clientIP = req.ip;
    // @ts-ignore
    req.feathers.t = req.t;
    // @ts-ignore
    req.feathers.lang = req.locale;
  }
  next();
});
app.configure(
  socketio(io => {
    io.on(
      'connection',
      (
        socket: SocketIO.Socket & {
          socketLimiter: SocketLimiter;
          authSocketLimiter: SocketLimiter;
        }
      ) => {
        if (apiLimiter) {
          socket.socketLimiter = new SocketLimiter(apiLimiter.max, apiLimiter.interval);
          // for v 2.0
          //          socket.socketLimiter = new SocketLimiter({
          //            tokensPerInterval: apiLimiter.max,
          //            interval: apiLimiter.interval
          //          });
          socket.use((packet, next) => {
            if (!socket.socketLimiter.tryRemoveTokens(1)) {
              // if exceeded
              const message = 'Too many requests in a given amount of time (rate limiting)';
              socket.emit('rate-limit', new TooManyRequests(message));
              // Add a timeout so that error message is correctly handled
              setTimeout(() => socket.disconnect(true), 3000);
              return;
            }
            next();
          });
        }
        if (authLimiter && authLimiter.websocket) {
          socket.authSocketLimiter = new SocketLimiter(authLimiter.max, authLimiter.interval);
          // for v 2.0
          //          socket.authSocketLimiter = new SocketLimiter({
          //            tokensPerInterval: authLimiter.max,
          //            interval: authLimiter.interval
          //          });
          socket.on('authenticate', data => {
            // We only limit password guessing
            if (data.strategy === 'local') {
              if (!socket.authSocketLimiter.tryRemoveTokens(1)) {
                // if exceeded
                const message =
                  'Too many authentication requests in a given amount of time (rate limiting)';
                socket.emit('rate-limit', new TooManyRequests(message));
                // Add a timeout so that error message is correctly handled
                setTimeout(() => socket.disconnect(true), 3000);
              }
            }
          });
        }
      }
    );
    io.use((socket: SocketIO.Socket & { feathers?: any }, next) => {
      socketLogger(app, socket)
      socket.feathers.clientIP = socket.conn.remoteAddress; //socket.handshake.address;
      next();
    });
  })
);

// database setup
const database = config('database');
Object.keys(database).forEach(k => {
  const setup = database[k]
  if (k === 'core') {
    app.configure(require(`./${setup.client}`).default('core-mongo', setup))
    app.configure(require(`./mongoose`).default('core-mongoose', setup))
  } else if (['pg', 'mysql', 'mysql2', 'mssql', 'oracledb', 'sqlite3'].includes(setup.client)) {
    app.configure(require(`./knex`).default(k, setup))
  } else {
    app.configure(require(`./${setup.client}`).default(k, setup))
  }
})

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// Could not find any handler
app.use((req, res, next) => {
  throw new NotFound(`Page not found: ${req.url}`);
});

// Error handling
app.use(errorLogger);
app.use(function (err: Error, req: any, res: any, next: Function) {
  // get meta
  const errors: { [index: string]: { code: number; message: string; detail: string } } = {
    BadRequest: { code: 400, message: 'Bad Request', detail: err.message },
    NotAuthenticated: { code: 401, message: 'Not Authenticated', detail: 'You need to be logged in to access.' },
    PaymentError: { code: 402, message: 'Payment Required', detail: err.message },
    Forbidden: { code: 403, message: 'Not Allowed', detail: 'You are not allowed to access this resource.' },
    NotFound: { code: 404, message: 'Resource not Found', detail: 'Please verify your address/link.' },
    MethodNotAllowed: { code: 405, message: 'Method not Allowed', detail: err.message },
    NotAcceptable: { code: 406, message: 'Not Acceptable', detail: err.message },
    Timeout: { code: 408, message: 'Request Timeout', detail: err.message },
    Conflict: { code: 409, message: 'Conflict', detail: err.message },
    LengthRequired: { code: 411, message: 'Length Required', detail: err.message },
    Unprocessable: { code: 422, message: 'Unprocessable Entity', detail: err.toString() },
    ValidationError: { code: 422, message: 'Unprocessable Entity', detail: err.toString() },
    TooManyRequests: { code: 429, message: 'Too Many Requests', detail: err.message },
    //GeneralError: { code: 500, message: 'Internal Server Error', detail: err.message },
    NotImplemented: { code: 501, message: 'Not Implemented', detail: `Not implemented: ${err.message}!` },
    BadGateway: { code: 502, message: 'Bad Gateway', detail: err.message },
    Unavailable: { code: 503, message: 'Service Unavailable', detail: err.message },
  };
  const error = errors[err.constructor.name] || {
    code: 500,
    message: 'Internal Server Error',
    detail: `${err.message}\nPlease contact the administrator for further help.`
  };
  if (error.code === 401)
    res.set('WWW-Authenticate', `Bearer realm="${config('authentication.jwtOptions.issuer')}"`)
  const type = req.headers.accept;
  // set status code
  res.status(error.code);
  if (type && type == 'application/json') res.json(error);
  else res.render(`error`, { error, packageInfo, serverInfo, clientInfo });
});

app.hooks(appHooks);

export default app;
