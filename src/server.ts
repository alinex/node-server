import app from './app';

import config from './config';
import { selfcheck } from './services/checkup/data';
import { t } from './i18nCLI';

export async function start(): Promise<void> {
  const host = config('server.host');
  const port = config('server.port');
  const logger = app.get('logger');

  if (config('server.checkup'))
    await selfcheck(logger.verbose)

  logger.verbose(t('start.bind'));
  var server;
  const ssl = config('server.ssl');
  if (ssl) {
    const fs = require('fs');
    const https = require('https');
    server = https.createServer(
      {
        key: fs.readFileSync(ssl.key),
        cert: fs.readFileSync(ssl.cert)
      },
      app
    );
  } else {
    const http = require('http');
    server = http.createServer(app);
  }
  server.listen(port);
  app.setup(server);

  server.on('listening', () => {
    const proto = ssl ? 'https' : 'http';
    logger.info(t('start.listen', { url: '%s://%s:%d' }), proto, host, port);
  });
  return;
}
