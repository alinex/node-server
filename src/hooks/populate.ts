import { Hook } from '@feathersjs/feathers';
import { populate, PopulateOptions } from 'feathers-hooks-common';

import { hookLogger } from '../logger';

export default (options: PopulateOptions): Hook => {
    return async context => {
      // @ts-ignore
        hookLogger(context, __filename, options.schema.include?.parentField || options.schema.parentField);

        const result: any = await populate(options)(context);
        if (result) {
            delete result.result._include;
            delete result.dispatch._include;
        }

      // @ts-ignore
        const nameAs: string = options.schema.include?.nameAs || options.schema.nameAs || 'list';
        hookLogger(result, __filename, `added ${result.result[nameAs].length} ${nameAs}`, true);
        return result;
    };
};
