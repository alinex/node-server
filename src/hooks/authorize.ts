import { Hook } from '@feathersjs/feathers';
import { Ability } from '@casl/ability';
// import { Ability, subject } from '@casl/ability';
import { toMongoQuery } from '@casl/mongoose';
import { Forbidden } from '@feathersjs/errors';
// import { packRules } from '@casl/ability/extra';

import { hookLogger } from '../logger';
import { authenticate } from './authenticate';
import abilities from './abilities'

const TYPE_KEY = Symbol.for('type');

// don't use casl alias becuase needed for debugging, too
const actionMap: { [index: string]: string } = {
    patch: 'update',
    find: 'read',
    get: 'read',
    delete: 'remove'
};

// hook to check if user is allowd
export default (name: string | null = null): Hook => {
    return async context => {
        // no checking for internal calls or authentication
        if (!context.params.provider || context.params.provider === 'internal') return context;
        // check is allowed without rights for local ip
        if (['check'].includes(context.path) && ['127.0.0.1', '::ffff:127.0.0.1'].includes(context.params.clientIP))
            return context;

        let result = <any> await authenticate()(context);
        if (!result) return;
        context = result;

        // get meta data
        const action = actionMap[context.method] || context.method;
        const service = name ? context.app.service(name) : context.service;
        const serviceName = name || context.path;
        const logger = context.app.get('logger');

        abilities()(context) // load abilities if not present

        hookLogger(context, __filename, `${action} ${serviceName} ${context.id || ''}`);

        const ability = new Ability(context.params.user?.abilities || context.params.abilities);
//      hookLogger(context, __filename, `read users -> ${ability.can('read', 'users')}`);
//      hookLogger(context, __filename, `read self -> ${ability.can('read', new users({ _id: '5f22c05c3b1e27d2c0a52ed6' }))}`);
//      hookLogger(context, __filename, `read all -> ${ability.can('read', new users({}))}`);
//      hookLogger(context, __filename, `read all -> ${ability.can('read', subject('users', {}))}`);
//      hookLogger(context, __filename, `read any -> ${ability.can('read', new (class info { })())}`);
//      hookLogger(context, __filename, `read any -> ${ability.can('read', eval('new (class info { })()'))}`);
//      hookLogger(context, __filename, `read any -> ${ability.can('read', subject('info', {}))}`);
//      hookLogger(context, __filename, `read any -> ${packRules(ability.j)}`);

      // helper to check rights
        const throwUnlessCan = (action: string, resource?: any | string) => {
            if (ability.cannot(action, serviceName))
                throw new Forbidden(`You are not allowed to ${action} ${resource ? 'this' : 'any'} ${serviceName}`);
        };

        // check for create
        if (context.method === 'create') {
            context.data[TYPE_KEY] = serviceName;
            throwUnlessCan('create', context.data);
            return context;
        }
        // direct method call without element id
        if (!context.id) {
            // check abilities
            throwUnlessCan(action);
            const query = toMongoQuery(ability, serviceName, action);
            if (query !== null) {
                Object.assign(context.params.query, query);
            } else {
                // The only issue with this is that user will see total amount of records in db
                // for the resources which he shouldn't know.
                // Alternative solution is to assign `__nonExistingField` property to query
                // but then feathers-mongoose will send a query to MongoDB which for sure will return empty result
                // and may be quite slow for big datasets
                const query: any = context.params.query;
                query.$limit = 0;
            }
            hookLogger(context, __filename, 'OK', true);
            return context;
        }

        // get object to check it's data
        const params = Object.assign({}, context.params, { provider: null });
        const data = await service.get(context.id, params);
        // check abilities
        if (typeof data === 'object') data[TYPE_KEY] = serviceName;
        throwUnlessCan(action, result);
        // if it is a simple get call we can just use the already retrieved result
        if (action === 'get') {
            context.result = data;
            logger.verbose('use already fetched result');
        }

        hookLogger(context, __filename, 'OK', true);
        return context;
    };
};
