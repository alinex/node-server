import * as feathersAuthentication from '@feathersjs/authentication';
import { NotAuthenticated } from '@feathersjs/errors';
import { Hook, HookContext } from '@feathersjs/feathers';
import { AuthenticateHookSettings } from '@feathersjs/authentication/lib/hooks/authenticate';
import * as ipaddr from 'ipaddr.js';
import { Db } from 'mongodb';

import { hookLogger } from '../logger';

function hasToken (hook: any) {
  if (hook.params.headers == undefined) return false;
  if (hook.data?.accessToken == undefined) return false;
  return hook.params.headers.authorization || hook.data.accessToken;
}

//export function isLocal (context: HookContext): boolean {
//  const addr = ipaddr.parse(context.params.clientIP);
//  return addr.range() === 'loopback';
//}

async function loadUser (context: HookContext, user: string) {
  const userService = context.app.service('users');
  const list = await userService.find({ query: { '$limit': 1, nickname: user } })
  context.params.user = await userService.get(list.data[0]._id)
}

const verifyJWT = feathersAuthentication.hooks.authenticate('jwt');

export function authenticate (method?: string | AuthenticateHookSettings, ...strat: string[]): Hook {
  const verify = method ? feathersAuthentication.hooks.authenticate.apply(undefined, [method, ...strat]) : verifyJWT;

  return async (context: HookContext) => {
    // don't check again for internal calls
    if (!context.params.provider || context.params.provider === 'internal') return context;
    if (context.params.authenticated) return context;

    hookLogger(context, __filename);

    if (context.type !== 'before')
      throw new Error(`The 'authenticate' hook should only be used as a 'before' hook.`);
    const userService = context.app.service('users');
    try {
      await verify(context);
      if (context.params.anonymous === true) {
        // use guest user
        const list = await userService.find({ query: { '$limit': 1, nickname: 'guest' } })
        context.params.user = await userService.get(list.data[0]._id)
      }
    } catch (error) {
      // check for ip login
      const list = await userService.find({ query: { autologin: { $type: 'array' } } })
      if (list.data.length) {
        let addr = ipaddr.parse(context.params.clientIP);
        let user: any = null;
        // @ts-ignore
        if (addr.isIPv4MappedAddress()) addr = addr.toIPv4Address()
        list.data.forEach((u: any) => {
          u.autologin.forEach((r: any) => {
            let range: [ipaddr.IPv4 | ipaddr.IPv6, number];
            if (r.match(/\//)) range = ipaddr.parseCIDR(r);
            else {
              const ip = ipaddr.parse(r);
              range = ip.kind() === 'ipv4' ? [ip, 32] : [ip, 128];
            }
            try {
              if (addr.match(range)) user = u
            } catch (e) {}
          })
        })
        if (user) {
        await loadUser(context, user.nickname);
        return context;
        }
      }
      if (error instanceof NotAuthenticated && !hasToken(context)) {
        await loadUser(context, 'guest');
        hookLogger(context, __filename, 'guest', true);
        return context;
      }
      throw error
    }
    const user = context.params.user;
    if (user && user.disabled) throw new NotAuthenticated('This user has been disabled');
    const db: Db = await context.app.get('core-mongo');
    const col = db.collection('revokedTokens');
    if (await col.find({ token: context.params.authentication?.accessToken }).count())
      throw new NotAuthenticated('This token was already revoked');

    hookLogger(context, __filename, user?._id, true);
    return context;
  };
}

export default authenticate;
