import { TooManyRequests } from '@feathersjs/errors';
import { RateLimiter } from 'limiter';
import { HookContext } from '@feathersjs/feathers';

import { hookLogger } from '../logger';

export default (options: { interval: number; max: number }) => {
    const limiter = new RateLimiter(options.max, options.interval);

    return async (context: HookContext) => {
        hookLogger(context, __filename);
        if (context.type !== 'before') throw new Error(`The 'rateLimit' hook should only be used as a 'before' hook.`);

        if (!limiter.tryRemoveTokens(1)) {
            // if exceeded
            throw new TooManyRequests('Too many requests in a given amount of time (rate limiting) on service ');
        }

        hookLogger(context, __filename, undefined, true);
        return context;
    };
};
