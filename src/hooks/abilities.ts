import { Hook, HookContext } from '@feathersjs/feathers';
import { Application } from '@feathersjs/express';

import { hookLogger } from '../logger';
//import { isLocal } from './authenticate';
import createRoleModel from '../models/roles';

async function getRoleAbilities (context: HookContext, role: string): Promise<any> {
    const app = <Application>context.app;
    const roleModel = createRoleModel(app);
    return (await roleModel.find({ name: 'Local' }))[0].abilities;
}

function replaceConditionVariables(condition: any, user: any) {
    if (!condition) return null
    Object.keys(condition).forEach((key) => {
        if (typeof condition[key] === 'object') {
            replaceConditionVariables(condition[key], user)
        } else if (condition[key] === '$MYSELF') {
            condition[key] = user._id
        }
    })
}

export default (name: string | null = null): Hook => {
    return async (context:HookContext): Promise<HookContext> => {

        const user = context.result?.user || context.params?.user
        if (user.abilities) return context

        hookLogger(context, __filename, `for user ${user.nickname || user.email || 'guest'}`);

        let rules = [];
        if (user) {
            const roles = user.roles?.filter((role: any) => !role.disabled)
                .map((role: any) => role.abilities)
            rules = [].concat.apply([], roles) // combine rules from different role definitions together
//        } else if (isLocal(context)) {
//            rules = await getRoleAbilities(context, 'Local');
        } else {
            rules = await getRoleAbilities(context, 'Guest');
        }
        // substitutions
        rules.forEach((rule: any) => replaceConditionVariables(rule.conditions, user))
        // set result
        if (user) user.abilities = rules
        else context.params.abilities = rules

        hookLogger(context, __filename, `added ${user.abilities.length} rules`, true);

        return context;
    };
};
