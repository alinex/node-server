import { HookContext } from '@feathersjs/feathers';
import { BadRequest } from '@feathersjs/errors';
import { getItems } from 'feathers-hooks-common';

import { hookLogger } from '../logger';
// @ts-ignore
import taiPasswordStrength from 'tai-password-strength';

import config from '../config';

const strengthTester = new taiPasswordStrength.PasswordStrength();

export default (options: { interval: number; max: number }) => {
    return async (context: HookContext) => {
        hookLogger(context, __filename);
        if (context.type !== 'before')
            throw new Error(`The 'passwordCheck' hook should only be used as a 'before' hook.`);

        const passwordStrength = config('authentication.passwordStrength');
        let user = getItems(context);
        let password = user.password;
        if (password && passwordStrength) {
            const results = strengthTester.check(password);
            const bits = results.trigraphEntropyBits || results.shannonEntropyBits;
            if (bits < passwordStrength)
                throw new BadRequest('The provided password does not comply to the password policy.');
        }

        hookLogger(context, __filename, undefined, true);
        return context;
    };
};
