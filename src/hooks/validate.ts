import { HookContext } from '@feathersjs/feathers';
import { getItems, replaceItems } from 'feathers-hooks-common';
import { Validator } from '@alinex/validator';
import { Schema } from '@alinex/validator/lib/schema';

import { hookLogger } from '../logger';

export default (schema: Schema) => {
  const val = new Validator(schema);

  return async (context: HookContext) => {
    hookLogger(context, __filename);
    if (context.type !== 'before')
    throw new Error(`The 'validate' hook should only be used as a 'before' hook.`);

    let data = getItems(context);
    return val.load('en', { data }).then(() => {
      hookLogger(context, __filename, undefined, true);
      replaceItems(context, val.get())
    });
  };
};
