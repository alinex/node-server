import { Hook, Service } from '@feathersjs/feathers';
// @ts-ignore
import CacheMap from '@feathers-plus/cache';
import mongoose from 'mongoose';
import { cache } from 'feathers-hooks-common';

import { hookLogger } from '../logger';

function makeCacheKey(key: any) {
    return key instanceof mongoose.Types.ObjectId ? key.toString() : key;
}

export function createCache(size: number): Map<string, any> {
    return CacheMap({ max: size });
}

export function useCache(map: Map<string, any>): Hook<any, Service<any>> {
    return async context => {
        const hook = `${context.method} ${context.path} ${context.id || ''}`;
        const result: any = await cache(map, undefined, { makeCacheKey })(context);
        if (context.type === 'after') {
            hookLogger(context, __filename, hook);
        } else if (result?.result) {
            //            result.dispatch = Object.assign({}, result.result);
            result.result._isCached = true;
            hookLogger(context, __filename, hook, true);
        }
        return result;
    };
}
