import * as Builder from '@alinex/validator/lib/schema';

// this is only used for sanitization of email
export const create = new Builder.ObjectSchema({
  item: {
    strategy: new Builder.StringSchema({ allow: ['jwt', 'local']}),
    email: new Builder.EmailSchema({ normalize: true }),
    password: new Builder.StringSchema(),
    accessToken: new Builder.StringSchema()
  },
  denyUndefined: true
})
