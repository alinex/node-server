import * as Builder from '@alinex/validator/lib/schema';

const authentication = new Builder.ObjectSchema({
    // https://docs.feathersjs.com/api/authentication/service.html#configuration
    item: {
        secret: new Builder.StringSchema({ detail: 'The JWT signing secret.', min: 32 }),
        authStrategies: new Builder.ArraySchema({
            detail:
                ' A list of authentication strategy names to allow on this authentication service to create access tokens.',
            item: { '*': new Builder.StringSchema({ allow: ['local', 'jwt'] }) },
            min: 1,
            unique: true
        }),
        jwtOptions: new Builder.ObjectSchema({
            //                    keys: [
            //                        // https://github.com/auth0/node-jsonwebtoken
            //                        { key: 'usernameField', schema: new Builder.StringSchema({ min: 1 }) },
            //                        { key: 'passwordField', schema: new Builder.StringSchema({ min: 1 }) }
            //                    ],
            //                    mandatory: true
        }),
        local: new Builder.ObjectSchema({
            item: {
                // https://docs.feathersjs.com/api/authentication/local.html#configuration
                usernameField: new Builder.StringSchema({ min: 1 }),
                passwordField: new Builder.StringSchema({ min: 1 })
            },
            mandatory: true,
            denyUndefined: true
        }),
        passwordStrength: new Builder.LogicSchema({
            check: [
                new Builder.StringSchema({
                    replace: [
                        { match: 'VERY_WEAK', replace: '0' },
                        { match: 'WEAK', replace: '32' },
                        { match: 'REASONABLE', replace: '48' },
                        { match: 'VERY_STRONG', replace: '80' },
                        { match: 'STRONG', replace: '64' }
                    ]
                }),
                new Builder.NumberSchema({ integer: true, min: 0 })
            ]
        })
    },
    mandatory: true,
    denyUndefined: true
});

export default authentication;
