// Initializes the `users` service on path `/users`
import jwt_decode from "jwt-decode";
import { ServiceAddons, Params } from '@feathersjs/feathers';
import { NotFound } from '@feathersjs/errors';
import {
    AuthenticationService,
    AuthenticationRequest,
    JWTStrategy,
    AuthenticationBaseStrategy,
    AuthenticationResult
} from '@feathersjs/authentication';
import { LocalStrategy } from '@feathersjs/authentication-local';
import { Application } from '../../declarations';
import config from '../../config';
import { serviceLogger } from '../../logger';
import { Db } from 'mongodb';

import api from './api';
import hooks from './hooks';
import app from '../../app';

class Authentication extends AuthenticationService {
    public docs = api;
    private context: any;

    constructor(app: Application, path: string) {
        super(app);
        this.context = { app, path };
        app.use(path, this);
        const service: ServiceAddons<any> = app.service(path);
        service.hooks(hooks);
    }

    async create(data: AuthenticationRequest, params: Params) {
        const context = { ...this.context, params, method: 'create' };
        serviceLogger(context, data.email || data.strategy );
        params = { ...params, provider: 'internal' }; // always handle following as internal
        const result = await super.create(data, params);
        serviceLogger({ ...context, result }, result.user._id, true);
        return result;
    }

    async remove(id: string | null, params: Params) {
        const context = { ...this.context, params, method: 'remove' };
        serviceLogger(context, id);
        let result = await super.remove(id, params);
        const { accessToken } = result;
        if (accessToken) {
          const db: Db = await app.get('core-mongo');
          const col = db.collection('revokedTokens');
          const decoded = jwt_decode(accessToken);
          result = { revoked: true, accessToken: result.accessToken }
          if (decoded && !await col.find({ token: accessToken }).count())
            col.insertOne({ token: accessToken, insertedAt: new Date() });
        } else throw new NotFound('No access token given');
        serviceLogger({ ...context, result }, undefined, true);
        return result;
    }
}

class AnonymousStrategy extends AuthenticationBaseStrategy {
    async authenticate(authentication: AuthenticationResult, params: Params) {
        return {
            anonymous: true
        };
    }
}

// Add this service to the service type index
declare module '../../declarations' {
    interface ServiceTypes {
        authentication: Authentication & ServiceAddons<any>;
    }
}

export default function(app: Application) {
    app.set('authentication', {
        ...config('authentication'),
        service: 'users',
        entity: 'user'
    });
    const authentication = new Authentication(app, '/authentication');
    // set authentication strategies
    authentication.register('jwt', new JWTStrategy());
    authentication.register('local', new LocalStrategy());
    authentication.register('anonymous', new AnonymousStrategy());
}
