import abilities from '../../hooks/abilities';
import validate from '../../hooks/validate';
import * as schema from './schema';

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [validate(schema.create)],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [abilities()],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
