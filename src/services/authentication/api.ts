export default {
  description: 'Authentication Management',
  idType: 'string',
  definitions: {
    authentication: {
      title: 'Authentication',
      description: 'Data to authenticate on server',
      type: 'object',
      properties: {
        strategy: {
          description: 'Currently only the `local` strategy is allowed.',
          type: 'string',
          enum: ['local'],
          example: 'local'
        },
        email: { type: 'string', example: 'demo@alinex.de' },
        password: { type: 'string', example: 'demo123' }
      },
      required: ['strategy', 'email', 'password'],
      example: {
        strategy: 'local',
        email: 'demo@alinex.de',
        password: 'demo123'
      }
    },
    authenticationResponse: {
      title: 'Authentication Response',
      description: 'User and JSON web token',
      type: 'object',
      properties: {
        accessToken: {
          type: 'string',
          description: 'Token used to access restricted resource',
          example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTM4MDU3OTcsImV4cCI6MTU5Mzg5MjE5NywiYXVkIjoiaHR0cHM6Ly9kZW1vLmFsaW5leC5kZSIsImlzcyI6ImFsaW5leC5kZSIsInN1YiI6IjQyOWVlNzEyNzYxMjJmNTVhM2E5NDc5NiIsImp0aSI6ImQxZjM0ZDIxLTExNTMtNDFhYy1iYzFiLWJmMjY5YWI0MjRiNSJ9.Egi5Tp5StjxcrLuE5urqbr2U5Y9x34R_nBQOpLg4UqY'
        },
        authentication: {
          type: 'object',
          properties: {
            strategy: {
              description: 'Strategy used for authentication.',
              type: 'string',
              enum: ['jwt', 'local'],
              example: 'jwt'
            },
            accessToken: {
              type: 'string',
              description: 'Token used to access restricted resource',
              example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTM4MDU3OTcsImV4cCI6MTU5Mzg5MjE5NywiYXVkIjoiaHR0cHM6Ly9kZW1vLmFsaW5leC5kZSIsImlzcyI6ImFsaW5leC5kZSIsInN1YiI6IjQyOWVlNzEyNzYxMjJmNTVhM2E5NDc5NiIsImp0aSI6ImQxZjM0ZDIxLTExNTMtNDFhYy1iYzFiLWJmMjY5YWI0MjRiNSJ9.Egi5Tp5StjxcrLuE5urqbr2U5Y9x34R_nBQOpLg4UqY'
            },
            "payload": {
              type: 'object',
              properties: {
                "iat": { type: 'number', example: 1584392804, description: 'Identifies the time at which the JWT was issued.' },
                "exp": { type: 'number', example: 1584479204, description: 'Identifies the expiration time on and after which the JWT must not be accepted for processing.' },
                "aud": { type: 'string', example: "https://demo.alinex.de", description: 'Identifies the recipients that the JWT is intended for.' },
                "iss": { type: 'string', example: "alinex.de", description: 'Identifies principal that issued the JWT.' },
                "sub": { type: 'string', example: "429ee71276122f55a3a94796", description: 'Identifies the subject of the JWT.' },
                "jti": { type: 'string', example: "45a30504-7d58-4a0f-ad82-6fca8499bdf9", description: 'Case sensitive unique identifier of the token even among different issuers.' }
              }
            }
          }
        }
      },
      user: { $ref: '#/components/schemas/user' },
    example: {
      accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTM4MDU3OTcsImV4cCI6MTU5Mzg5MjE5NywiYXVkIjoiaHR0cHM6Ly9kZW1vLmFsaW5leC5kZSIsImlzcyI6ImFsaW5leC5kZSIsInN1YiI6IjQyOWVlNzEyNzYxMjJmNTVhM2E5NDc5NiIsImp0aSI6ImQxZjM0ZDIxLTExNTMtNDFhYy1iYzFiLWJmMjY5YWI0MjRiNSJ9.Egi5Tp5StjxcrLuE5urqbr2U5Y9x34R_nBQOpLg4UqY',
      authentication: { strategy: 'local' },
      user: {
        _id: '429ee71276122f55a3a94796',
        email: 'demo@alinex.de',
        nickname: 'demo',
        disabled: false,
        createdAt: 'Wed Jun 03 2020 22:03:23 GMT+0200 (CEST)',
        updatedAt: 'Wed Jun 03 2020 22:03:23 GMT+0200 (CEST)',
        name: 'Demo User',
        position: 'Test',
        avatar: 'https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm',
        roles_id: ['5b2025aedcb10b38733b9825'],
        roles: [
          {
            _id: '5b2025aedcb10b38733b9825',
            name: 'Admin',
            description: 'Super Administrator with maximum rights',
            abilities: [
              {
                action: ['read', 'update', 'create', 'remove'],
                subject: ['users', 'roles', 'info', 'checkup']
              }
            ],
            createdAt: '2018-06-12T19:57:34.027Z',
            updatedAt: '2018-06-12T19:57:34.027Z'
          }
        ],
        abilities: [
          {
            action: ['read', 'update', 'create', 'remove'],
            subject: ['users', 'roles', 'info', 'checkup']
          }
        ]
      }
    }
    },
},
securities: [],
  operations: {
  create: {
    summary: 'Get new JWT',
      description: 'Get new JWT to access restricted routes on api thanks to user info',
        parameters: [],
          requestBody: {
      required: true,
        content: {
        'application/json': {
          schema: {
            $ref: '#/components/schemas/authentication'
          }
        }
      }
    },
    responses: {
      '201': {
        description: 'JWT returned',
          content: {
          'application/json': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'application/bson': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'application/x-msgpack': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'application/javascript': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'text/x-coffeescript': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'text/x-yaml': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'text/x-ini': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'text/x-java-properties': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'text/x-toml': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'application/xml': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'text/csv': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
          'text/plain': { schema: { $ref: '#/components/schemas/authenticationResponse' } }
        }
      },
      '401': { $ref: '#/components/responses/Unauthorized' },
      '404': { $ref: '#/components/responses/NotFound' },
      '406': { $ref: '#/components/responses/NotAcceptable' },
      '500': { $ref: '#/components/responses/InternalError' }
    },
    security: []
  },
  remove: {
    summary: 'Revoke JWT',
      description: 'Removes the resource with id.',
        tags: ['authentication'],
          parameters: [
            {
              description: 'ID of authentication to remove',
              in: 'path',
              required: true,
              name: 'id',
              schema: {
                type: 'integer'
              }
            }
          ],
            responses: {
      '200': {
        description: 'success',
          content: {
              'application/json': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'application/bson': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'application/x-msgpack': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'application/javascript': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'text/x-coffeescript': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'text/x-yaml': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'text/x-ini': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'text/x-java-properties': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'text/x-toml': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'application/xml': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'text/csv': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/authenticationResponse' } },
              'text/plain': { schema: { $ref: '#/components/schemas/authenticationResponse' } }
        }
      },
      '401': { $ref: '#/components/responses/Unauthorized' },
      '404': { $ref: '#/components/responses/NotFound' },
      '406': { $ref: '#/components/responses/NotAcceptable' },
      '500': { $ref: '#/components/responses/InternalError' }
    },
    security: []
  }
},
externalDocs: {
  url: 'https://alinex.gitlab.io/node-server/service/authentication'
}
};
