import { Application } from '../declarations';
import { readdirSync, lstatSync, existsSync } from 'fs';
import { resolve } from 'path';

export default function (app: Application) {
  const logger = app.get('logger');

  readdirSync(__dirname)
    .filter(f => lstatSync(`${__dirname}/${f}`).isDirectory())
    .forEach(e => {
      logger.silly(`Loading ${e} service...`);
      app.configure(require(`./${e}`).default);
    });

  // load parent services
  let parent = existsSync('src/services') ? resolve('src/services') : resolve('lib/services')
  if (parent !== __dirname && existsSync(parent))
    readdirSync(parent)
      .filter(f => lstatSync(`${parent}/${f}`).isDirectory())
      .forEach(e => {
        logger.silly(`Loading ${e} service...`);
        app.configure(require(`${parent}/${e}`).default);
      });
//    app.configure(require(parent).default)
}
