import ObjectSchema from '@alinex/validator/lib/type/object'
import ArraySchema from '@alinex/validator/lib/type/array'
import NumberSchema from '@alinex/validator/lib/type/number'
import BooleanSchema from '@alinex/validator/lib/type/boolean'
import StringSchema from '@alinex/validator/lib/type/string'

import { store } from '@alinex/checkup/lib/setupSchema'

const settings = {
    verbose: new NumberSchema({
        title: 'Verbosity Level (higher is more verbose)',
        integer: true, min: 0, max: 9, default: 0
    }),
    extend: new BooleanSchema({ title: 'Automatically extend verbosity level in case of problem' }),
    analyze: new BooleanSchema({ title: 'Should analyzation for possible fixes be made', default: true }),
    autofix: new BooleanSchema({ title: 'Should automatic repair be run' }),
    concurrency: new NumberSchema({
        title: 'How many processes may run in parallel in each state',
        integer: true, min: 1
    }),
    store: store
}

const checkup = new ObjectSchema({
    item: {
        ...settings,
        selfcheck: new ObjectSchema({ item: settings, denyUndefined: true }),
        scheduler: new ObjectSchema({
            item: {
                '*': new ObjectSchema({
                    item: {
                        ...settings,
                        cron: new StringSchema({ allow: ['none', /^(?:[-*,/0-9]+ ){4}[-*,/0-9]+$/] }),
                        paths: new ArraySchema({
                            makeArray: true,
                            split: /\s*,\s*/,
                            item: { '*': new StringSchema() }
                        }),
                        tags: new ArraySchema({
                            makeArray: true,
                            split: /\s*,\s*/,
                            item: { '*': new StringSchema() }
                        }),
                    },
                    denyUndefined: true
                })
            }
        })
    },
    denyUndefined: true
})

export default checkup;
