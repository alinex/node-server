import { ServiceAddons, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
//import { NotFound } from '@feathersjs/errors';
//import async from '@alinex/async';

import { Application } from '../../declarations';
import config from '../../config';
import applyFilter from '../../helper/applyFilter';
import { serviceLogger } from '../../logger';
import BaseService from '../base';

import api from './api';
import hooks from './hooks';
import data from './data';

export interface Data { }

interface ServiceOptions { }

class Checkup extends BaseService implements ServiceMethods<Data | Data[]> {
  public docs = api;
  private context: any;

  options: ServiceOptions;

  constructor(app: Application, path: string) {
    super()
    this.context = { app, path };
    this.options = { paginate: config('paginate') };
    app.use(path, this);
    // shortcut paths redirected to api
    app.get(`${path}/:path/:reporter`, (req, res) => res.redirect(301, `${path}/${req.params.path}?reporter=${req.params.reporter}`));
    const service: ServiceAddons<any> = app.service(path);
    service.hooks(hooks);
  }

  async find(params: Params): Promise<Data[] | Paginated<Data>> {
    const context = { ...this.context, params, method: 'find' };
    serviceLogger(context);
    const result = await data(params?.query || {}, params.lang)
      .then(e => applyFilter(e, params));
    const abstract = result.summary
    serviceLogger({ ...context, result }, abstract, true);
    return result
  }

  async get(id: string, params: Params): Promise<Data> {
    const context = { ...this.context, params, method: 'get' };
    serviceLogger(context, id);
    const { verbose, extend, analyze, reporter, tags } = params.query || {}
    let paths = params.query?.paths || []
    const [cases, path] = id.indexOf(':') === -1 ? [undefined, id] : id.split(/:/, 2)
    if (path.length) paths.push(path)
    if (!paths.length) paths = undefined
    const result = await data({ cases, paths, tags, verbose, extend, analyze, reporter }, params.lang, path)
      .then(e => applyFilter(e, params));
    const abstract = result.summary
    serviceLogger({ ...context, result }, abstract, true);
    return result
  }

  async patch(id: string, data: any, params: Params): Promise<Data> {
    const context = { ...this.context, params, method: 'get' };
    serviceLogger(context, id);
    const { verbose, extend, analyze, reporter } = params.query || {}
    const result = await data({ paths: [id], verbose, extend, analyze, reporter, autofix: true }, params.lang, id)
    const abstract = result.summary
    serviceLogger({ ...context, result }, abstract, true);
    return result
  }

}

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    checkup: Checkup & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  new Checkup(app, '/checkup');
}
