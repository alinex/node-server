import { Suite, Execution } from '@alinex/checkup'

import config from '../../config';
import { t } from '../../i18nCLI';

const suites: { [key: string]: Suite } = {}

const mimetypes: any = {
  html: 'text/html',
  console: 'text/plain'
}

export async function selfcheck(logger: (message: string) => void) {
  const title = t('start.selfcheck')
  const startup = new Suite({
    // not allowed to change config for selfcheck
    // config: config('checkup.selfcheck.config') ?? `${process.cwd()}/config/checkup/selfcheck.yml`,
    config: `${process.cwd()}/config/checkup/selfcheck.yml`,
    verbose: config('checkup.selfcheck.verbose') ?? config('checkup.verbose'),
    extend: config('checkup.selfcheck.extend') ?? config('checkup.extend'),
    analyze: config('checkup.selfcheck.analyze') ?? config('checkup.analyze'),
    autofix: config('checkup.selfcheck.autofix') ?? config('checkup.autofix'),
    concurrency: config('checkup.selfcheck.concurrency') ?? config('checkup.concurrency'),
    store: config('checkup.selfcheck.store') ?? config('checkup.store'),
  })
  const runner = await startup.create()
  runner.on('test', (name: string) => logger(`${title}: ${runner.report('console', name, { result: false })}`))
  return runner.start()
    .then(() => {
      const summary = runner.report('list')._summary
      if (!summary.ERROR) return
      throw new Error(`There were ${summary.ERROR} errors in checkup, skip server start.`)
    })
}

export async function run(params: any, lang: string, root?: string) {
  // create new suite if not already created
  const id = `${params.cases}${params.verbose || 0}${params.extend ? '' : 'e'}`
  if (!suites[id])
    suites[id] = new Suite({
      config: params.cases ? `${process.cwd()}/config/checkup/${params.cases}.yml` : `${process.cwd()}/config/checkup/default.yml`,
      verbose: params.verbose ?? config('checkup.verbose'),
      extend: params.extend ?? config('checkup.extend'),
      analyze: params.analyze ?? config('checkup.analyze'),
      autofix: params.autofix ?? config('checkup.autofix'),
      concurrency: config('checkup.concurrency'),
      store: params.store ?? config('checkup.store'),
    })

  // define filter
  const filter: Execution = { lang }
  if (params.paths) {
    if (!Array.isArray(params.paths)) filter.paths = [params.paths]
    else filter.paths = params.paths
  }
  if (params.tags) {
    if (!Array.isArray(params.tags)) filter.tags = [params.tags]
    else filter.tags = params.tags
  }

  // run
  const runner = await suites[id].create(filter)
  return runner.start()
    .then(() => runner.report(params.reporter || 'list', root))
    .then((res: any) => {
      // return string as object with text to allow properties
      const data = typeof res === 'string' ? { text: res } : res
      // add a hidden property with meta data for formatter
      Object.defineProperty(data, '__mimetype', { value: mimetypes[params.reporter] });
      return data
    })
}

export default run
