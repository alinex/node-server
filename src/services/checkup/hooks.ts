import authorize from '../../hooks/authorize';
// Don't remove this comment. It's needed to format import lines nicely.

export default {
    before: {
        all: [authorize()],
        find: [(context: any) => {
            // default to html reporter for browser
            const p = context.params
            if (!p.query.reporter && p.headers.accept.split(',')[0] === 'text/html')
                p.query.reporter = 'html'
        }],
        get: [(context: any) => {
            // default to html reporter for browser
            const p = context.params
            if (!p.query.reporter && p.headers.accept.split(',')[0] === 'text/html')
                p.query.reporter = 'html'
        }],
        create: [],
        update: [],
        patch: [],
        remove: []
    },

    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },

    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
