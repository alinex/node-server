export default {
  description: 'System Selfchecks',
  idType: 'string',
  definitions: {
    checkup: {
      title: 'Checkup',
      type: 'object',
      properties: {
      },
      example: {
      }
    },
    checkups: {
      title: 'Checkups',
      type: 'array',
      items: {
        $ref: '#/components/schemas/checkup'
      }
    }
  },
  securities: 'all',
  operations: {
    find: {
      summary: 'Run selfcheck on system.',
      description:
        'Run some checks and get all results back together with the time used to execute. The checkups to be run may be filtered.',
      parameters: [
        {
          description: 'Case configuration to use: "default".',
          in: 'query',
          name: 'cases',
          schema: { type: 'string' }
        },
        {
          description: 'Filter checkups starting with paths.',
          in: 'query',
          name: 'paths',
          schema: {
            type: 'array',
            items: { type: 'string' }
          }
        },
        {
          description: 'Filter checkups which contain all specified tags.',
          in: 'query',
          name: 'tags',
          schema: {
            type: 'array',
            items: { type: 'string' }
          }
        },
        {
          description: 'Verbosity level in log (0..9).',
          in: 'query',
          name: 'verbose',
          schema: {
            type: 'number',
            minimum: 0
          }
        },
        {
          description: 'Extend verbosity level on failure.',
          in: 'query',
          name: 'extend',
          schema: {
            type: 'boolean'
          }
        },
        {
          description: 'Run deeper analyzation on failure.',
          in: 'query',
          name: 'analyze',
          schema: {
            type: 'boolean'
          }
        },
        {
          description: 'Result reporter to use.',
          in: 'query',
          name: 'reporter',
          schema: {
            type: 'string',
            enum: ['result', 'console']
          }
        }
      ],
      security: [{ bearer: [] }],
      responses: {
        '200': {
          description: 'success',
          content: {
            'application/json': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/bson': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/x-msgpack': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/javascript': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-coffeescript': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-yaml': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-ini': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-java-properties': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-toml': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/xml': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/csv': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/plain': { schema: { $ref: '#/components/schemas/checkups' } }
          }
        },
        '401': { $ref: '#/components/responses/Unauthorized' },
        '404': { $ref: '#/components/responses/NotFound' },
        '406': { $ref: '#/components/responses/NotAcceptable' },
        '500': { $ref: '#/components/responses/InternalError' }
      }
    },
    get: {
      summary: 'Run one specific checkup or group.',
      description: 'Run only a single checkup case or a whole group and get the result back.',
      parameters: [
        {
          description: 'The start of the checkup path.',
          in: 'path',
          required: true,
          name: 'id',
          schema: {
            type: 'string'
          },
          example: 'host.load'
        },
        {
          description: 'Verbosity level in log (0..9).',
          in: 'query',
          name: 'verbose',
          schema: {
            type: 'number',
            minimum: 0
          }
        },
        {
          description: 'Extend verbosity level on failure.',
          in: 'query',
          name: 'verbose',
          schema: {
            type: 'boolean'
          }
        },
        {
          description: 'Run deeper analyzation on failure.',
          in: 'query',
          name: 'analyze',
          schema: {
            type: 'boolean'
          }
        },
        {
          description: 'Result reporter to use.',
          in: 'query',
          name: 'reporter',
          schema: {
            type: 'string',
            enum: ['result', 'console']
          }
        }
      ],
      security: [{ bearer: [] }],
      responses: {
        '200': {
          description: 'success',
          content: {
            'application/json': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/bson': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/x-msgpack': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/javascript': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-coffeescript': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-yaml': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-ini': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-java-properties': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-toml': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/xml': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/csv': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/plain': { schema: { $ref: '#/components/schemas/checkups' } }
          }
        },
        '401': { $ref: '#/components/responses/Unauthorized' },
        '404': { $ref: '#/components/responses/NotFound' },
        '406': { $ref: '#/components/responses/NotAcceptable' },
        '500': { $ref: '#/components/responses/InternalError' }
      }
    },
    create: false,
    update: false,
    patch: {
      summary: 'Run one specific checkup or group with possible repair.',
      description: 'Run only a single checkup case or a whole group. If possible do some repair and get the result back.',
      parameters: [
        {
          description: 'The start of the checkup path.',
          in: 'path',
          required: true,
          name: 'id',
          schema: {
            type: 'string'
          },
          example: 'host.load'
        },
        {
          description: 'Verbosity level in log (0..9).',
          in: 'query',
          name: 'verbose',
          schema: {
            type: 'number',
            minimum: 0
          }
        },
        {
          description: 'Extend verbosity level on failure.',
          in: 'query',
          name: 'verbose',
          schema: {
            type: 'boolean'
          }
        },
        {
          description: 'Run deeper analyzation on failure.',
          in: 'query',
          name: 'analyze',
          schema: {
            type: 'boolean'
          }
        },
        {
          description: 'Result reporter to use.',
          in: 'query',
          name: 'reporter',
          schema: {
            type: 'string',
            enum: ['result', 'console']
          }
        }
      ],
      security: [{ bearer: [] }],
      responses: {
        '200': {
          description: 'success',
          content: {
            'application/json': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/bson': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/x-msgpack': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/javascript': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-coffeescript': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-yaml': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-ini': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-java-properties': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/x-toml': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/xml': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/csv': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/checkups' } },
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/checkups' } },
            'text/plain': { schema: { $ref: '#/components/schemas/checkups' } }
          }
        },
        '401': { $ref: '#/components/responses/Unauthorized' },
        '404': { $ref: '#/components/responses/NotFound' },
        '406': { $ref: '#/components/responses/NotAcceptable' },
        '500': { $ref: '#/components/responses/InternalError' }
      }
    },
    remove: false
  },
  externalDocs: {
    url: 'https://alinex.gitlab.io/node-server/service/checkup'
  }
};
