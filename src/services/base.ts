// Initializes the `users` service on path `/users`
import { NullableId, Paginated, Params } from '@feathersjs/feathers';
import { NotImplemented } from '@feathersjs/errors';
import { DataStore } from '@alinex/datastore'

class BaseService {

  async find (params?: Params): Promise<DataStore|any[] | Paginated<any>> {
    throw new NotImplemented('FIND/GET method');
  }

  async get (id: string, params?: Params): Promise<any | Paginated<any>> {
    throw new NotImplemented('GET/GET method');
  }

  async create (data: any, params?: Params): Promise<any> {
    throw new NotImplemented('CREATE/POST method');
  }

  async update (id: NullableId, data: any, params?: Params): Promise<any> {
    throw new NotImplemented('UPDATE/PUT method');
  }

  async patch (id: NullableId, data: any, params?: Params): Promise<any> {
    throw new NotImplemented('PATCH method');
  }

  async remove (id: NullableId, params?: Params): Promise<any> {
    throw new NotImplemented('REMOVE method');
  }
}

export default BaseService
