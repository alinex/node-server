// Initializes the `users` service on path `/users`
import { ServiceAddons, Paginated, Params } from '@feathersjs/feathers';
import { Service } from 'feathers-mongoose';
import { Application } from '../../declarations';
import config from '../../config';
import { serviceLogger } from '../../logger';

import createModel from '../../models/roles';
import hooks from './hooks';
import api from './api';

class Roles extends Service {
    public docs = api;
    private context: any;

    constructor(app: Application, path: string) {
        super({ paginate: config('paginate'), Model: createModel(app) });
        this.context = { app, path };
        app.use(path, this);
      const service: ServiceAddons<any> = app.service(path);
      service.hooks(hooks);
    }

  public async find (params?: Params): Promise<any | any[] | Paginated<any>> {
        const context = { ...this.context, params, method: 'find' };
        serviceLogger(context);
        const result = await super.find(params);
        const abstract = Array.isArray(result) ? `${result.length} records` : `${result.total} records`;
        serviceLogger({ ...context, result }, abstract, true);
        return result;
    }

    public async get(id: string, params?: Params): Promise<any[] | Paginated<any>> {
        const context = { ...this.context, params, method: 'get' };
        serviceLogger(context, id);
        const result = await super.get(id, params);
        const abstract = result.name;
        serviceLogger({ ...context, result }, abstract, true);
        return result;
    }
}

// Add this service to the service type index
declare module '../../declarations' {
    interface ServiceTypes {
        roles: Roles & ServiceAddons<any>;
    }
}

export default function(app: Application) {
    new Roles(app, '/roles');
}
