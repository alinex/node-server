import * as Builder from '@alinex/validator/lib/schema';

const fields = {
  _id: new Builder.StringSchema(),
  name: new Builder.StringSchema({ trim: true, min: 4 }),
  description: new Builder.StringSchema({ trim: true, min: 4 }),
  disabled: new Builder.BooleanSchema(),
  abilities: new Builder.ArraySchema({
    item: { '*': new Builder.ObjectSchema({
      item: {
        action: new Builder.ArraySchema({ item: { '*': new Builder.StringSchema({ min: 1 }) } }),
        subject: new Builder.ArraySchema({ item: { '*': new Builder.StringSchema({ min: 1 }) } }),
        conditions: new Builder.ObjectSchema(),
        fields: new Builder.ArraySchema({ item: { '*': new Builder.StringSchema({ min: 1 }) } })
      }
    }) }
  })
}

export const create = new Builder.ObjectSchema({
  item: fields,
  denyUndefined: true
})

export const update = new Builder.ObjectSchema({
  item: fields,
  denyUndefined: true,
  mandatory: true
})

export const patch = new Builder.ObjectSchema({
  item: fields,
  denyUndefined: true
})
