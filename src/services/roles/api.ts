export default {
    description: 'Role based access management',
    idType: 'string',
    definitions: {
        role: {
            title: 'Role',
            type: 'object',
            required: ['email', 'password'],
            properties: {
                _id: {
                    type: 'string',
                    description: 'The id of the role'
                },
                email: {
                    type: 'string',
                    description: 'Email address as unique identifier',
                    example: 'info@alinex.de'
                },
                password: {
                    type: 'string',
                    description: 'Secret password (make it unguessable)'
                },
                nickname: {
                    type: 'string',
                    description: 'A displayable pseudonym under which this role is presented'
                },
                name: {
                    type: 'string',
                    description: 'Full, real name of the role'
                },
                position: {
                    type: 'string',
                    description: 'Within this organisation'
                },
                disabled: {
                    type: 'boolean',
                    description: 'Set to false to prevent role to login'
                }
            }
        },
        roles: {
            title: 'Roles',
            type: 'array',
          items: { $ref: '#/components/schemas/role' }
        }
    },
    securities: ['find', 'get', 'update', 'patch', 'remove'],
    operations: {
        find: {
            summary: 'List roles.',
            description: 'Get a list of roles with support of filter, paging snd column selection.',
            parameters: [
                {
                    description: 'Filter email address.',
                    in: 'query',
                    name: 'email',
                    schema: {
                        type: 'string'
                    }
                },
                {
                    description: 'Filter on nickname.',
                    in: 'query',
                    name: 'nickname',
                    schema: {
                        type: 'string'
                    }
                },
                {
                    description: 'Filter on name.',
                    in: 'query',
                    name: 'name',
                    schema: {
                        type: 'string'
                    }
                },
                {
                    description: 'Filter position.',
                    in: 'query',
                    name: 'position',
                    schema: {
                        type: 'string'
                    }
                },
                {
                    description: 'Number of records to skip.',
                    in: 'query',
                    name: '$skip',
                    schema: {
                        type: 'number',
                        minimum: 1
                    }
                },
                {
                    description: 'Number of results to return.',
                    in: 'query',
                    name: '$limit',
                    schema: {
                        type: 'number',
                        minimum: 1
                    }
                },
                {
                    description:
                        'Property to sort results. \
                        The value defines the order: 1 = ascending, -1 = descending.',
                    in: 'query',
                    name: '$sort',
                    schema: {
                        type: 'object',
                        properties: {
                            email: { type: 'integer', minimum: -1, maximum: 1, description: 'Email address' },
                            nickname: { type: 'integer', minimum: -1, maximum: 1, description: 'Alias name' },
                            name: { type: 'integer', minimum: -1, maximum: 1, description: 'Full name' },
                            position: {
                                type: 'integer',
                                minimum: -1,
                                maximum: 1,
                                description: 'Position in the organization'
                            }
                        }
                    },
                    style: 'deepObject',
                    explode: true
                },
                {
                    description: 'Array of columns to return.',
                    in: 'query',
                    name: '$select',
                    schema: {
                        type: 'array',
                        items: {
                            type: 'string',
                            enum: ['_id', 'nickname', 'name', 'position']
                        }
                    },
                    style: 'form',
                    explode: true
                }
            ],
            security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/roles' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/roles' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/roles' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/roles' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/roles' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/roles' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/roles' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/roles' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/roles' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/roles' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/roles' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/roles' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/roles' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/roles' } }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '500': { $ref: '#/components/responses/InternalError' }
            }
        },
        get: {
            summary: 'Get role information.',
            description: 'Get information about a specific role.',
            parameters: [
                {
                    description: 'ID of the role',
                    in: 'path',
                    required: true,
                    name: '_id',
                    schema: {
                        type: 'string'
                    },
                    example: '429ee71276122f55a3a94796'
                }
            ],
            security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/role' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/role' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/role' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/role' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/role' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/role' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/role' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/role' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/role' } }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '500': { $ref: '#/components/responses/InternalError' }
            }
        },
        create: {
            summary: 'Create new role account',
            description: 'A new role account can be created without authentication.',
            responses: {
                '200': {
                    description: 'account created',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/role' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/role' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/role' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/role' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/role' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/role' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/role' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/role' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/role' } }
                    }
                },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '422': { $ref: '#/components/responses/ValidationError' },
                '500': { $ref: '#/components/responses/InternalError' }
            },
            security: [],
            requestBody: {
                required: true,
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/roles'
                        },
                        example: {
                            email: 'demo@alinex.de',
                            nickname: 'demo',
                            name: 'Demo User',
                            position: 'Test'
                        }
                    }
                }
            }
        },
        update: {
            summary: 'Update complete record',
            description: 'Updates the resource identified by id using data.',
            parameters: [
                {
                    description: 'ID of role to update',
                    in: 'path',
                    required: true,
                    name: '_id',
                    schema: {
                        type: 'string'
                    }
                }
            ],
            security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/role' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/role' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/role' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/role' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/role' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/role' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/role' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/role' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/role' } }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '422': { $ref: '#/components/responses/ValidationError' },
                '500': { $ref: '#/components/responses/InternalError' }
            },
            requestBody: {
                required: true,
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/roles'
                        },
                        example: {
                            email: 'demo@alinex.de',
                            nickname: 'demo',
                            name: 'Demo User',
                            position: 'Test'
                        }
                    }
                }
            }
        },
        patch: {
            summary: 'Patch role account',
            description: 'Updates the resource identified by id using data.',
            parameters: [
                {
                    description: 'ID of roles to update',
                    in: 'path',
                    required: true,
                    name: '_id',
                    schema: {
                        type: 'string'
                    }
                }
            ],
            security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/role' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/role' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/role' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/role' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/role' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/role' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/role' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/role' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/role' } }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '422': { $ref: '#/components/responses/ValidationError' },
                '500': { $ref: '#/components/responses/InternalError' }
            },
            requestBody: {
                required: true,
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/roles'
                        }
                    }
                }
            }
        },
        remove: {
            summary: 'Delete role account',
            description: 'Remove the selected role account.',
            parameters: [
                {
                    description: 'ID of roles to remove',
                    in: 'path',
                    required: true,
                    name: '_id',
                    schema: {
                        type: 'string'
                    }
                }
            ],
            security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/role' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/role' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/role' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/role' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/role' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/role' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/role' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/role' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/role' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/role' } }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '500': { $ref: '#/components/responses/InternalError' }
            }
        }
    },
    externalDocs: {
        url: 'https://alinex.gitlab.io/node-server/service/roles'
    }
};
