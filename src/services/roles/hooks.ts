import authorize from '../../hooks/authorize';
import validate from '../../hooks/validate';
import * as schema from './schema';
import { HookContext } from '@feathersjs/feathers';
import { discard, getItems, replaceItems } from 'feathers-hooks-common';

import { hookLogger } from '../../logger';

const removeEmptyField = async (context: HookContext) => {
  let data = getItems(context);
  if (data.abilities) {
    hookLogger(context, __filename + ':removeEmptyField');
    data.abilities = data.abilities.map((rule: any) => {
      if (rule.fields && !rule.fields.length) delete rule.fields
      return rule
    })
    replaceItems(context, data)
    hookLogger(context, __filename + ':removeEmptyField', undefined, true);
  }
};

export default {
  before: {
    all: [authorize()],
    find: [],
    get: [],
    create: [validate(schema.create)],
    update: [discard('createdAt', 'updatedAt', '__caslSubjectType__'), removeEmptyField, validate(schema.update)],
    patch: [discard('createdAt', 'updatedAt', '__caslSubjectType__'), removeEmptyField, validate(schema.patch)],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
