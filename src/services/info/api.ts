export default {
    description: 'Get server information.',
    idType: 'string',
    definitions: {
        info: {
            title: 'Information',
            type: 'object',
            properties: {
                group: {
                    type: 'string',
                    description: 'Setting group name',
                    example: 'group'
                },
                name: {
                    type: 'string',
                    description: 'The name of the setting',
                    example: 'name'
                },
                value: {
                    description: 'The current value',
                  example: 'mongodb://localhost:27017/alinex_server'
                }
            },
            required: ['group', 'name', 'value'],
        example: {
          group: 'database',
          name: 'core server',
          value: 'mongodb://localhost:27017/alinex_server'
        }
        },
        'infos': {
            title: 'Information Elements',
            type: 'array',
            items: {
                $ref: '#/components/schemas/info'
            }
        }
    },
    securities: 'all',
    operations: {
        find: {
            summary: 'Get complete system information.',
            description:
                'Collect a lot of system information and return them. This goes from the hardware, OS, server to the software.',
            parameters: [
                {
                    description: 'Filter on specific group.',
                    in: 'query',
                    name: 'group',
                    schema: {
                        type: 'string',
                        enum: ['host', 'env', 'node', 'server', 'service', 'database']
                    }
                },
                {
                    description: 'Number of records to skip.',
                    in: 'query',
                    name: '$skip',
                    schema: {
                        type: 'number',
                        minimum: 1
                    }
                },
                {
                    description: 'Number of results to return.',
                    in: 'query',
                    name: '$limit',
                    schema: {
                        type: 'number',
                        minimum: 1
                    }
                },
                {
                    description:
                        'Property to sort results. \
                        The value defines the order: 1 = ascending, -1 = descending.',
                    in: 'query',
                    name: '$sort',
                    schema: {
                        type: 'object',
                        properties: {
                            group: { type: 'integer', minimum: -1, maximum: 1, description: 'Group Name' },
                            name: { type: 'integer', minimum: -1, maximum: 1, description: 'Element Name' },
                            value: { type: 'integer', minimum: -1, maximum: 1, description: 'Element Value' }
                        }
                    },
                    style: 'deepObject',
                    explode: true
                },
                {
                    description: 'Array of columns to return.',
                    in: 'query',
                    name: '$select',
                    schema: {
                        type: 'array',
                        items: {
                            type: 'string',
                            enum: ['group', 'name', 'value']
                        }
                    },
                    style: 'form',
                    explode: true
                }
            ],
            security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/infos' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/infos' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/infos' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/infos' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/infos' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/infos' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/infos' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/infos' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/infos' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/infos' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/infos' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/infos' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/infos' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/infos' } }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '500': { $ref: '#/components/responses/InternalError' }
            }
        },
        get: {
            summary: 'Specific Info group or element value',
            description:
                'Get all values of one group with `<group>` as ID. Or get only one specific value using `<group>.<name>` as ID.',
            parameters: [
                {
                    description: 'The selection `<group>` or `<group>.<name>`',
                    in: 'path',
                    required: true,
                    name: 'id',
                    schema: {
                        type: 'string'
                    },
                    example: 'database.core version'
                }
            ],
            security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/info' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/info' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/info' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/info' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/info' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/info' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/info' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/info' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/info' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/info' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/info' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/info' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/info' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/info' } }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '500': { $ref: '#/components/responses/InternalError' }
            }
        },
        create: false,
        update: false,
        patch: false,
        remove: false
    },
    externalDocs: {
        url: 'https://alinex.gitlab.io/node-server/service/users'
    }
};
