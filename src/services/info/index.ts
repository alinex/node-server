// Initializes the `users` service on path `/users`
import { ServiceAddons, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import config from '../../config';
import applyFilter from '../../helper/applyFilter';
import { serviceLogger } from '../../logger';
import BaseService from '../base';

import api from './api';
import hooks from './hooks';
import { analyze, Data } from './analyze';

interface ServiceOptions { }

class Info extends BaseService implements ServiceMethods<Data | Data[]> {
  public docs = api;
  private context: any;

  options: ServiceOptions;

  constructor(app: Application, path: string) {
    super()
    this.context = { app, path };
    this.options = { paginate: config('paginate') };
    app.use(path, this);
    const service: ServiceAddons<any> = app.service(path);
    service.hooks(hooks);
  }

  async find (params?: Params): Promise<Data[] | Paginated<Data>> {
    const context = { ...this.context, params, method: 'find' };
    serviceLogger(context);
    const result = await analyze(this.context.app)
      .then(e => applyFilter(e, params));
    const abstract = Array.isArray(result) ? `${result.length} records` : `${result.total} records`;
    serviceLogger({ ...context, result }, abstract, true);
    return result;
  }

  async get (id: string, params?: Params): Promise<Data[] | any> {
    const context = { ...this.context, params, method: 'get' };
    serviceLogger(context);
    const [group, name] = id.split('.');
    let result = (await analyze(this.context.app).then(e => applyFilter(e, params))).filter((e: any) => e.group === group);
    if (result && name) result = result.filter((e: any) => e.name === name)[0].value;
    if (result) {
      const abstract = `${result.length} records`;
      serviceLogger({ ...context, result }, abstract, true);
      return result;
    }
    throw new Error(`No informational element under ${id} found!`);
  }
}

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    info: Info & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  new Info(app, '/info');
}
