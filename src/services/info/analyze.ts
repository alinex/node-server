import os from 'os';
import { Application } from '../../declarations';
import config from '../../config';
import async from '@alinex/async';

export interface Data {
  group: string;
  name: string;
  value: any;
}

function lag() {
  return new Promise(function (resolve) {
    const last = process.hrtime();
    setImmediate(function () {
      const delta = process.hrtime(last);
      resolve(`${delta[0]}s ${Math.round(delta[1] / 1000)}ms`);
    });
  });
}

function mongoVersion(app: Application, name: string): Promise<string> {
  return new Promise(function (resolve) {
    const mongoose = app.get(`${name}-mongoose`);
    const admin = new mongoose.mongo.Admin(mongoose.connection.db);
    admin.buildInfo(function (err: Error, info: any) {
      resolve(info.version);
    });
  });
}

function pgVersion(app: Application, name: string): Promise<string> {
  const knex = app.get(name);
  return knex.raw('show server_version').first()
}
function mysqlVersion(app: Application, name: string): Promise<string> {
  const knex = app.get(name);
  return knex.raw('SELECT VERSION()').first()
}
function mssqlVersion(app: Application, name: string): Promise<string> {
  const knex = app.get(name);
  return knex.raw('SELECT @@VERSION').first()
}
function oracleVersion(app: Application, name: string): Promise<string> {
  const knex = app.get(name);
  return knex.raw('SELECT version FROM dba_registry').first()
}
function sqliteVersion(app: Application, name: string): Promise<string> {
  const knex = app.get(name);
  return knex.raw('select sqlite_version()').first()
}

export async function analyze(app: Application): Promise<Data[]> {
  const data: Data[] = [];

  const cpus = os.cpus();
  data.push({ group: 'host', name: 'Architecture', value: os.arch() });
  data.push({ group: 'host', name: 'CPU Type', value: cpus[0].model });
  data.push({ group: 'host', name: 'CPU Cores', value: cpus.length });
  data.push({ group: 'host', name: 'CPU Speed (MHz)', value: cpus[0].speed });
  data.push({ group: 'host', name: 'CPU Load', value: os.loadavg().join(', ') });
  data.push({ group: 'host', name: 'Memory Total (Bytes)', value: os.totalmem() });
  data.push({ group: 'host', name: 'Memory Free (Bytes)', value: os.freemem() });
  data.push({ group: 'host', name: 'Uptime (Seconds)', value: os.uptime() });

  data.push({ group: 'os', name: 'Hostname', value: os.hostname() });
  const inet = os.networkInterfaces();
  const network: string[] = [];
  Object.keys(inet).forEach(function (key) {
    let alias = 0;
    inet[key]?.forEach(function (iface) {
      if (alias || iface.family !== 'IPv4' || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return;
      }
      network.push(`${key} => ${iface.address}`);
      alias++;
    });
  });
  data.push({ group: 'os', name: 'Network', value: network });
  data.push({ group: 'os', name: 'Platform', value: os.platform() });
  data.push({ group: 'os', name: 'Release', value: os.release() });
  data.push({ group: 'os', name: 'Time', value: Date.now() });
  data.push({ group: 'os', name: 'Temp Directory', value: os.tmpdir() });

  const user = os.userInfo();
  data.push({ group: 'user', name: 'User', value: user.username });
  data.push({ group: 'user', name: 'User ID', value: user.uid });
  data.push({ group: 'user', name: 'Group ID', value: user.gid });
  data.push({ group: 'user', name: 'User Home', value: user.homedir });

  Object.keys(process.env).forEach(function (key) {
    const val = process.env[key];
    data.push({ group: 'env', name: key, value: val });
  });

  data.push({ group: 'node', name: 'Process ID', value: process.pid });
  data.push({ group: 'node', name: 'Parent Process ID', value: process.ppid });
  data.push({ group: 'node', name: 'Process Name', value: process.title });
  data.push({ group: 'node', name: 'Node Version', value: process.versions.node });
  data.push({ group: 'node', name: 'v8 Version', value: process.versions.v8 });
  data.push({ group: 'node', name: 'Working Directory', value: process.cwd() });
  data.push({ group: 'node', name: 'Process Uptime (Seconds)', value: process.uptime() });
  const mem = process.memoryUsage()
  data.push({ group: 'node', name: 'Memory RSS (Byte)', value: mem.rss });
  data.push({ group: 'node', name: 'Memory Heap Total (Byte)', value: mem.heapTotal });
  data.push({ group: 'node', name: 'Memory Heap Used (Byte)', value: mem.heapUsed });
  data.push({ group: 'node', name: 'Memory External (Byte)', value: mem.external });
  data.push({ group: 'node', name: 'Event Loop Lag', value: await lag() });

  const packageInfo = require(`${process.cwd()}/package.json`);
  const serverInfo = require('../../../package.json');
  data.push({ group: 'server', name: 'Name', value: packageInfo.title || packageInfo.name });
  data.push({ group: 'server', name: 'Version', value: packageInfo.version });
  data.push({
    group: 'server',
    name: 'Author',
    value: `${packageInfo.author.name} <${packageInfo.author.email}>`
  });
  if (packageInfo.name != serverInfo.name)
    data.push({ group: 'server', name: 'Alinex Server', value: serverInfo.version });
  data.push({ group: 'server', name: 'Feathers', value: app.version });
  data.push({ group: 'server', name: 'Express', value: require('express/package.json').version });
  data.push({ group: 'server', name: 'Checkup', value: require('@alinex/checkup/package.json').version });

  for (const key in app.services) {
    const service = (<any>app.services)[key];
    data.push({ group: 'service', name: key, value: service.docs?.description || key });
  }

  await async.each(Object.keys(config('database')), async name => {
    const setup = config(`database.${name}`)
    let server = setup.connection
    if (typeof setup.connection === 'object') {
      if (setup.connection.filename) {
        server = `file://${setup.connection.filename}`
      } else {
        server = `${setup.client}://${setup.connection.user}@${setup.connection.host}`
        if (setup.connection.port) server += `:${setup.connection.port}`
        server += `/${setup.connection.database}`
      }
    }
    data.push({ group: 'database', name: `${name} Server`, value: server });
    switch (setup.client) {
      case 'mongodb':
      case 'mongooose':
        data.push({ group: 'database', name: `${name} Version`, value: await mongoVersion(app, name) });
        break;
      case 'pg':
        data.push({ group: 'database', name: `${name} Version`, value: await pgVersion(app, name) });
        break;
      case 'mysql':
      case 'mysql2':
        data.push({ group: 'database', name: `${name} Version`, value: await mysqlVersion(app, name) });
        break;
      case 'mssql':
        data.push({ group: 'database', name: `${name} Version`, value: await mssqlVersion(app, name) });
        break;
      case 'oracledb':
        data.push({ group: 'database', name: `${name} Version`, value: await oracleVersion(app, name) });
        break;
      case 'sqlite':
        data.push({ group: 'database', name: `${name} Version`, value: await sqliteVersion(app, name) });
        break;
    }
  }, 1)
  return data;
}
