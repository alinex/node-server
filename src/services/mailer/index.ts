// Initializes the `users` service on path `/users`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import config from '../../config';
//import { Mailer } from './class';
import hooks from './hooks';
import api from './api';
// @ts-ignore
import mailer from 'feathers-mailer';

// Add this service to the service type index
declare module '../../declarations' {
    interface ServiceTypes {
        mailer: ServiceAddons<any>;
    }
}

export default function(app: Application) {
    const smtp = config('smtp');

    // Initialize our service with any options it requires
    //app.use('/mailer', new Mailer(options, app));
    const service = mailer(smtp, { from: smtp.auth.user });
    service.docs = api;
    app.use('/mailer', service);

    // get initialized service to register hooks and filters
    app.service('mailer').hooks(hooks);
}
