import { disallow } from 'feathers-hooks-common';
// Don't remove this comment. It's needed to format import lines nicely.

export default {
    before: {
        all: [disallow('external')], // don't allow to use from extern
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },

    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },

    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
};
