import * as Builder from '@alinex/validator/lib/schema';

const fields = {
  _id: new Builder.StringSchema(),
  email: new Builder.EmailSchema({ normalize: true, registered: true }),
  password: new Builder.StringSchema({ trim: true, min: 8 }),
  autologin: new Builder.ArraySchema({
    item: { '*': new Builder.StringSchema() } // TODO maybe later use IPSchema with range validator
  }),
  nickname: new Builder.StringSchema({ trim: true, min: 4}),
  disabled: new Builder.BooleanSchema(),
  name: new Builder.StringSchema(),
  position: new Builder.StringSchema(),
  roles_id: new Builder.ArraySchema({
    item: { '*': new Builder.StringSchema() }
  })
}

export const create = new Builder.ObjectSchema({
  item: fields,
  denyUndefined: true
})

export const update = new Builder.ObjectSchema({
  item: fields,
  denyUndefined: true,
  mandatory: true
})

export const patch = new Builder.ObjectSchema({
  item: fields,
  denyUndefined: true
})
