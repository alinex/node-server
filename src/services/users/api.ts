export default {
    description: 'User management',
    idType: 'string',
    definitions: {
        user: {
            title: 'User',
            type: 'object',
            required: ['email', 'password'],
            properties: {
                _id: {
                    type: 'string',
                    description: 'The id of the user'
                },
                autologin: {
                    type: 'array',
                    items: { type: 'string' },
                    description: 'IP/CIDR for auto login as this user',
                    example: '127.0.0.1/8'
                },
                email: {
                    type: 'string',
                    description: 'Email address as unique identifier',
                    example: 'info@alinex.de'
                },
                password: {
                    type: 'string',
                    description: 'Secret password (make it unguessable)'
                },
                nickname: {
                    type: 'string',
                    description: 'A displayable pseudonym under which this user is presented'
                },
                name: {
                    type: 'string',
                    description: 'Full, real name of the user'
                },
                position: {
                    type: 'string',
                    description: 'Within this organisation'
                },
                disabled: {
                    type: 'boolean',
                    description: 'Set to false to prevent user to login'
                }
            }
        },
        users: {
            title: 'Users',
            type: 'array',
          items: { $ref: '#/components/schemas/user' }
        }
    },
    securities: ['find', 'get', 'update', 'patch', 'remove'],
    operations: {
        find: {
            summary: 'Users',
            description: 'Get a list of users with support of filter, paging and column selection.',
            parameters: [
              {
                description: 'CIDR for auto login.',
                in: 'query',
                name: 'cidr',
                schema: {
                  type: 'string'
                }
              },
              {
                description: 'Filter email address.',
                in: 'query',
                name: 'email',
                schema: {
                  type: 'string'
                }
              },
                {
                    description: 'Filter on nickname.',
                    in: 'query',
                    name: 'nickname',
                    schema: {
                        type: 'string'
                    }
                },
                {
                    description: 'Filter on name.',
                    in: 'query',
                    name: 'name',
                    schema: {
                        type: 'string'
                    }
                },
                {
                    description: 'Filter position.',
                    in: 'query',
                    name: 'position',
                    schema: {
                        type: 'string'
                    }
                },
                {
                    description: 'Number of records to skip.',
                    in: 'query',
                    name: '$skip',
                    schema: {
                        type: 'number',
                        minimum: 1
                    }
                },
                {
                    description: 'Number of results to return.',
                    in: 'query',
                    name: '$limit',
                    schema: {
                        type: 'number',
                        minimum: 1
                    }
                },
                {
                    description:
                        'Property to sort results. \
                        The value defines the order: 1 = ascending, -1 = descending.',
                    in: 'query',
                    name: '$sort',
                    schema: {
                        type: 'object',
                        properties: {
                            email: { type: 'integer', minimum: -1, maximum: 1, description: 'Email address' },
                            nickname: { type: 'integer', minimum: -1, maximum: 1, description: 'Alias name' },
                            name: { type: 'integer', minimum: -1, maximum: 1, description: 'Full name' },
                            position: {
                                type: 'integer',
                                minimum: -1,
                                maximum: 1,
                                description: 'Position in the organization'
                            }
                        }
                    },
                    style: 'deepObject',
                    explode: true
                },
                {
                    description: 'Array of columns to return.',
                    in: 'query',
                    name: '$select',
                    schema: {
                        type: 'array',
                        items: {
                            type: 'string',
                            enum: ['_id', 'nickname', 'name', 'position']
                        }
                    },
                    style: 'form',
                    explode: true
                }
            ],
        security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                        'application/json': { schema: { $ref: '#/components/schemas/users' } },
                        'application/bson': { schema: { $ref: '#/components/schemas/users' } },
                        'application/x-msgpack': { schema: { $ref: '#/components/schemas/users' } },
                        'application/javascript': { schema: { $ref: '#/components/schemas/users' } },
                        'text/x-coffeescript': { schema: { $ref: '#/components/schemas/users' } },
                        'text/x-yaml': { schema: { $ref: '#/components/schemas/users' } },
                        'text/x-ini': { schema: { $ref: '#/components/schemas/users' } },
                        'text/x-java-properties': { schema: { $ref: '#/components/schemas/users' } },
                        'text/x-toml': { schema: { $ref: '#/components/schemas/users' } },
                        'application/xml': { schema: { $ref: '#/components/schemas/users' } },
                        'text/csv': { schema: { $ref: '#/components/schemas/users' } },
                        'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/users' } },
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/users' } },
                        'text/plain': { schema: { $ref: '#/components/schemas/users' } }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '500': { $ref: '#/components/responses/InternalError' }
            }
        },
        get: {
            summary: 'Get user information.',
            description: 'Get information about a specific user.',
            parameters: [
                {
                    description: 'ID of the user',
                    in: 'path',
                    required: true,
                    name: '_id',
                    schema: {
                        type: 'string'
                    },
                    example: '429ee71276122f55a3a94796'
                }
            ],
          security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/user' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/user' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/user' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/user' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/user' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/user' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/user' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/user' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/user' } }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '500': { $ref: '#/components/responses/InternalError' }
            }
        },
        create: {
            summary: 'Create new user account',
            description: 'A new user account can be created without authentication.',
            responses: {
                '200': {
                    description: 'account created',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/user' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/user' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/user' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/user' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/user' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/user' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/user' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/user' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/user' } }
                    }
                },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '422': { $ref: '#/components/responses/ValidationError' },
                '500': { $ref: '#/components/responses/InternalError' }
            },
            security: [],
            requestBody: {
                required: true,
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/user'
                        },
                        example: {
                            email: 'demo@alinex.de',
                            nickname: 'demo',
                            name: 'Demo User',
                            position: 'Test'
                        }
                    }
                }
            }
        },
        update: {
            summary: 'Update complete record',
            description: 'Updates the resource identified by id using data.',
            parameters: [
                {
                    description: 'ID of user to update',
                    in: 'path',
                    required: true,
                    name: '_id',
                    schema: {
                        type: 'string'
                    }
                }
            ],
            requestBody: {
                required: true,
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/users'
                        },
                        example: {
                            email: 'demo@alinex.de',
                            nickname: 'demo',
                            name: 'Demo User',
                            position: 'Test'
                        }
                    }
                }
            },
          security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/user' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/user' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/user' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/user' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/user' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/user' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/user' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/user' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/user' } }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '422': { $ref: '#/components/responses/ValidationError' },
                '500': { $ref: '#/components/responses/InternalError' }
            }
        },
        patch: {
            summary: 'Patch user account',
            description: 'Updates the resource identified by id using data.',
            parameters: [
                {
                    description: 'ID of users to update',
                    in: 'path',
                    required: true,
                    name: '_id',
                    schema: {
                        type: 'string'
                    }
                }
            ],
            requestBody: {
                required: true,
                content: {
                  'application/json': { schema: { $ref: '#/components/schemas/user' } },
                  'application/bson': { schema: { $ref: '#/components/schemas/user' } },
                  'application/x-msgpack': { schema: { $ref: '#/components/schemas/user' } },
                  'application/javascript': { schema: { $ref: '#/components/schemas/user' } },
                  'text/x-coffeescript': { schema: { $ref: '#/components/schemas/user' } },
                  'text/x-yaml': { schema: { $ref: '#/components/schemas/user' } },
                  'text/x-ini': { schema: { $ref: '#/components/schemas/user' } },
                  'text/x-java-properties': { schema: { $ref: '#/components/schemas/user' } },
                  'text/x-toml': { schema: { $ref: '#/components/schemas/user' } },
                  'application/xml': { schema: { $ref: '#/components/schemas/user' } },
                  'text/csv': { schema: { $ref: '#/components/schemas/user' } },
                  'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/user' } },
                  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/user' } },
                  'text/plain': { schema: { $ref: '#/components/schemas/user' } }
                }
            },
          security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                        'application/json': {
                            schema: {
                                $ref: '#/components/schemas/users'
                            },
                            example: {
                                email: 'demo@alinex.de',
                                nickname: 'demo',
                                name: 'Demo User',
                                position: 'Test'
                            }
                        }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '422': { $ref: '#/components/responses/ValidationError' },
                '500': { $ref: '#/components/responses/InternalError' }
            }
        },
        remove: {
            summary: 'Delete user account',
            description: 'Remove the selected user account.',
            parameters: [
                {
                    description: 'ID of users to remove',
                    in: 'path',
                    required: true,
                    name: '_id',
                    schema: {
                        type: 'string'
                    }
                }
            ],
          security: [{ bearer: [] }],
            responses: {
                '200': {
                    description: 'success',
                    content: {
                      'application/json': { schema: { $ref: '#/components/schemas/user' } },
                      'application/bson': { schema: { $ref: '#/components/schemas/user' } },
                      'application/x-msgpack': { schema: { $ref: '#/components/schemas/user' } },
                      'application/javascript': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-coffeescript': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-yaml': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-ini': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-java-properties': { schema: { $ref: '#/components/schemas/user' } },
                      'text/x-toml': { schema: { $ref: '#/components/schemas/user' } },
                      'application/xml': { schema: { $ref: '#/components/schemas/user' } },
                      'text/csv': { schema: { $ref: '#/components/schemas/user' } },
                      'application/vnd.ms-excel': { schema: { $ref: '#/components/schemas/user' } },
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': { schema: { $ref: '#/components/schemas/user' } },
                      'text/plain': { schema: { $ref: '#/components/schemas/user' } }
                    }
                },
                '401': { $ref: '#/components/responses/Unauthorized' },
                '404': { $ref: '#/components/responses/NotFound' },
                '406': { $ref: '#/components/responses/NotAcceptable' },
                '500': { $ref: '#/components/responses/InternalError' }
            }
        }
    },
    externalDocs: {
        url: 'https://alinex.gitlab.io/node-server/service/users'
    }
};
