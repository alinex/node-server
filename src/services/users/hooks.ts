import * as local from '@feathersjs/authentication-local';
import { discard } from 'feathers-hooks-common';
// import { discard, iff } from 'feathers-hooks-common';
import authorize from '../../hooks/authorize';
import gravatar from '../../hooks/gravatar';
import populate from '../../hooks/populate';
import validate from '../../hooks/validate';
import * as schema from './schema';
//import { useCache, createCache } from '../../hooks/cache';
const { hashPassword, protect } = local.hooks;

//const cache = createCache(30);

const populateSchema = {
  include: {
    service: 'roles',
    nameAs: 'roles',
    parentField: 'roles_id',
    childField: '_id',
    asArray: true
  }
};

export default {
  before: {
    all: [],
    find: [authorize()],
    get: [authorize()], //, useCache(cache)],
    create: [validate(schema.create), hashPassword('password'), gravatar()],
    update: [authorize(), discard('roles', 'createdAt', 'updatedAt', 'avatar', '__caslSubjectType__'), validate(schema.update), hashPassword('password'), gravatar()],
    patch: [authorize(), discard('roles', 'createdAt', 'updatedAt', 'avatar', '__caslSubjectType__'), validate(schema.patch), hashPassword('password'), gravatar()],
    remove: [authorize()]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [populate({ schema: populateSchema })], //iff(context => !context.result._isCached, populate({ schema: populateSchema }), useCache(cache))],
    create: [], //useCache(cache)],
    update: [], //useCache(cache)],
    patch: [], //useCache(cache)],
    remove: [], //useCache(cache)]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
