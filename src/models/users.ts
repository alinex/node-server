import { Application } from '../declarations';

export default function(app: Application) {
    const mongooseClient = app.get('core-mongoose');
    const users = new mongooseClient.Schema(
        {
            // _id
            // __v version id
            email: { type: String, unique: true, required: true },
            password: { type: String, required: true },
            autologin: [{ type: String }],
            nickname: { type: String, unique: true, required: true },
            name: { type: String },
            position: { type: String },
            avatar: { type: String },
            disabled: { type: Boolean },
            roles_id: [{ type: mongooseClient.Schema.Types.ObjectId, ref: 'roles', required: true }]
        },
        {
            // createdAt
            // updatedAt
            timestamps: true
        }
    );

    return mongooseClient.model('users', users);
}
