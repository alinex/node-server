import { Application } from '../declarations';

export default function(app: Application) {
    const mongooseClient = app.get('core-mongoose');
    if (mongooseClient.models.roles) return mongooseClient.models.roles;

    const roles = new mongooseClient.Schema(
        {
            // _id
            // __v version id
            name: { type: String, required: true },
            description: String,
            disabled: Boolean,
            abilities: [
                {
                    action: [{ type: String, enum: ['read', 'update', 'create', 'remove'] }],
                    subject: [String],
                    conditions: mongooseClient.Schema.Types.Mixed,
                    fields: [String],
                    inverted: Boolean,
                    reason: { type: String }
                }
            ]
        },
        {
            // createdAt
            // updatedAt
            timestamps: true
        }
    );

    return mongooseClient.model('roles', roles);
}
