import { createLogger, format, transports, addColors } from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';
import mung from 'express-mung';
import { basename, extname } from 'path';
import { inspect } from 'util';

import { Application } from './declarations';
import config from './config';

addColors({
    error: 'red',
    warn: 'yellow',
    info: 'green',
    http: 'cyan',
    verbose: 'blue',
    debug: 'white',
    silly: 'grey'
});

// Configure the Winston logger. For the complete documentation see https://github.com/winstonjs/winston
const logger = createLogger({
    // To see more detailed errors, change this to 'debug'
    level: config('log.level'),
    format: format.combine(
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        format.splat(),
        format.printf(
            info =>
                `${info.timestamp} ${info.level.toLocaleUpperCase().padEnd(8)} ${info.message}` +
                (info.splat !== undefined ? `${info.splat}` : ' ')
        ),
        format.colorize({ all: true })
    ),
    transports: [
        new transports.Console(),
        new DailyRotateFile({
            dirname: 'log',
            filename: 'error.log',
            datePattern: 'YYYY-MM-DD',
            zippedArchive: true,
            maxFiles: 90,
            createSymlink: true,
            symlinkName: 'error.log',
            level: 'error'
        }),
        new DailyRotateFile({
            dirname: 'log',
            filename: 'combined.log',
            datePattern: 'YYYY-MM-DD',
            zippedArchive: true,
            maxFiles: 30,
            createSymlink: true,
            symlinkName: 'combined.log',
        })
    ]
});

const action = createLogger({
  // To see more detailed errors, change this to 'debug'
  level: config('log.level'),
  format: format.combine(
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    format.splat(),
    format.printf(
      info =>
        `${info.timestamp} ${info.level.toLocaleUpperCase().padEnd(8)} ${info.message}` +
        (info.splat !== undefined ? `${info.splat}` : ' ')
    ),
    format.colorize({ all: true })
  ),
  transports: [
    new transports.Console(),
    new DailyRotateFile({
      dirname: 'log',
      filename: 'action.log',
      datePattern: 'YYYY-MM',
      zippedArchive: true,
      maxFiles: 60, // 5 years
      createSymlink: true,
      symlinkName: 'action.log',
    })
  ]
});

// add logger for start and end of http request/response
export function addLogger(this: Application) {
    let app = this;
    app.set('logger', logger);
    const level = logger.levels[logger.level];
    if (level >= 3) app.use(httpLogger); // >= http
    if (level >= 6)
        // >= silly
        app.use(
            mung.json((body: any, _: any, res: any) => {
                res.body = body;
            })
        );
}

export function socketLogger (app: Application, socket: SocketIO.Socket): void {
  const logger = app.get('logger');
  logger.http('%s SOCKET  connected', socket.conn.remoteAddress)
}

export function socket2Logger (context: any, end: boolean = false) {
  if (context.params.provider !== 'socketio') return
  const logger = context.app.get('logger');
  if (!logger.debug) return;
  const { path, method, params } = context;
  const ip = params.clientIP || '-';
  const direction = end ? '<' : '>';
  const result = context.dispatch || context.result
  const data = Object.keys(params.query).length ? params.query : undefined
  if (context.error) {
    const message = context.error.toString().replace(/^Error/, context.error.constructor.name)
    logger.error(
      '%s\n%s',
      message,
      context.error.stack
      ?.split('\n')
      .slice(1, 6)
      .join('\n')
      );
//      context.error = new Error(message)
      return context
  }
  logger.http(
    `${ip} SOCKET  ${direction} ${path}::${method} ${!end && data ? (typeof data === 'object' ? inspect(data) : data) : ''}`
  );
  if (end) logger.silly('%s SOCKET  < Result: %o', ip, result.data || result);
  else {
    logger.silly('%s SOCKET  > Params: %o', ip, { query: params.query, data: context.data });
  }
}

function httpLogger(req: any, res: any, next: Function): void {
    const logger = req.app.get('logger');
    const start = new Date();
    logger.http(
        '%s %s > %s %s %s "%s"',
        req.ip,
        req.protocol.toUpperCase().padEnd(7),
        req.method,
        req.url,
        req.headers.referer || '-',
        req.get('User-Agent')
    );
    if (Object.keys(req.headers).length) logger.silly('%s HTTP    > Headers: %o', req.ip, req.headers);
    if (req.body && Object.keys(req.body).length) logger.silly('%s HTTP    > Request: %o', req.ip, req.body);

    res.on('finish', function() {
        const end = new Date();
        logger.http(
            `${req.ip} ${req.protocol.toUpperCase().padEnd(7)} < ${req.method} ${req.url} ${res.statusCode} ${
                res.statusMessage
            } ${res.getHeader('Content-Length') || 0} B ${end.getTime() - start.getTime()} ms`
        );
        if (res.body) logger.silly('%s HTTP    < Response: %o', req.ip, res.body);
    });
    next();
}

// logger for error messages
export function errorLogger(err: Error, req: any, res: any, next: Function): void {
    const logger = req.app.get('logger');
    if (!logger.error) return next(err);
    const message = err.toString().replace(/^Error/, err.constructor.name)
    err.message = message
    logger.error(
        '%s\n%s',
        err.toString(),
        err.stack
            ?.split('\n')
            .slice(1, 6)
            .join('\n')
    );
    next(err)
}

export function hookLogger(context: any, file: string, data: any = '', end: boolean = false) {
    const logger = context.app.get('logger');
    if (!logger.debug) return;
    const name = basename(file, extname(file));
    const ip = context.params.clientIP || '-';
    const direction = end ? '<' : '>';
    const params = Object.assign({}, context.params);
    delete params.headers;
    delete params.clientIP;
    logger.debug(`${ip} HOOK    ${direction} ${name} ${data ? (typeof data === 'object' ? inspect(data) : data) : ''}`);
    if (end) {
        logger.silly('%s HOOK    < Params: %o', ip, params.data || params);
        logger.silly('%s HOOK    < Result: %o', ip, context.result);
    } else logger.silly('%s HOOK    > Params: %o', ip, params);
}

export function serviceLogger(context: any, data: any = '', end: boolean = false) {
    const logger = context.app.get('logger');
    if (!logger.debug) return;
    const { method, path, result } = context;
    const ip = context.params.clientIP || '-';
    const direction = end ? '<' : '>';
    const params = Object.assign({}, context.params);
    delete params.headers;
    delete params.clientIP;
    logger.verbose(
        `${ip} SERVICE ${direction} ${path}::${method} ${data ? (typeof data === 'object' ? inspect(data) : data) : ''}`
    );
    if (end) logger.silly('%s SERVICE < Result: %o', ip, result.data || result);
    else {
        logger.silly('%s SERVICE > Params: %o', ip, params);
    }
}

export function actionLogger (context: any, id: any, data: any) {
//  const logger = context.app.get('logger');
  if (!logger.debug) return;
  const { path } = context;
  const ip = context.params.clientIP || '-';
  const user = context.result?.user || context.params?.user
  action.warn(
    `${ip} ACTION  \$ ${user.nickname} ${path} ${id ? (typeof id === 'object' ? inspect(id) : id) : '-'} ${data ? (typeof data === 'object' ? inspect(data) : data) : '-'}`
  );
}

export default logger;
