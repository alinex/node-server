import Debug from 'debug';
import Validator from '@alinex/validator';
import DataStore from '@alinex/datastore';
import { AnySchema } from '@alinex/validator/lib/schema';

import schema from './configSchema';
import { t } from './i18nCLI';

const debug = Debug('server:config');
let TYPE = 'production';

let loader: Promise<DataStore> | undefined = undefined;
let store: Validator = new Validator(new AnySchema());

export function describe(path = ''): string {
    return schema.describe(2, path);
}

export function load(type?: string): Promise<void> {
    if (type) {
        TYPE = type;
        loader = undefined;
    }
    if (!loader) {
        console.log(t('start.config', { TYPE }));

        // define configuration source
        const val = new Validator(schema, { source: `config/default.yml` }, { source: `config/${TYPE}.yml`, array: 'replace' });

        loader = <Promise<DataStore>>val.load()
            .catch(e => { throw new Error(e) })
            .then(_ => {
                store = val;
                if (process.env.LOG_LEVEL) store.get('log').level = process.env.LOG_LEVEL;
                debug('Finished loading');
            });
    }
    return loader.then(_ => {
        return Promise.resolve();
    });
}

export function has(command?: string): any {
    return store.has(command);
}

export function get(command?: string, fallback?: any): any {
    if (Object.keys(store.get()).length === 0) throw new Error('No configuration is loaded before accessing it!');
    debug('get %s', command);
    const data = store.get(command, fallback);
    debug('got %o', data);
    return data;
}

export default get;
