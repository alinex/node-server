# Alinex Server

A pre-configured, ready to use REST Server which can easily be used as backend to other services
or any frontend type.

## Usage

    server [options]...

General Options:

    --verbose, -v   run in verbose mode (multiple makes more verbose)      [count]
    --quiet, -q     don't output header and footer                       [boolean]
    --nocolors, -C  turn of color output                                 [boolean]
    --help, -h      Show help                                            [boolean]

You may use environment variables prefixed with 'SERVER\_' to set any of
the options like 'SERVER_VERBOSE' to set the verbose level.

## Documentation

See the online [documentation](https://alinex.gitlabs.io/node-server) for more information and also
how to setup the schema and use of API.

## License

(C) Copyright 2018-2019 Alexander Schilling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

> <http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
