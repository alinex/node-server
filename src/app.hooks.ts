import { socket2Logger } from './logger';
// Application hooks that run for every service
// Don't remove this comment. It's needed to format import lines nicely.

export default {
    before: {
    all: [(context: any) => socket2Logger(context)],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
    },

    after: {
      all: [(context: any) => socket2Logger(context, true)],
      find: [],
      get: [],
      create: [],
      update: [],
      patch: [],
      remove: []
    },

    error: {
      all: [(context: any) => socket2Logger(context, true)],
      find: [],
      get: [],
      create: [],
      update: [],
      patch: [],
      remove: []
    }
};
