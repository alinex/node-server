import knex from 'knex';
import { Application } from './declarations';

export default function (name: string, setup: any) {
  return function (app: Application) {
    const logger = app.get('logger');
    logger.silly(`Initializing ${name} as connection to ${setup.client} database...`);
    // setup
    const db = knex(setup);
    app.set(name, db);
  }
}
