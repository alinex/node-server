const i18next = require('i18next')
// @ts-ignore
import middleware from 'i18next-http-middleware'
import { readdirSync, lstatSync } from 'fs'
import Backend from 'i18next-fs-backend'
import { join } from 'path'
import moment from 'moment'
import Numeral from 'numeral';
import "numeral/locales/de";

var convert = require('@bstoots/convert-units');

export const i18n = i18next.createInstance()

i18n
  .use(Backend)
  .use(middleware.LanguageDetector)
  .init({
  // debug: true,
  // updateFiles: true,
  fallbackLng: 'en',
  initImmediate: false,
  preload: readdirSync(join(__dirname, '../data/locales')).filter((fileName: string) => {
    const joinedPath = join(join(__dirname, '../data/locales'), fileName)
    const isDirectory = lstatSync(joinedPath).isDirectory()
    return isDirectory
  }),
  ns: 'core',
  defaultNS: 'core',
  backend: {
    loadPath: join(__dirname, '../data/locales/{{lng}}/{{ns}}.yml')
  },
    interpolation: {
      escapeValue: false,
      format: function (value: any, format: string, lng: string, options: any) {
        const f = format.split(/\s+/)
        if (f[0] === 'lpad') return value.toString().padStart(f[1])
        else if (f[0] === 'rpad') return value.toString().padEnd(f[1])
        else if (f[0] === 'percent') {
          if (lng.substr(0, 2) === 'de') lng = 'de'
          else lng = 'en'
          Numeral.locale(lng)
          return Numeral(value).format('0.[00]%')
        }
        else if (f[0] === 'unit') {
          const quantity = convert(value).from(f[1]).toBest();
          if (lng.substr(0, 2) === 'de') lng = 'de'
          else lng = 'en'
          Numeral.locale(lng)
          return Numeral(quantity.val).format('0.[00]') + ' ' + quantity.unit
        }
        else if (typeof value === 'number') {
          if (lng.substr(0, 2) === 'de') lng = 'de'
          else lng = 'en'
          Numeral.locale(lng)
          return Numeral(value).format('0.[00]')
        }
        else if (value instanceof Date) {
          const d = moment(value)
          d.locale(lng)
          return d.format(format.trim() || 'L')
        }
        return value
      }
    }
})

export function t(keys: string|string[], options?: any) {
  return i18n.t(keys, options)
}
export function T(lang: string, ns?: string) {
  return i18n.getFixedT(lang, ns)
}
export function language () {
  return i18n.language
}
export function loadNamespace(ns: string) {
  return i18n.loadNamespaces(ns)
}

// console.log(t('test'))
// const x = T('en')
// console.log(x('test'))
//
// loadNamespace('b')
// console.log(x('b:test'))
